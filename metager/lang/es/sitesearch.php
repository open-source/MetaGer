<?php
return [
    'head' => [
        '1' => 'Metager Widget de búsqueda',
        '2' => 'Aquí encuentra el MetaGer-widget para su sitio web',
        '3' => 'Sitio web donde buscar',
        '4' => 'Introducir sitio web',
        '5' => 'Generar',
    ],
    'generated' => [
        '1' => 'Vista previa',
        '2' => 'Buscar y encontrar seguro con MetaGer',
        '3' => 'Busca con MetaGer...',
        '4' => 'Búsqueda',
        '5' => 'Código',
        '6' => 'es',
    ],
];
