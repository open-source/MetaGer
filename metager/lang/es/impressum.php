<?php
return [
    'title' => 'Aviso legal',
    'headline' => [
        '1' => '<a href="http://suma-ev.de/" target="_blank" rel="noopener">SUMA-EV - Verein für freien Wissenszugang (e.V.)</a> en cooperación con la <a href="http://www.uni-hannover.de/" target="_blank" rel="noopener">universidad Leibniz Hannover</a>',
    ],
    'info' => [
        '1' => 'Articulo de Wikipedia de <a href="http://de.wikipedia.org/wiki/Suma_e.V." target="_blank" rel="noopener">SUMA-EV</a>',
        '2' => "SUMA-EV\r
Postfach 51 01 43\r
D-30631 Hannover\r
Deutschland/Germany",
        '3' => "Contacto:\r
Tel.: +4951134000070\r
Correo electrónico <a href=\"/en-GB/kontakt\">Formulario de contacto cifrado</a>",
        '4' => 'Junta Directiva: Dominik Hebeler, Phil Höfer, Carsten Riel, Manuela Branz',
        '6' => 'Encargado de protección de menores: Manuela Branz <a href="mailto:jugendschutz@metager.de">jugendschutz@metager.de</a>',
        '8' => '"SUMA-EV - Verein für freien Wissenszugang" es una asociación sin fines de lucro, registrado en el registro de asociaciones del Amtsgericht Hannover bajo numero VR200033. Número de identificación a efectos del IVA:  DE 300 464 091 La "Gottfried Wilhelm Leibniz Universität Hannover" es una entidad del derecho publico.',
        '9' => 'Exención de responsabilidad',
        '10' => 'Pese al control minucioso de contenidos no podemos hacernos responsable de contenidos encontrados en hipervinculos externos. Para los contenidos de páginas linkeadas son unicamente responsables las respectivas compañías operadoras.',
    ],
];
