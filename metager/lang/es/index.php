<?php
return [
    'plugin' => 'Instalar MetaGer',
    'plugin-title' => 'Añadir MetaGer a su navegador',
    'key' => [
        'placeholder' => 'Introducir la clave para la búsqueda sin publicidad',
        'tooltip' => [
            'nokey' => 'Configurar una búsqueda sin anuncios',
            'empty' => 'Ficha agotada. Recarga ahora.',
            'low' => 'Ficha a punto de agotarse. Recarga ahora.',
            'full' => 'Búsqueda sin publicidad activada.',
        ],
    ],
    'placeholder' => 'MetaGer: Buscar & encontrar seguro, proteger la privacidad',
    'searchbutton' => 'Iniciar MetaGer-Search',
    'foki' => [
        'web' => 'Web',
        'bilder' => 'Imágenes',
        'nachrichten' => 'Noticias/Política',
        'science' => 'Ciencia',
        'produkte' => 'Productos',
        'maps' => 'Mapas',
    ],
    'adfree' => 'Utiliza MetaGer sin publicidad',
    'skip' => [
        'search' => 'Saltar a la consulta de búsqueda',
        'navigation' => 'Saltar a la navegación',
        'fokus' => 'Saltar a la selección del foco de búsqueda',
    ],
    'lang' => 'lenguaje wwitch',
    'searchreset' => 'eliminar la entrada de consulta de búsqueda',
    'searchbar-replacement' => [
        'message' => 'El motor de búsqueda MetaGer ya sólo está disponible sin publicidad.',
        'start' => 'Crear una nueva clave',
        'login' => 'Inicio de sesión con clave',
        'why' => '¿Por qué?',
        'key_error' => "La clave introducida no es válida. Por favor, compruebe la entrada.",
        'login_code_error' => "El código de inicio de sesión introducido no era válido. Sugerencia: ¡Los códigos de inicio de sesión sólo son válidos mientras están visibles en otro dispositivo!",
        'payment_id_error' => "Ha introducido un identificador de pago que no es una clave correcta. Tu clave tiene 36 caracteres.",
    ],
];
