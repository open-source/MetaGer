<?php
return [
    'title' => 'Ayuda de MetaGer',
    'backarrow' => 'Devolver',
    'app' => [
        'title' => 'Aplicación Android <a title="For easy help, click here" href="/hilfe/easy-language/services#help-app" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'También puedes utilizar MetaGer como aplicación. Solo tienes que descargar la aplicación <a href="https://metager.de/app" target="_blank" rel="noopener">MetaGer App</a> en tu smartphone Android.',
    ],
    'widget' => [
        'title' => 'Widget MetaGer <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-widget" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Este es un generador de código que le permite incrustar MetaGer en su sitio web. Puede utilizarlo para realizar búsquedas en su propio sitio o en Internet, según desee. Para cualquier consulta, utilice <a href="/kontakt/" target="_blank" rel="noopener">nuestro formulario de contacto</a>.',
    ],
    'maps' => [
        'title' => 'Mapas MetaGer <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-maps" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Preservar la privacidad en la era de los gigantes mundiales de los datos también nos ha llevado a desarrollar <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a>: el (que sepamos) único planificador de rutas que ofrece una funcionalidad completa a través de un navegador y una app sin almacenar la ubicación del usuario. Todo esto es verificable porque nuestro software es de código abierto. Para utilizar maps.metager.de, recomendamos nuestra versión rápida de app. Puede descargar nuestras aplicaciones desde <a href="/app" target="_blank">aquí</a> (o, por supuesto, también desde Play Store).',
        '2' => 'También se puede acceder a esta función de mapas desde la búsqueda de MetaGer (y viceversa). Una vez que hayas buscado un término en MetaGer, verás un nuevo foco de búsqueda "Mapas" en la esquina superior derecha. Al hacer clic en él, accederá al mapa correspondiente.',
        '3' => 'Al cargarse, el mapa muestra los puntos (POIs = Points of Interest) encontrados por MetaGer, que también se enumeran en la columna de la derecha. Al hacer zoom, esta lista se adapta a la sección del mapa. Al pasar el ratón por encima de un marcador del mapa o de la lista, se resalta el punto correspondiente. Haga clic en "Detalles" para obtener más información sobre ese punto a partir de la base de datos que aparece a continuación.',
    ],
    'easy-help' => 'Haciendo clic en el símbolo <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , accederá a una versión simplificada de la ayuda.',
    'services' => [
        'text' => "Servicios adicionales en torno a la búsqueda",
    ],
];
