<?php
return [
    'title' => [
        '2' => 'Uso de las páginas principales',
        '1' => 'Ayuda de MetaGer',
    ],
    'startpage' => [
        'title' => 'Página de inicio <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-startpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'La página de inicio incluye el campo de búsqueda, un botón en la esquina superior derecha para acceder al menú y dos enlaces debajo del campo de búsqueda para añadir MetaGer a su navegador y buscar sin anuncios. En la parte inferior, encontrará información sobre MetaGer y la asociación SUMA-EV. Además, en la parte inferior aparecen nuestras áreas de interés <i>Privacidad garantizada, Asociación sin ánimo de lucro, Diverso y gratuito</i> y <i>100% energía verde</i>. Haciendo clic en las respectivas secciones o desplazándose por ellas, podrá encontrar más información. ',
    ],
    'searchfield' => [
        'title' => 'El campo de búsqueda <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#eh-searchfield" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'El campo de búsqueda consta de varias partes:',
        'memberkey' => 'el símbolo de la llave: Aquí puede introducir su clave para utilizar la búsqueda sin anuncios. También puedes ver tu saldo de fichas y gestionar tu clave.',
        'slot' => 'el campo de búsqueda: Introduzca aquí el término de búsqueda. No se distingue entre mayúsculas y minúsculas.',
        'search' => 'la lupa: Inicie su búsqueda haciendo clic aquí o pulsando "Intro".',
        'morefunctions' => 'Encontrará funciones adicionales en la opción de menú "<a href = "/hilfe/funktionen">Funciones de búsqueda</a>"',
    ],
    'resultpage' => [
        'title' => 'Página de resultados <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-resultpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'foci' => 'Bajo el campo de búsqueda, hay 3 focos de búsqueda diferentes (Web, Imágenes, Noticias), cada uno de los cuales está asociado a motores de búsqueda específicos.',
        'choice' => 'A continuación, verá dos elementos: "Filtro" y "Configuración", si procede.',
        'filter' => 'Filtro: Aquí puede mostrar y ocultar las opciones de filtro y aplicar filtros. En cada foco de búsqueda tienes diferentes opciones de selección. Algunas funciones sólo están disponibles cuando se utiliza una tecla MetaGer.',
        'settings' => 'Configuración: Aquí, usted puede hacer ajustes permanentes de búsqueda para su búsqueda MetaGer en el foco actual. También puede seleccionar y deseleccionar los motores de búsqueda asociados al foco. Tus ajustes se guardan mediante una cookie de texto plano no identificable personalmente. También puedes acceder a la página de configuración a través del menú de la esquina superior derecha.',
    ],
    'backarrow' => 'Devolver',
    'result' => [
        'title' => 'Resultados <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-results" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => [
            '1' => 'Todos los resultados se presentan en el siguiente formato:',
            'open' => '"ABRIR": Haga clic en el titular o en el enlace inferior (la URL) para abrir el resultado en la misma pestaña.',
            'newtab' => '"ABRIR EN NUEVA PESTAÑA" abre el resultado en una nueva pestaña. Alternativamente, también puede abrir una nueva pestaña utilizando CTRL y el botón izquierdo del ratón o el botón central del ratón.',
            'anonym' => '"ABRIR ANÓNIMAMENTE" significa que el resultado se abre bajo la protección de nuestro proxy. Encontrará más información al respecto en la sección <a href = "/hilfe/datensicherheit#h-proxy">Anonimizar el servidor proxy MetaGer</a>.',
            'more' => '<img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>: Al hacer clic en <img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>, obtendrá nuevas opciones; el aspecto del resultado cambia:',
            '2' => 'Las nuevas opciones son:',
            'domainnewsearch' => '"Iniciar una nueva búsqueda en este dominio": Se realiza una búsqueda más detallada en el dominio del resultado.',
            'hideresult' => '"ocultar": Permite ocultar los resultados de este dominio. También puede escribir este modificador directamente después del término de búsqueda y concatenarlos; también se permite un comodín "*". Véase también Configuración para una solución permanente.',
        ],
    ],
    'settings' => [
        'title' => 'Ajustes <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-settings" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '2' => 'Motores de búsqueda utilizados <br> Aquí puede ver y ajustar los motores de búsqueda que está utilizando. Haciendo clic en el nombre correspondiente, puede activarlo o desactivarlo en consecuencia.',
        '3' => 'Filtros de búsqueda <br> Los filtros de búsqueda te permiten filtrar tu búsqueda permanentemente.',
        '4' => 'Lista negra <br> Aquí puede crear una lista negra personal. Puede utilizarla para filtrar dominios específicos y crear su propia configuración de búsqueda. Al hacer clic en "Añadir", estos ajustes se añadirán al enlace en la sección "Nota".',
        '5' => 'Alternar el modo oscuro <br> Cambia al modo oscuro fácilmente aquí.',
        '6' => 'Abrir resultados en una pestaña nueva <br> Aquí puede activar permanentemente la función para abrir los resultados en una pestaña nueva.',
        '7' => 'Citas <br> Aquí puedes activar o desactivar la visualización de citas.',
        '1' => 'Búsqueda sin publicidad <br> Aquí puede ver el saldo de su llave y su llave. También tiene la opción de recargar o eliminar su llave.',
        '8' => 'Restaurar toda la configuración actual <br> Aparecerá un enlace que puede establecer como página de inicio o marcador para conservar la configuración actual.',
        '9' => 'Publicidad Sutil para Nuestro Propio Servicio <br> Le mostramos publicidad sutil para nuestros propios servicios. Puede desactivar nuestra autopromoción aquí.',
    ],
    'easy-help' => 'Haciendo clic en el símbolo <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , accederá a una versión simplificada de la ayuda.',
];
