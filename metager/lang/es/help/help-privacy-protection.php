<?php
return [
    'title' => 'Ayuda de MetaGer',
    'backarrow' => 'Devolver',
    'tor' => [
        'title' => 'Servicio oculto TOR <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-torhidden" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Desde hace muchos años, MetaGer oculta y no almacena las direcciones IP. Sin embargo, estas direcciones son temporalmente visibles en el servidor de MetaGer mientras se ejecuta una búsqueda: si MetaGer se viera comprometida, un atacante podría leer y almacenar sus direcciones. Para cumplir con los más altos requisitos de seguridad, operamos una instancia de MetaGer en la red Tor: el Servicio Oculto MetaGer TOR - accesible a través de: <a href="/tor/" target="_blank" rel="noopener">https://metager.de/tor/</a>. Para utilizarlo, necesita un navegador especial, que puede descargar de <a href="https://www.torproject.org/" target="_blank" rel="noopener">https://www.torproject.org/</a>.',
        '2' => 'Puede acceder a MetaGer en el navegador Tor en: http://metagerv65pwclop2rsfzg4jwowpavpwd6grhhlvdgsswvo6ii4akgyd.onion .',
    ],
    'proxy' => [
        'title' => 'Anonimizar el servidor proxy MetaGer <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-proxy" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Para utilizarlo, sólo tienes que hacer clic en "ABRIR ANÓNIMAMENTE" en la parte inferior del resultado en la página de resultados de MetaGer. A continuación, tu solicitud se dirigirá al sitio web de destino a través de nuestro servidor proxy anonimizador, y tus datos personales permanecerán totalmente protegidos. Importante: si sigue los enlaces de las páginas a partir de este momento, seguirá protegido por el proxy. Sin embargo, no podrá introducir una nueva dirección en el campo de dirección de la parte superior. En este caso, perderá la protección. Puedes ver si sigues protegido en el campo de dirección, que mostrará: https://proxy.suma-ev.de/?url=here es la dirección actual.',
    ],
    'maps' => [
        'title' => 'Mapas MetaGer',
        '1' => 'Preservar la privacidad en la era de los gigantes mundiales de los datos también nos ha llevado a desarrollar <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a>: el (que sepamos) único planificador de rutas que ofrece una funcionalidad completa a través del navegador y la app sin almacenar la ubicación del usuario. Todo esto es verificable porque nuestro software es de código abierto. Para utilizar maps.metager.de, recomendamos nuestra versión rápida de la app. Puede descargar nuestras aplicaciones desde <a href="/app" target="_blank">aquí</a> (o, por supuesto, también desde Play Store).',
        '2' => 'También se puede acceder a esta función de mapas desde la búsqueda de MetaGer (y viceversa). Una vez que hayas buscado un término en MetaGer, verás un nuevo foco de búsqueda "Mapas" en la esquina superior derecha. Al hacer clic en él, accederá al mapa correspondiente.',
        '3' => 'Al cargarse, el mapa muestra los puntos (POIs = Points of Interest) encontrados por MetaGer, que también se enumeran en la columna de la derecha. Al hacer zoom, esta lista se adapta a la sección del mapa. Al pasar el ratón por encima de un marcador del mapa o de la lista, se resalta el punto correspondiente. Haga clic en "Detalles" para obtener más información sobre ese punto a partir de la base de datos que aparece a continuación.',
    ],
    'content' => [
        'title' => 'Contenido cuestionable / Protección de menores <a title="For easy help, click here" href="/help/easy-language/privacy-protection#eh-content" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'explanation' => [
            '1' => 'He recibido "hits" que no sólo me resultan molestos, sino que además contienen, en mi opinión, ¡contenido ilegal!',
            '2' => 'Si encuentra en internet algo que considera ilegal o perjudicial para los menores, puede ponerse en contacto con <a href="mailto:hotline@jugendschutz.net" target="_blank" rel="noopener">hotline@jugendschutz.net</a> por correo electrónico o visitar <a href="http://www.jugendschutz.net/" target="_blank" rel="noopener">www.jugendschutz.net</a> y rellenar el formulario de denuncia disponible allí. Es útil facilitar una breve nota sobre lo que considera inadmisible y cómo dio con ese contenido. También puede comunicarnos directamente los contenidos dudosos. Para ello, envíe un correo electrónico a nuestro responsable de protección de menores (<a href="mailto:jugendschutz@metager.de" target="_blank" rel="noopener">jugendschutz@metager.de</a>).',
        ],
    ],
    'easy-help' => 'Haciendo clic en el símbolo <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , accederá a una versión simplificada de la ayuda.',
    'privacy' => [
        'title' => "Anonimato y seguridad de los datos",
        '1' => 'Cookies de seguimiento, identificadores de sesión y direcciones IP <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-tracking" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '2' => 'Ninguno de ellos se utiliza, almacena, retiene o procesa de otro modo aquí en MetaGer (excepción: almacenamiento a corto plazo para la protección contra ataques de hackers y bots). Dado que consideramos este tema extremadamente importante, también hemos creado formas de ayudarte a alcanzar el máximo nivel de seguridad: el servicio oculto TOR de MetaGer y nuestro servidor proxy anonimizador.',
        '3' => "A continuación encontrará más información. Puede acceder a las funciones desde \"Servicios\" en la barra de navegación.",
    ],
];
