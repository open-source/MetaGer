<?php

return [
    "achtung"	=>	"Atención:\r\nDado que desarrollamos y mejoramos nuestro motor de búsqueda  constantemente, puede suceder que siempre haya cambios en la estructura y función. Aunque intentamos adaptar la ayuda lo más rápido posible a los cambios, no podemos descartar la posibilidad de discrepancias temporales en partes de las instrucciones.",
    "title"	=>	"Ayuda de MetaGer",
    "title.2"	=>	"Uso del motor de búsqueda",

];
