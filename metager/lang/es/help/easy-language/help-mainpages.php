<?php

return [

    "title"	=>	"Ayuda de MetaGer",
    "title.2"	=>	"Uso del motor de búsqueda",
    "startpage.title"	=>	"La página de inicio",
    "startpage.info"	=>	"La página de inicio contiene el campo de búsqueda, un botón en la parte superior derecha para abrir el menú y un enlace debajo del campo de búsqueda con el que puede agregar MetaGer a su navegador. En la parte inferior encontrará información sobre MetaGer y la asociación patrocinadora SUMA-EV.",
    "searchfield.title"	=>	"El campo de búsqueda",
    "searchfield.info"	=>	"El campo de búsqueda consiste de varias partes:",
    "searchfield.memberkey"	=>	"El símbolo de la llave (solo para asociados): Los socios del SUMA-EV pueden ingresar aquí su clave para poder utilizar la búsqueda sin publicidad. Si quiere resultados de búqueda sin anuncios publicitarios, conviértase en socio: <a href = \"/beitritt/\">Solicitud de admisión</a>",
    "searchfield.slot"	=>	"El campo de búsqueda: Ingrese su término de búsqueda aquí.",
    "searchfield.search"	=>	"La lupa: Inicie la búsqueda aquí con un clic o pulse \"Enter\".",
    "resultpage.title"	=>	"La página de resultados",
    "resultpage.foci"	=>	"Debajo del campo de búsqueda puede ver seis focos de búsqueda diferentes (tres en el sector anglófono) (web, imágenes ... ...), a los que se asignan internamente motores de búsqueda específicos.",
    "resultpage.choice"	=>	"A continuación verá dos opciones: en su caso, \"Filtro\" y \"Configuración\"",
    "resultpage.filter"	=>	"Filtros: Le ofrecen la posibilidad de elgir entre mostrar y ocultar opciones de filtro y de aplicar filtros. En cada enfoque de búsqueda, tiene diferentes opciones.",
    "resultpage.settings.0"	=>	"Configuración: Le permite realizar configuraciones de búsqueda permanentes en el foco actual para su búsqueda con MetaGer. También puede seleccionar o eliminar los buscadores asignados al foco. Sus configuraciones se guardan con la ayuda de cookies de texto sin formato no personal. También puede acceder a la página de configuración a través del menú en la parte superior derecha.",
    "resultpage.settings.1"	=>	"En \"Nota\", encontrará un enlace que le muestra todas sus configuraciones y con un clic las puede borrar, si lo desea.  Además encontrará un enlace que puede copiar y guardar como marcador. Si más tarde selecciona el marcador, sus configuraciones estarán disponibles nuevamente.\r\n",
    "resultpage.settings.2"	=>	"Aquí también tiene la oportunidad de compilar una lista negra personal; Por lo tanto, no solo puede filtrar los motores de búsqueda, sino también los dominios especiales y, por lo tanto, establecer su propia configuración de búsqueda. Con un clic en \"Agregar\", estas configuraciones se adjuntan al enlace en la sección \"Nota\".",
    "resultpage.settings.3"	=>	" Aquí puede activar y desactivar la visualización de las citas.",
    "resultpage.settings.4"	=>	"Simplemente cambie aquí al modo oscuro.",
    "stopworte.title"	=>	"Palabras de parada",
    "stopworte.1"	=>	"Si desea excluir en los resultados de búsqueda de MetaGer aquellos que contienen ciertas palabras (palabras de exclusión / palabras de parada), puede hacerlo agregando un signo menos a estas palabras.",
    "stopworte.2"	=>	"Ejemplo: Está buscando un automóvil nuevo, pero definitivamente no debe ser un BMW. \r\nEntonces tu entrada es:",
    "stopworte.3"	=>	"coche nuevo -bmw",
    "mehrwortsuche.title"	=>	"Búsqueda de varias palabras",
    "mehrwortsuche.1"	=>	"Si busca más de una palabra en MetaGer, automáticamente intentaremos proporcionarle los resultados que contengan todas las palabras o que se acerquen lo más posible a ellas.",
    "mehrwortsuche.2"	=>	"Si eso no es suficiente para usted, tiene dos opciones para refinar su búsqueda:"

];