<?php
return [
    'head' => [
        '1' => 'Aplicaciones MetaGer',
        '2' => 'MetaGer App',
        '3' => 'Aplicación Maps MetaGer',
        '4' => 'Instalación',
    ],
    'disclaimer' => [
        '1' => 'Actualmente solo podemos proporcionar nuestras aplicaciones para dispositivos Android.',
    ],
    'metager' => [
        '1' => 'Con esta aplicación, obtiene toda la potencia de nuestro motor de búsqueda en su smartphone. Busque en Internet con solo deslizar un dedo mientras que mantiene su privacidad.',
        '2' => 'Puede instalar la aplicación para nuestra búsqueda a través de Google Playstore o instalarla manualmente desde nuestro servidor en su smartphone, protegida de datos.',
        'playstore' => 'Google Playstore',
        'fdroid' => 'F-Droid Store',
        'manuell' => 'Instalación manual',
    ],
    'maps' => [
        '1' => 'Esta aplicación proporciona una integración nativa de <a href="https://maps.metager.de" target="_blank">MetaGer Maps</a> (impulsado por <a href="https://www.openstreetmap.de/" target="_blank">Openstreetmap</a>) en su dispositivo móvil Android.',
        '2' => 'De este modo el servicio de mapas y navegación también funciona de manera óptima y rápida en su smartphone. La aplicación aumenta el rendimiento en comparación con el uso en el navegador móvil y ofrece algunas otras ventajas. ¡Intentalo!',
        '3' => 'El APK para la instalación manual es aproximadamente 4 veces el tamaño de la instalación playstore (~ 250 MB), ya que contiene las bibliotecas para todas las arquitecturas de CPU comunes. El actualizador integrado conocerá la arquitectura de la CPU de tu dispositivo e instalará la versión correcta (pequeña) de la aplicación en la primera actualización. Si usted mismo conoce la arquitectura de su dispositivo también puede <a href="https://gitlab.metager.de/metagermaps/android/-/releases/permalink/latest" target="_blank">instalar directamente el paquete pequeño</a>.',
        'list' => [
            '1' => 'Acceso a datos de posición => Si el GPS está activado en su teléfono móvil, podemos mejorar sus resultados de búsqueda. Además habilita la función de navegación paso a paso.<b>Por supuesto, estos datos no se almacenan en ningún lugar y ciertamente no se transmiten a terceros.</b>',
            '2' => 'El APK para la instalación manual tiene un actualizador integrado. Para que el actualizador funcione, la aplicación le pedirá permiso para publicar notificaciones con el fin de notificarle de una actualización disponible y utiliza el permiso de Android REQUEST_INSTALL_PACKAGES para que pueda pedirle que instale la actualización de la aplicación.',
        ],
        '4' => 'Después del primer inicio, la aplicación solicita los siguientes permisos:',
    ],
];
