<?php
return [
    'angle-double-right' => [
        'alt' => 'Doble flecha a la derecha',
    ],
    'icon-lupe' => [
        'alt' => 'Búsqueda',
    ],
    'menu' => [
        'alt' => 'Menú',
    ],
    'cogs' => [
        'alt' => 'Engranajes',
    ],
    'ellipsis' => [
        'alt' => 'Más opciones',
    ],
    'ellipsis-horizontal' => [
        'alt' => 'Más opciones',
    ],
    'home' => [
        'alt' => 'Casa',
    ],
    'trashcan' => [
        'alt' => 'Cubo de basura',
    ],
    'x' => [
        'alt' => 'Cerrar',
    ],
    'floppy' => [
        'alt' => 'floppy',
    ],
    'Icon-settings' => 'Rueda dentada',
];
