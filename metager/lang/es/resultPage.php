<?php
return [
    'opensearch' => 'MetaGer: Buscar y encontrar seguro, proteger la esfera privada',
    'startseite' => 'MetaGer página de inicio',
    'impressum' => 'Aviso legal',
    'search-placeholder' => 'Introduzca los términos de búsqueda',
    'metager3' => 'Estás en una versión de prueba de MetaGer.',
    'engines' => [
        'queried' => 'Servicios de búsqueda solicitados',
        'disabled' => 'Añadir servicios de búsqueda a la consulta',
        'payment_required' => 'Servicios de búsqueda disponibles con <a href=\':link\'>la clave MetaGer</a>',
    ],
    'skiplinks' => [
        'heading' => 'Saltar rápidamente al contenido',
        'results' => 'Saltar a los resultados de búsqueda',
        'query' => 'Saltar al campo de entrada de la consulta de búsqueda',
        'settings' => 'Ir a la configuración de búsqueda',
        'navigation' => 'Saltar a navegación',
        'return' => 'Puede volver a este menú en cualquier momento pulsando la tecla escape',
    ],
];
