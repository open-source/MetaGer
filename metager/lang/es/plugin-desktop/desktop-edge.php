<?php
return [
    'default-search-v15' => [
        '1' => 'Haga clic en "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">"</img> en la parte superior derecha del navegador<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">y abra la "Configuración"</img>.',
        '2' => 'Desplácese hacia abajo y haga clic en "Configuración avanzada".',
        '3' => 'Desplácese de nuevo hacia abajo hasta el punto "Buscar en la barra de direcciones con" y haga clic en "Cambiar".',
        '4' => 'Selecciona "MetaGer: Search & find securely..." y haz clic en "Set as default".',
    ],
    'default-search-v18' => [
        '1' => 'Haga clic en "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">"</img> en la parte superior derecha del navegador<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">y abra la "Configuración"</img>.',
        '2' => 'Seleccione la pestaña "Avanzado".',
        '3' => 'Desplácese hasta la opción "Buscar en la barra de direcciones con" y haga clic en "Cambiar".',
        '4' => 'Selecciona "MetaGer: Search & find securely..." y haz clic en "Set as default".',
    ],
    'default-search-v80' => [
        '1' => 'Abra una nueva pestaña e introduzca "edge://settings/searchEngines" en la barra de direcciones para entrar en la configuración del motor de búsqueda.',
        '2' => 'Haga clic en "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">"</img> junto a la entrada de MetaGer<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">y seleccione "Establecer como predeterminado"</img>.',
    ],
    'default-page-v15' => [
        '1' => 'Haga clic en "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">"</img> en la parte superior derecha del navegador<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">y abra la "Configuración"</img>.',
        '2' => 'En "Establecer página de inicio" o "Abrir Edge con", selecciona "una página específica" e introduce allí ":link".',
        '3' => 'Haga clic para <img class= "mg-icon" src="/img/floppy.svg">guardar MetaGer como motor de búsqueda por defecto</img>.',
    ],
    'default-page-v80' => [
        '1' => 'Introduzca "edge://settings/onStartup" en la barra de direcciones para acceder a la configuración "On Startup".',
        '2' => 'Seleccione "Abrir página o páginas específicas" e introduzca ":link" como URL para "Añadir nueva página".',
        '3' => 'Nota: Todas las páginas web visibles aquí se abrirán ahora cuando se inicie el navegador. Puede eliminar las entradas moviendo el ratón sobre ellas y<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">haciendo clic en</img>"<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">"</img> a la derecha.',
    ],
    'plugin' => 'Al instalar nuestra extensión de navegador <a href="https://microsoftedge.microsoft.com/addons/detail/fdckbcmhkcoohciclcedgjmchbdeijog" target="_blank" rel="noopener"></a> , MetaGer se instala automáticamente como motor de búsqueda predeterminado. Es probable que tenga que activar manualmente la extensión en la configuración del navegador después de la instalación.',
];
