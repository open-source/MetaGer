<?php
return [
    'engines' => [
        'queried' => 'Services de recherche interrogés',
        'disabled' => 'Ajouter des services de recherche à la requête',
        'payment_required' => 'Services de recherche disponibles avec la clé <a href=\':link\'>MetaGer</a>',
    ],
    'opensearch' => 'MetaGer : recherche sécurisée, protection de la vie privée',
    'startseite' => 'Page d\'accueil MetaGer',
    'impressum' => 'avis de site',
    'search-placeholder' => 'Votre recherche...',
    'metager3' => 'Vous êtes actuellement sur une version test de MetaGer.',
    'skiplinks' => [
        'heading' => 'Accéder rapidement au contenu',
        'results' => 'Sauter aux résultats de la recherche',
        'query' => 'Aller au champ de saisie de la requête de recherche',
        'settings' => 'Sauter aux paramètres de recherche',
        'navigation' => 'Sauter à la navigation',
        'return' => 'Vous pouvez revenir à ce menu à tout moment en appuyant sur la touche d\'échappement',
    ],
];
