<?php
return [
    'key' => [
        'tooltip' => [
            'nokey' => 'Recherche sans publicité',
            'low' => 'Jeton bientôt épuisé. Rechargez maintenant.',
            'full' => 'Recherche sans publicité activée.',
            'empty' => 'Jeton épuisé. Rechargez maintenant.',
        ],
        'placeholder' => 'Saisir la clé du membre',
    ],
    'placeholder' => 'MetaGer : Rechercher et trouver avec protection de la vie privée',
    'searchbutton' => 'Démarrer MetaGer-Search',
    'foki' => [
        'web' => 'Web',
        'bilder' => 'Images',
        'nachrichten' => 'Actualités',
        'science' => 'La science',
        'produkte' => 'Produits',
        'maps' => 'Cartes',
    ],
    'plugin' => 'Installer MetaGer',
    'plugin-title' => 'Ajouter MetaGer à votre navigateur',
    'adfree' => 'Utiliser MetaGer sans publicité',
    'skip' => [
        'search' => 'Passer à la saisie de la requête de recherche',
        'navigation' => 'Sauter à la navigation',
        'fokus' => 'Passer à la sélection de l\'axe de recherche',
    ],
    'lang' => 'langue wwitch',
    'searchreset' => 'supprimer l\'entrée de la requête de recherche',
    'searchbar-replacement' => [
        'message' => 'Le moteur de recherche MetaGer est désormais disponible uniquement sans publicité !',
        'login' => 'Connexion avec la clé',
        'start' => 'Créer une nouvelle clé',
        'why' => 'pourquoi ?',
        'key_error' => "La clé introduite n'était pas valide. Veuillez vérifier la saisie.",
        'login_code_error' => "Le code de connexion saisi n'était pas valide. Conseil : les codes de connexion ne sont valables que lorsqu'ils sont visibles sur un autre appareil !",
        'payment_id_error' => "Vous avez introduit un identifiant de paiement qui n'est pas une clé correcte. Votre clé comporte 36 caractères.",
    ],
];
