<?php
return [
    'metager' => [
        'manuell' => 'Installation manuelle',
        '1' => 'Cette application apporte toute la puissance de Metager à votre smartphone. Effectuez des recherches sur le web d\'une seule touche tout en préservant votre vie privée.',
        '2' => 'Il y a deux façons d\'obtenir notre application : en l\'installant via le Google Playstore ou (ce qui est mieux pour votre vie privée) en l\'obtenant directement de notre serveur.',
        'playstore' => 'Google Playstore',
        'fdroid' => 'Boutique F-Droid',
    ],
    'head' => [
        '1' => 'Application MetaGer',
        '2' => 'Application MetaGer',
        '3' => 'Application MetaGer Maps',
        '4' => 'Installation',
    ],
    'disclaimer' => [
        '1' => 'Pour l\'instant, nous n\'avons qu\'une version Android de notre application.',
    ],
    'maps' => [
        '1' => 'Cette application offre une intégration native de <a href="https://maps.metager.de" target="_blank">MetaGer Maps</a> (powered by <a href="https://www.openstreetmap.de/" target="_blank">Openstreetmap</a>) sur votre appareil mobile Android.',
        '2' => 'Le calculateur d\'itinéraires et le service de navigation fonctionnent donc très rapidement sur votre smartphone. L\'application est plus rapide que l\'utilisation dans un navigateur web mobile. Et il y a encore d\'autres avantages - jetez-y un coup d\'œil !',
        '3' => 'L\'APK pour l\'installation manuelle est environ 4x la taille de l\'installation Playstore (~250MB) parce qu\'il contient des librairies pour toutes les architectures CPU courantes. L\'outil de mise à jour intégré connaîtra l\'architecture du processeur de votre appareil et installera la version correcte (petite) de l\'application lors de la première mise à jour. Si vous connaissez vous-même l\'architecture de votre appareil, vous pouvez également <a href="https://gitlab.metager.de/metagermaps/android/-/releases/permalink/latest" target="_blank">installer directement le petit paquet</a>.',
        'list' => [
            '1' => 'Accès aux données de positionnement => Si le GPS est activé, nous pouvons fournir de meilleurs résultats de recherche. Vous avez ainsi accès à la navigation pas à pas. <b> Bien entendu, nous ne stockons aucune de vos données et nous ne les transmettons pas à des tiers.</b>',
            '2' => 'L\'APK pour l\'installation manuelle dispose d\'un outil de mise à jour intégré. Pour que l\'updater fonctionne, l\'application demandera la permission de poster des notifications afin de vous avertir de la disponibilité d\'une mise à jour et utilisera la permission Android REQUEST_INSTALL_PACKAGES afin de vous demander d\'installer la mise à jour de l\'application.',
        ],
        '4' => 'Après le premier démarrage, les autorisations suivantes vous seront demandées :',
    ],
];
