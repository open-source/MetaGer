<?php
return [
    'startpage' => [
        'title' => 'La page d\'accueil <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-startpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'La page d\'accueil comprend le champ de recherche, un bouton dans le coin supérieur droit pour accéder au menu, et deux liens sous le champ de recherche pour ajouter MetaGer à votre navigateur et effectuer des recherches sans publicité. Dans la partie inférieure, vous trouverez des informations sur MetaGer et l\'association SUMA-EV. En outre, nos domaines d\'intérêt <i>Garantie de confidentialité, Association à but non lucratif, Diversité et gratuité</i>, et <i>Énergie 100 % verte</i> sont affichés au bas de la page. En cliquant sur les sections respectives ou en faisant défiler l\'écran, vous trouverez de plus amples informations. ',
    ],
    'title' => [
        '1' => 'MetaGer - Aide',
        '2' => 'Utilisation des pages principales',
    ],
    'searchfield' => [
        'title' => 'Le champ de recherche <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#eh-searchfield" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'Le champ de recherche se compose de plusieurs parties :',
        'memberkey' => 'le symbole de la clé : Ici, vous pouvez saisir votre clé pour utiliser la recherche sans publicité. Vous pouvez également consulter le solde de vos jetons et gérer votre clé.',
        'slot' => 'le champ de recherche : Saisissez votre terme de recherche ici. Les majuscules et les minuscules ne sont pas distinguées.',
        'search' => 'la loupe : Commencez votre recherche en cliquant ici ou en appuyant sur la touche "Entrée".',
        'morefunctions' => 'D\'autres fonctions sont disponibles sous l\'option de menu "<a href = "/hilfe/funktionen">Fonctions de recherche</a>".',
    ],
    'backarrow' => 'Retour',
    'resultpage' => [
        'title' => 'La page des résultats <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-resultpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'foci' => 'Sous le champ de recherche, il y a trois domaines de recherche différents (Web, Images, Actualités), chacun étant associé à des moteurs de recherche spécifiques.',
        'choice' => 'En dessous, vous verrez deux éléments : "Filtre" et "Paramètres", le cas échéant.',
        'filter' => 'Filtre : Ici, vous pouvez afficher et masquer les options de filtrage et appliquer des filtres. Dans chaque focus de recherche, vous disposez de différentes options de sélection. Certaines fonctions ne sont disponibles que si vous utilisez une clé MetaGer.',
        'settings' => 'Paramètres : Ici, vous pouvez définir des paramètres de recherche permanents pour votre recherche MetaGer dans le focus actuel. Vous pouvez également sélectionner et désélectionner les moteurs de recherche associés à la cible. Vos paramètres sont sauvegardés à l\'aide d\'un cookie en texte clair non identifiable personnellement. Vous pouvez également accéder à la page des paramètres via le menu situé dans le coin supérieur droit.',
    ],
    'result' => [
        'title' => 'Résultats <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-results" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => [
            '1' => 'Tous les résultats sont présentés dans le format suivant :',
            'open' => '"OPEN" : Cliquez sur le titre ou le lien ci-dessous (l\'URL) pour ouvrir le résultat dans le même onglet.',
            'newtab' => '"Ouvrir dans un nouvel onglet" permet d\'ouvrir le résultat dans un nouvel onglet. Vous pouvez également ouvrir un nouvel onglet en utilisant les touches CTRL et le clic gauche ou le bouton du milieu de la souris.',
            'anonym' => '"OUVRIR ANONYMEMENT" signifie que le résultat est ouvert sous la protection de notre proxy. Vous trouverez des informations à ce sujet dans la section <a href = "/hilfe/datensicherheit#h-proxy">Anonymizing MetaGer Proxy Server</a>.',
            'more' => '<img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>: Lorsque vous cliquez sur <img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>, vous obtenez de nouvelles options ; l\'apparence du résultat change :',
            '2' => 'Les nouvelles options sont les suivantes :',
            'domainnewsearch' => '"Lancer une nouvelle recherche sur ce domaine" : Une recherche plus détaillée est effectuée sur le domaine du résultat.',
            'hideresult' => '"hide" (cacher) : Cela vous permet de masquer les résultats de ce domaine. Vous pouvez également écrire ce commutateur directement après votre terme de recherche et le concaténer ; un caractère de remplacement "*" est également autorisé. Voir également Paramètres pour une solution permanente.',
        ],
    ],
    'settings' => [
        'title' => 'Paramètres <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-settings" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '2' => 'Moteurs de recherche utilisés <br>Ici, vous pouvez afficher et ajuster les moteurs de recherche que vous utilisez. En cliquant sur le nom correspondant, vous pouvez l\'activer ou le désactiver en conséquence.',
        '3' => 'Filtres de recherche <br>Les filtres de recherche vous permettent de filtrer votre recherche de façon permanente.',
        '4' => 'Liste noire <br>Ici, vous pouvez créer une liste noire personnelle. Vous pouvez l\'utiliser pour filtrer des domaines spécifiques et créer vos propres paramètres de recherche. En cliquant sur "Ajouter", ces paramètres seront ajoutés au lien dans la section "Note".',
        '5' => 'Basculer en mode sombre <br> Basculer en mode sombre facilement ici.',
        '6' => 'Ouvrir les résultats dans un nouvel onglet <br>Ici, vous pouvez activer de manière permanente la fonction d\'ouverture des résultats dans un nouvel onglet.',
        '7' => 'Citations <br>Vous pouvez activer ou désactiver l\'affichage des citations ici.',
        '1' => 'Recherche sans publicité <br>Ici, vous pouvez consulter le solde de votre clé et votre clé. Vous avez également la possibilité de recharger ou de retirer votre clé.',
        '8' => 'Restaurer tous les paramètres actuels <br>Un lien s\'affiche, que vous pouvez définir comme page d\'accueil ou comme signet pour conserver vos paramètres actuels.',
        '9' => 'Publicité subtile pour nos propres services <br>Nous vous montrons une publicité subtile pour nos propres services. Vous pouvez désactiver notre autopromotion ici.',
    ],
    'easy-help' => 'En cliquant sur le symbole <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , vous accéderez à une version simplifiée de l\'aide.',
];
