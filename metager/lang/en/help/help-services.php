<?php

return [
    "app" => [
        "1" => "You can also use MetaGer as an app. Simply download the <a href=\"https://metager.de/app\" target=\"_blank\" rel=\"noopener\">MetaGer App</a> on your Android smartphone.",
        "title" => 'Android App <a title="For easy help, click here" href="/hilfe/easy-language/services#help-app" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    "backarrow" => 'Back',
    "easy-help" => 'By clicking on the symbol <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>, you will access a simplified version of the help.',

    "services" => [
        "text" => "Additional services around the search",
    ],
    "maps" => [
        "1" => 'Preserving privacy in the age of global data giants has also led us to develop <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a>: the (to our knowledge) only route planner that offers full functionality via a browser and app without storing user locations. All of this is verifiable because our software is open source. For using maps.metager.de, we recommend our fast app version. You can download our apps from <a href="/app" target="_blank">here</a> (or of course also from the Play Store).',
        "2" => "This map function can also be accessed from the MetaGer search (and vice versa). Once you have searched for a term in MetaGer, you will see a new search focus 'Maps' in the upper right corner. Clicking on it will take you to a corresponding map.",
        "3" => "Upon loading, the map displays the points (POIs = Points of Interest) found by MetaGer, which are also listed in the right column. When zooming, this list adapts to the map section. Hovering your mouse over a marker on the map or in the list highlights the corresponding item. Click 'Details' to get more information about that point from the database below.",
        "title" => 'MetaGer Maps <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-maps" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    "title" => 'MetaGer - Help',
    "widget" => [
        "1" => "This is a code generator that allows you to embed MetaGer into your website. You can use it to perform searches on your own site or on the internet as desired. For any questions, please use <a href=\"/kontakt/\" target=\"_blank\" rel=\"noopener\">our contact form</a>.",
        "title" => 'MetaGer Widget <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-widget" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
];
