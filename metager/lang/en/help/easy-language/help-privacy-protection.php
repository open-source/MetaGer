<?php
return [
    "title" => 'MetaGer - Easy Help',
    "backarrow" => 'Back',
    'glossary' => 'By clicking on the symbol<a title="This symbol leads to the glossary" href="/hilfe/easy-language/glossary"><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>, you can access the explanation of difficult words.',

    "datenschutz" => [
        "title" => "Anonymity and Data Security",
        "1" => 'Tracking Cookies, Session IDs, and IP Addresses',
        "2" => 'At MetaGer, you are anonymous. <br> That means you remain unrecognized. <br> We do not store any data that could identify anyone. <br> We offer 2 features for this purpose: <br> Tor Hidden Service<a title="With it you can search safely. Click for more information" href="/hilfe/easy-language/glossary#gltorhidden" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> and <br> open anonymously<a title="A person is anonymous. So, nobody knows: Who is this person? Click here for more information" href="/hilfe/easy-language/glossary#glopenanonymously" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>',
        "3" => 'Find more about the topic at <a href="/hilfe/easy-language/privacy-protection#eh-torhidden">Tor Hidden Service</a>. <br> To learn more about <strong>open anonymously</strong><a title="A person is anonymous. So, nobody knows: Who is this person? Click here for more information" href="/hilfe/easy-language/glossary#glopenanonymously" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>, check out <a href="/hilfe/easy-language/privacy-protection#eh-proxy">MetaGer-Proxy</a>. <br> You can access these features under <strong>Services</strong> in the navigation bar.',
    ],
    "tor" => [
        "title" => "Tor Hidden Service",
        "1" => 'Tor Hidden Service<a title="With it you can search safely. Click for more information" href="/hilfe/easy-language/glossary#gltorhidden" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> is the safest way to use MetaGer. <br> However, you need the Tor Browser<a title="A browser for the darkweb. Click for more information." href="/hilfe/easy-language/glossary#gltorbrowser" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> for it. <br> You can download the Tor Browser<a title="A browser for the darkweb. Click for more information." href="/hilfe/easy-language/glossary#gltorbrowser" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> here: <br><a href=\"https://www.torproject.org/\" target=\"_blank\" rel=\"noopener\">https://www.torproject.org</a>. <br> Once the Tor Browser is downloaded, you can use the Tor Hidden Service<a title="With it you can search safely. Click for more information" href="/hilfe/easy-language/glossary#gltorhidden" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> here: <br><a href=\"https://metager.de/tor/\" target=\"_blank\" rel=\"noopener\">https://metager.de/tor</a>.',
        "2" => 'MetaGer can be accessed through the Tor Browser<a title="A browser for the darkweb. Click for more information." href="/hilfe/easy-language/glossary#gltorbrowser" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> using this link: <br> http://metagerv65pwclop2rsfzg4jwowpavpwd6grhhlvdgsswvo6ii4akgyd.onion',
    ],
    "proxy" => [
        "title" => "MetaGer-Proxy (open anonymously)",
        "1" => 'With our open-anonymously function, you can open web pages anonymously. <br> That means you remain unrecognized. <br> Open anonymously<a title="A person is anonymous. So, nobody knows: Who is this person? Click here for more information" href="/hilfe/easy-language/glossary#glopenanonymously" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> acts as a protective shield. <br> If you want to search anonymously, follow these steps: <br> In the search results, there is a button <strong>open anonymously</strong><a title="A person is anonymous. So, nobody knows: Who is this person? Click here for more information" href="/hilfe/easy-language/glossary#glopenanonymously" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>. <br> Here\'s how the button looks:',
        "2" => "When you click the button, you search anonymously. <br> If you enter a new web page in the search bar above, <br> you will no longer search anonymously.",
    ],
    "content" => [
        'title' => 'Questionable Content / Youth Protection',
        "explanation" => [
            '1' => 'If you find content on the internet that appears to be prohibited, you can report it. <br> Send an email to <a href="mailto:hotline@jugendschutz.net" target="_blank" rel="noopener">hotline@jugendschutz.net</a>. <br> Alternatively, visit <a href="http://www.jugendschutz.net/" target="_blank" rel="noopener">www.jugendschutz.net</a> and fill out the complaint form. ',
            '2' => 'If you come across prohibited content on MetaGer, please let us know. <br> Send an email to <a href="mailto:jugendschutz@metager.de" target="_blank" rel="noopener">jugendschutz@metager.de</a>.',
        ],
    ],
];
