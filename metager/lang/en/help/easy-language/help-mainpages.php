<?php

return [
    "title"                 => 'MetaGer - Easy Help',
    "backarrow"             => 'Back',
    'glossary'              => 'By clicking on the icon<a title="This symbol leads to the glossary" href="/hilfe/easy-language/glossary" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>, you can find an explanation of the difficult words.',
    "title.2"               => 'Using the main pages',
    "startpage"  => [
        "title"       => 'The start page',
        "info"  => [
            "1"        => 'The start page has many functions. <br>At the top right of the page, there is a button. <br>This is what the button looks like.',
            "2"      => 'You can press this button.<br> Then you will see a menu. <br> In this menu, you can choose many different things.',
            "3"      => 'In the middle of the start page is the search field.<br> Below the search field, it says <strong>Install MetaGer</strong>. <br> You can click on it.<br> That way, you can download MetaGer.<br>Then you can set MetaGer as your default search engine<a title="The standard search engine is the search engine you always use. Click here for more information here" href="/hilfe/easy-language/glossary#glstandardsearch" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>.',
            "4"      => 'At the bottom of the home page, there are colorful boxes. <br> You can click on these boxes. <br>There, you can learn more about MetaGer.',
        ]
    ], 
    "searchfield"  => [
        "title"     => 'The search field',
        "info"      => 'The search field has 3 parts:',
        "memberkey"  => [
            "1" => 'The left field:', 
            "2" => 'The left field shows a key.<br> This is what the left field looks like:',
            "3" => 'You can search with us without seeing ads. <br> To do this, click on the key. <br> Then click on the button <strong>Set Up Ad-Free Search</strong>.<br> After that, click on the button <strong>Create Key Now</strong>.<br> Then you will receive a password. <br> This password is called the key. <br> Now you can recharge the key with token for money.<br> Enter this key in the left field. <br> Then you can search without ads. <br> You can find more information here: <a href = "/hilfe/easy-language/functions#eh-keyexplain">MetaGer Key</a>',
        ],
        "slot"  => [
            "1"      => 'The middle field:',
            "2"      => "In the middle field, you write what you want to search for. <br>It doesn't matter if you write the words in UPPER or lower case.<br> The middle field looks like this:",
        ],
        "search"  => [
            "1"    => 'The right field:',
            "2"    => 'The right field shows a magnifying glass.',
            "3"    => 'If you click on the field, you start the search. <br> You can also press the ENTER key on your keyboard.',
        ],
        "morefunctions" => 'With <a href = "/hilfe/easy-language/functions">search functions</a>, you can find out more about the <strong>search</strong> topic.',
    ], 
    "resultpage"  => [
        "title"      => 'The results page',
        "foci"  => [
            "1"       => 'Below the search field, there is a row with several buttons. <br>These are different search categories.<a title="Results are sorted to make them easier to find. Click here for more information." href="/hilfe/easy-language/glossary#glsearchcategories" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>. <br> This is what the row looks like:',
            "2"      => 'If you click on these buttons, you will see different types of results. <br>These can be, for example, images or news.',
        ],
        "choice"     => 'Below the row with the multiple buttons, there are 2 other buttons. <br> These buttons are Settings and Filter.',
        "filter"  => [
            "title"     => 'Filter<a title="With this, you can determine what the search engine should display. Click here for more information" href="/hilfe/easy-language/glossary#glfilter" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>:',
            "1"     => 'If you click on Filter<a title="With this, you can determine what the search engine should display. Click here for more information" href="/hilfe/easy-language/glossary#glfilter" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>, you can filter your search. <br> Each search category<a title="Results are sorted to make them easier to find. Click here for more information" href="/hilfe/easy-language/glossary#glsearchcategories" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> has different filtering options.<br> For example, you can search for results from the last 30 days.<br>Then you will only see results from the past 30 days.<br>You can set the Filter<a title="With this, you can determine what the search engine should display. Click here for more information" href="/hilfe/easy-language/glossary#glfilter" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> in our settings.',
        ],
        "settings" => [
            "title" => 'Settings:',
            "1" => 'When you click on Settings, you will see the settings. <br> There you can adjust the settings for the selected search category<a title="Results are sorted to make them easier to find. Click here for more information" href="/hilfe/easy-language/glossary#glsearchcategories" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>.',
        ],
    ],
    "result" => [
        "title" => 'Results',
        "info" => [
            "1" => 'Results look like this:',
            "open" => [
                "title" => 'Open:',
                "0" => 'There are several ways to view the result:',
                "1" => 'You can click on the heading.',
                "2" => 'You can click on the blue link below the heading. <br> This link is called URL<a title="A URL is a web address. Click here for more information" href="/hilfe/easy-language/glossary#glurl" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>.',
            ],
            "newtab" => [
                "title" => 'Open in new tab:',
                "1" => 'This button opens the result in a new tab<a title="A browser can display multiple pages simultaneously. Each page is in a tab. Click here for more information" href="/hilfe/easy-language/glossary#gltab" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>. <br> You can also open a result in a new tab<a title="A browser can display multiple pages simultaneously. Each page is in a tab. Click here for more information" href="/hilfe/easy-language/glossary#gltab" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> by using the CTRL key and left-click. <br> Or you can press the middle mouse button. <br> You can permanently open the result in a new tab<a title="A browser can display multiple pages simultaneously. Each page is in a tab. Click here for more information" href="/hilfe/easy-language/glossary#gltab" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> as well. <br> To do this, open our settings: <br> Under <strong>More settings</strong>, you will find the option to open results in a new tab<a title="A browser can display multiple pages simultaneously. Each page is in a tab. Click here for more information" href="/hilfe/easy-language/glossary#gltab" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>. <br> There you can enable it by switching it to <strong>on</strong>. <br> Then the results will always open in a new tab<a title="A browser can display multiple pages simultaneously. Each page is in a tab. Click here for more information" href="/hilfe/easy-language/glossary#gltab" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>.',
            ],
            "anonym" => [
                "title" => 'Open Anonymously<a title="A person is anonymous. So, nobody knows: Who is this person? Click here for more information" href="/hilfe/easy-language/glossary#glopenanonymously" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>:',
                "1" => 'By clicking here, you open the result in a protected manner. <br> It is like a shield of protection. <br> The shield hides your identity and location from the websites you visit.<br> You can find more information about Open Anonymously <a href = "/hilfe/easy-language/privacy-protection#eh-proxy/">here<a title="A person is anonymous. So, nobody knows: Who is this person? Click here for more information." href="/hilfe/easy-language/glossary#glopenanonymously" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a></a>.',
            ],
            "more" => [
                "title" => '<img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg"  alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>:',
                "1" => 'When you click on <img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg alt="More""/>, there are new options. <br> The result will look different. <br> It will look like this:',
            ],
            "2" => 'There are new options:',
            "domainnewsearch" => [
                "title" => 'Start a new search on this domain<a title="A domain contains all the websites that one has. Click for more information." href="/hilfe/easy-language/glossary#gldomain" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>:',
                "1" => 'By pressing this button, you search only on this page.',
                "2" => 'Example:',
                "3" => 'You find a result from the Duden website. <br> You click on <img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg alt="More""/> for that result. <br> Then you click on the button <strong>start a new search on this domain<a title="A domain contains all the websites that one has. Click for more information." href="/hilfe/easy-language/glossary#gldomain" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a></strong>. <br> Then you search again, but only on the Duden website.',
            ],
            "hideresult" => [
                "title" => 'Hide:',
                "1" => 'By clicking on the button, you hide all results from the page. <br> Then they are invisible. <br> If you always want it this way, check the <a href="#eh-settings">Settings</a>. <br> There you can adjust it.',
            ],
        ],
    ],
    "settings" => [
        "title" => 'Settings',
        "metagerkey" => [
            "title" => "Ad-Free Search",
            '1'=> 'Here you can see the credit for the ad-free search. <br> You can use the ad-free search with a MetaGer Key. <br> You can find the MetaGer Key under the credit. <br> Below that, you will find two buttons. <br> You can click the left button <strong>Charge Key</strong> to charge the key. <br> You can click the right button <strong>Remove Key</strong> to delete the key from the browser<a title="The browser can be used to view pages on the Internet. Click here for more infomation" href="/hilfe/easy-language/glossary#glbrowser" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>. <br> You can find more information about the MetaGer Key here: <a href = "/hilfe/easy-language/functions#eh-keyexplain">MetaGer Key</a> <br> If you don\'t have a key, it will look like this:',
            '2'=> "Three buttons are displayed. <br> You can click the left button with the text <strong>What is it?</strong>. <br> This will take you to the information page about the MetaGer Key. <br> You can click the middle button with the text <strong>Set up existing key</strong>. <br> If you already have a MetaGer Key, you can enter it there. <br> If you don't have a key, you can click the right button <strong>Create new Key</strong> to purchase a new key.",
        ],
        "cookies" => [
            "title" => 'Restore All Current Settings',
            "1" => 'Some browsers<a title="The browser can be used to view pages on the Internet. Click here for more infomation" href="/hilfe/easy-language/glossary#glbrowser" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> delete your cookies<a title="The cookie enables the computer to remember things. Click here for more information." href="/hilfe/easy-language/glossary#glcookies" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> after a session. <br> This also deletes the MetaGer settings. <br> You can restore the settings using the link. <br> You can save this link as your homepage. <br> Then, when you open the browser<a title="The browser can be used to view pages on the Internet. Click here for more infomation" href="/hilfe/easy-language/glossary#glbrowser" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>, the settings will be restored. ',
        ],
        "searchengine" => [
            "title" => 'Used Search Engines:',
            "1" => 'Here you can see all the search engines<a title="With a search engine you can search for information on the Internet. Click here for more information." href="/hilfe/easy-language/glossary#glsearchengine" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> that MetaGer uses. <br> You can select which search engines<a title="With a search engine you can search for information on the Internet. Click here for more information." href="/hilfe/easy-language/glossary#glsearchengine" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> you want to use. <br> You can click on the individual buttons. <br> If the buttons are crossed out, it means you are not using the search engines<a title="With a search engine you can search for information on the Internet. Click here for more information." href="/hilfe/easy-language/glossary#glsearchengine" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>. <br> Some search engines<a title="With a search engine you can search for information on the Internet. Click here for more information." href="/hilfe/easy-language/glossary#glsearchengine" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> can only be used with a <a href = "/hilfe/easy-language/functions#eh-keyexplain">MetaGer Key</a>.',
            "2" => 'Search engines<a title="With a search engine you can search for information on the Internet. Click here for more information." href="/hilfe/easy-language/glossary#glsearchengine" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> that can only be used with a MetaGer Key are grayed out.',
        ],
        "filter" => [
            "title" => 'Search Filters<a title="With this, you can determine what the search engine should display. Click here for more information" href="/hilfe/easy-language/glossary#glfilter" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>:',
            "1" => [
                "0" => 'With the search filter<a title="With this, you can determine what the search engine should display. Click here for more information" href="/hilfe/easy-language/glossary#glfilter" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>, you can filter your search. <br> You can choose what you want to see.',
                "1" => 'Date:',
                "2" => 'Here you can select the date of the search results. <br> Only results with the selected date will be shown. <br> This function is only available with a MetaGer Key.',
                "3" => 'Language:',
                "4" => 'Here you can select the language of the search results. <br> Only results in the selected language will be shown. <br> Some languages require a MetaGer Key.',
            ],
            "2" => 'SafeSearch:',
            "3" => 'With the SafeSearch filter<a title="With it, you can set whether the search engine should also show things related to sex." href="/hilfe/easy-language/glossary#glsafesearch" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> (youth protection filter), you can hide unpleasant results. <br> <strong>Strict</strong> finds almost no unpleasant results. <br> <strong>Off</strong> finds all results, including unpleasant results. <br> <strong>Moderate</strong> finds some unpleasant results. <br> With <strong>Any</strong>, each search engine<a title="With a search engine you can search for information on the Internet. Click here for more information." href="/hilfe/easy-language/glossary#glsearchengine" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> decides for itself. <br> The moderate mode requires a MetaGer Key.',
        ],
        "blacklist" => [
            "title" => 'Blacklist:',
            "1" => 'Here you can specify web pages that you do not want to see in the results. <br> Enter the page in the field. <br> Then click the <strong>Save</strong> button. <br> You can enter multiple results here. <br> This will hide the page in the results.',
            "2" => 'If you want to see a page again, you can delete the web page from the field.',
        ],
        "moresettings" => 'More Settings:',
        "advertisement" => [
            "title" => 'Subtle Advertising for Our Own Service',
            "1" => 'We show you advertisements for MetaGer. <br> This advertising is not very prominent. <br> You can turn off our own advertising. <br> However, you will need a <a href="/hilfe/easy-language/functions#eh-keyexplain">MetaGer Key</a> to do so.',
        ],        
        "darkmode" => [
            "title" => 'Toggle Dark Mode',
            "1" => 'Here you can turn on or off the dark mode. <br> In dark mode, the background is dark and the text is light.',
        ],
        "newtab" => [
            "title" => 'Open Results in New Tab',
            "1" => 'Here you can view the results in a new tab<a title="A browser can display multiple pages simultaneously. Each page is in a tab. Click here for more information" href="/hilfe/easy-language/glossary#gltab" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>. <br> This applies to each result.',
        ],
        "cite" => [
            "title" => 'Citations:',
            "1" => 'Here you can choose whether you want to see citations. <br> Citations are displayed on the results page on the right.',
        ],
    ],
];