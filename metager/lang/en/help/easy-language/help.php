<?php
return [
    "achtung" => 'Attention, as our search engine is constantly being developed and improved, there may be changes to its structure and functionality from time to time. While we strive to adapt the help section to these changes as quickly as possible, we cannot guarantee that there won\'t be temporary discrepancies in some explanations.',
    "title" => 'MetaGer - Easy Help',
    "easy.language.back" => "Back",
    
    "tableofcontents" => [
        'title' => '',
        "1"  => [
            '0' => 'The Main Pages',
            '1' => 'The Start Page',
            '2' => 'The Search Field',
            '3' => 'The Results Page',
            '4' => 'Settings',
        ],
        "2"  => [
            '0' => 'Useful Features and Tips',
            '1' => 'Search Functions',
            '2' => '!Bangs',
            '3' => 'Keywords',
            '4' => 'Download MetaGer',
        ],

        "3"  => [
            '0' => 'Features for Anonymity and Data Security',
            '2' => 'Cookies & Co.',
            '3' => 'Tor Hidden Service',
            '4' => 'Open Anonymously',
            '5' => 'Youth Protection',
        ],
        
        "4"  => [
            '0' => 'Additional Services around the Search',
            '1' => 'Android App',
            '3' => 'Search Term Associator',
            '4' => 'MetaGer Widget',
            '5' => 'MetaGer Maps',
        ],

    ],
];
