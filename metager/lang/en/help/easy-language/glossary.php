<?php
return [
    "title" => "Glossary",
    "tableofcontents" => "Table of Contents",
    "backarrow" => "Back",
    "uparrow" => "Up",

    "entry" => [
        "1" => "Search Engine",
        "2" => "Standard Search Engine",
        "3" => "Search Categories",
        "4" => "Filter",
        "5" => "URL",
        "6" => "Tab",
        "7" => "Open anonymously",
        "8" => "Safesearch",
        "9" => "!Bangs",
        "10" => "Cookies",
        "11" => "Domain",
        "12" => "Browser",
        "13"=>"Tor-Hidden-Service",
        "14"=>"Tor-Browser",
        "15"=>"App",
        "16"=>"Associator",
        "17"=>"Widget",
        "18"=>"Tor",

    ],

    "explanation" => [
        "entry1" => [
            '1' => 'A search engine is like a program on a computer. <br> With a search engine, you can search for information on the internet. <br> You can search for information on specific topics. <br> You can also search for images and videos.',
            '2' => 'There are many different search engines. <br> Many of them store your data. <br> Often, your data is shared with other companies that show you advertisements. <br> These advertisements are tailored to your interests. <br> The search engines make money this way. <br>MetaGer is a special search engine. <br> We do <strong>not</strong> store your data. <br> It is important to us to protect your data. <br>',
            '3' => 'What is a meta-search engine?',
            '4' => 'MetaGer is a meta-search engine. <br> A meta-search engine collects results from multiple search engines. <br> These results are then reorganized. <br> All the results are shown on one page. <br> So, with a meta-search engine, you search with many search engines at the same time.',
        ],
        "entry2" => [
            '1' => 'This is the search engine you always use for searching. <br> It is always selected when you search. <br> You open your browser. <br> You enter what you want to search for in the address bar at the top. <br> Then you search with the standard search engine.',
        ],
        "entry3" => [
            '1' => 'Some things are already sorted in the search engine to make them easier to find. <br> For example, you can use the search category <strong>Products</strong> when you want to buy something.',
            '2' => 'At MetaGer, there are suitable search engines for some search categories. <br> For example, specialized product search engines.',
        ],
        "entry4" => [
            '1' => 'A filter is a software. <br> With it, you can determine what the search engine should display. <br> For example, only new things. <br> Or only a specific language.',
            '2' => 'MetaGer has different filtering options. <br> You can sort by: date, size, document type, or Safesearch.',
        ],
        "entry5" => [
            '1' => 'A URL is an internet address. <br> The browser can use it to find a page on the internet. <br> So, search engines can find URLs. <br> The URL is shown in the address field of a browser.',
        ],
        "entry6" => [
            '1' => 'A browser can display multiple pages at the same time. <br> Each page is then in a tab. <br> You can click on a tab.',
            '2' => 'Most browsers use tabs. <br> Tabs can be grouped. <br> Some browsers offer anonymous tabs.',
        ],
        "entry7" => [
            '0' => 'What is anonymity?',
            '1' => 'You can open a page on the internet anonymously. <br> Click on the button <strong>open anonymously</strong> in MetaGer. <br> Then nobody knows that you have viewed the page.',
            '2' => 'The requested page is passed on to the proxy service at MetaGer.<br> Proxies prevent your own address from being visible.',
            '3' => 'A person is anonymous. <br> So, nobody knows: Who is this person? <br> There are no clues about who a person is.',
            '4' => 'On the internet, it is usually about not transmitting your own IP address.',
        ],
        "entry8" => [
            '1' => 'Safesearch is an English word. <br> Safesearch is a filter. <br> With it, you can set whether the search engine should also show things related to sex.',
            '2' => 'Safesearch follows US law. <br> What is not suitable for minors according to US law determines the search results. <br> Several categories between "off" and "strict" are offered.',
        ],
        "entry9" => [
            '1' => '"!Bangs" refers to a shortcut for faster searching. <br> With !bangs, you can perform specific searches more quickly.',
        ],
        "entry10" => [
            '1' => 'Cookies are software. <br> The computer writes them itself. <br> With the cookie, the computer can remember things. <br> So, the computer remembers settings like the dark mode.',
            '2' => 'Different types of cookies are distinguished. <br> Some are used to remember settings. <br> These are also used by MetaGer. <br> Others are used to control sessions. <br> In addition, there are third-party cookies that are written without technical necessity.',
        ],
        "entry11" => [
            '1' => 'Computers use numbers. <br> Words are made from them. <br> These are easier to remember. <br> All internet pages with a certain name are in a domain.',
            '2' => 'The Domain Name System is a service. <br> The short word for it is DNS. The DNS is an information service for computer networks. <br> The information translates address numbers into address words. <br> This makes it easier to remember computer addresses.',
        ],
        "entry12" => [
            '1' => 'A browser is a computer program. <br> With the browser, you can view pages on the internet. <br> Browser is an English word. <br> Pronounced like: "Brauser".',
            '2' => 'There are different browsers. <br> The differences are small. <br> There are browsers for the Onion web, also known as the Dark web.',
        ],

        "entry18" => [
            '1' => 'What is Tor?',
            '2' => 'Tor can be explained more easily with a simplified example. <br> Peter wants to know when Birgit\'s birthday is. <br> However, he doesn\'t want Birgit to know that he wants to know. <br> What can Peter do? <br> He asks Lena to inquire about Birgit\'s birthday. <br> However, there is a problem. <br> Lena knows who asked her to find out about Birgit\'s birthday. <br> To prevent being identified, Peter does the following:',
            '3' => 'Peter gives Lena a sealed envelope. <br> In this envelope, he asks about Birgit\'s birthday.',
            '4' => 'Lena does not open this envelope. <br> So, Lena does not know what Peter wants to know. <br> Lena passes this envelope on to Martin.',
            '5' => 'Martin doesn\'t know Peter or Birgit. <br> Martin also does not open the envelope. <br> He passes the envelope on to Sophie.',
            '6' => 'Sophie does not know that this envelope is from Peter. <br> She opens the envelope. <br> She sees that someone wants to know when Birgit\'s birthday is. <br> Sophie asks Birgit about her birthday. <br> She writes this information on a piece of paper and seals it in an envelope. <br> She gives this envelope to Martin.',
            '7' => 'Martin passes the sealed envelope on to Lena.',
            '8' => 'Lena returns the sealed envelope to Peter. <br> Again, Lena does not know what is written in the envelope.',
            '9' => 'Peter can now open this envelope and knows Birgit\'s birthday.',
        ],
        "entry13" => [
            '1' => 'With the Tor Hidden Service, you can use MetaGer over the Tor network.',
            '2' => 'A simplified explanation of the Tor network can be found on this page under <a href="/help/easy-language/glossary#gltor">Tor</a>. <br> For more information about Tor, visit the Tor website: <a href="https://www.torproject.org/">https://www.torproject.org</a>',
        ],
        "entry14" => [
            '1' => 'The Tor Browser provides a fully pre-configured way to use Tor.',
            '2' => 'A simplified explanation of the Tor network can be found on this page under <a href="/help/easy-language/glossary#gltor">Tor</a>. <br> For more information about Tor, visit the Tor website: <a href="https://www.torproject.org/">https://www.torproject.org</a>',
        ],
        "entry15"=> [
            '1' => 'An app is a program for mobile phones. <br>To use an app, you need a special type of mobile phone.<br>These phones are also called smartphones.<br>Apps require the internet.<br>Smartphones have internet.<br>So, you can use apps with smartphones.',
        ],
        "entry16"=> [
            '1' => 'Placeholder',
            '2'=>'Placeholder',
        ],
        "entry17"=> [
            '1' => 'Placeholder',
            '2'=>'Placeholder',
        ],
    ],
];
