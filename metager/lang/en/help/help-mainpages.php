<?php

return [
    "easy-help"=> 'By clicking on the symbol <a title="For easy help, click here" href="/hilfe/easy-language/mainpages" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>, you will access a simplified version of the help.',
    "backarrow"             => 'Back',
    "result"        => [
        "info"       => [
            "1" => 'All results are presented in the following format:',
            "2" => 'The new options are:',
            "anonym" => '"OPEN ANONYMOUSLY" means that the result is opened under the protection of our proxy. Some information about this can be found in the <a href = "/hilfe/datensicherheit#h-proxy">Anonymizing MetaGer Proxy Server</a> section.',
            "domainnewsearch" => '"Start a new searc on this domain": A more detailed search is performed on the domain of the result.',
            "hideresult" => '"hide": This allows you to hide results from this domain. You can also write this switch directly after your search term and concatenate it; a "*" wildcard is also allowed. See also Settings for a permanent solution.',
            "more" => '<img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>: When you click on <img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>, you will get new options; the appearance of the result changes:',
            "newtab" => '"OPEN IN NEW TAB" opens the result in a new tab. Alternatively, you can also open a new tab by using CTRL and left-click or the middle mouse button.',
            "open" => '"OPEN": Click on the headline or the link below (the URL) to open the result in the same tab.',
        ],
        "title" => 'Results <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-results" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    "resultpage"        => [
        "choice"     => 'Below, you will see two items: "Filter" and "Settings" if applicable.',
        "filter"     => 'Filter: Here, you can show and hide filter options and apply filters. In each search focus, you have different selection options. Some functions are only available when using a MetaGer key.',
        "foci"       => 'Under the search field, there are 3 different search foci (Web, Images, News), each of which is associated with specific search engines.',
        "settings" => 'Settings: Here, you can make permanent search settings for your MetaGer search in the current focus. You can also select and deselect search engines associated with the focus. Your settings are saved using a non-personally identifiable plaintext cookie. You can also access the settings page via the menu in the upper right corner.',
        "title" => 'The Results Page <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-resultpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    "searchfield"       => [
        "info"      => 'The search field consists of several parts:',
        "memberkey" => 'the key symbol: Here, you can enter your key to use ad-free search. You can also view your token balance and manage your key.',
        "morefunctions" => 'Additional functions can be found under the menu item "<a href = "/hilfe/funktionen">Search Functions</a>"',
        "search" => 'the magnifying glass: Start your search by clicking here or pressing "Enter".',
        "slot" => 'the search field: Enter your search term here. Uppercase and lowercase letters are not distinguished.',
        "title" => 'The Search Field <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#eh-searchfield" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    "settings"      => [
        "1" => 'Ad-Free Search <br> Here, you can view the balance of your key and your key. You also have the options to top up or remove your key.',
        "2" => 'Used Search Engines <br> Here, you can view and adjust the search engines you are using. By clicking on the corresponding name, you can enable or disable it accordingly.',
        "3" => 'Search Filters <br> Search filters allow you to filter your search permanently.',
        "4" => 'Blacklist <br> Here, you can create a personal blacklist. You can use it to filter out specific domains and create your own search settings. By clicking on "Add", these settings will be appended to the link in the "Note" section.',
        "5" => 'Toggle Dark Mode <br> Switch to Dark Mode easily here.',
        "6" => 'Open Results in New Tab <br> Here, you can permanently enable the function to open results in a new tab.',
        "7" => 'Quotes <br> You can toggle the display of quotes on and off here.',
        "8" => 'Restore All Current Settings <br> A link will be displayed that you can set as your homepage or bookmark to keep your currently set settings.',
        '9' =>'Subtle Advertising for Our Own Service <br> We show you subtle advertising for our own services. You can turn off our self-promotion here.',
        "title" => 'Settings <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-settings" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    "startpage" => [
        "info" => 'The start page includes the search field, a button in the upper right corner to access the menu, and two links below the search field to add MetaGer to your browser and search ad free. In the lower section, you will find information about MetaGer and the SUMA-EV association. Additionally, our focus areas <i>Guaranteed Privacy, Nonprofit Association, Diverse & Free</i>, and <i>100% Green Energy</i> are displayed at the bottom. By clicking on the respective sections or by scrolling, you can find more information. ',
        "title" => 'The Start Page <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-startpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    "title" => [
        "1" => 'MetaGer - Help',
        "2" => 'Using the Main Pages',
    ],
];