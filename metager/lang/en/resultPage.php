<?php
return [
    'opensearch' => 'MetaGer: secure search & find, protecting privacy',
    'skiplinks' => [
        'heading' => 'Quickly jump to content',
        'results' => 'Jump to search results',
        'query' => 'Jump to search query input field',
        'settings' => 'Jump to search settings',
        'navigation' => 'Jump to navigation',
        'return' => 'You can return to this menu any time by pressing the escape key'
    ],
    'startseite' => 'MetaGer start page',
    'impressum' => 'site notice',
    'search-placeholder' => 'Your search query...',
    'metager3' => 'You are currently on a MetaGer test version.',
    'engines' => [
        'queried' => 'Search services queried',
        'disabled' => 'Add search services to the query',
        'payment_required' => 'Search services available with <a href=\':link\'>MetaGer key</a>',
    ],
];
