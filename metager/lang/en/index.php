<?php
return [
    'skip' => [
        'search' => 'Skip to search query input',
        'navigation' => 'Skip to navigation',
        'fokus' => 'Skip to search focus selection',
    ],
    'lang' => 'wwitch language',
    'plugin' => 'Install MetaGer',
    'plugin-title' => 'Add MetaGer to your browser',
    'key' => [
        'placeholder' => 'Enter member key',
        'tooltip' => [
            'nokey' => 'Set up ad-free search',
            'empty' => 'Token used up. Recharge now.',
            'low' => 'Token soon used up. Recharge now.',
            'full' => 'Ad-free search enabled.',
        ],
    ],
    'placeholder' => 'MetaGer: Privacy Protected Search & Find',
    'searchbutton' => 'Start MetaGer-Search',
    'searchreset' => 'delete search query input',
    'foki' => [
        'web' => 'Web',
        'bilder' => 'Images',
        'nachrichten' => 'News',
        'science' => 'Science',
        'produkte' => 'Products',
        'maps' => 'Maps'
    ],
    'adfree' => 'MetaGer ad-free',
    'searchbar-replacement' => [
        'message' => 'The MetaGer search engine is now only available ad-free!',
        'why' => 'why?',
        'start' => 'Create a new Key',
        "key_error" => "The entered key was not valid. Please check the input.",
        "login_code_error" => "The entered login code was not valid. Hint: Login Codes are only valid while visible on another device!",
        "payment_id_error" => "You've entered a payment id which is not a correct key. Your key is 36 characters long.",
        "login" => "Login With Key"
    ]
];
