<?php
return [
    'title' => 'MetaGer - Hulp',
    'backarrow' => 'Terug',
    'app' => [
        'title' => 'Android-app <a title="For easy help, click here" href="/hilfe/easy-language/services#help-app" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Je kunt MetaGer ook als app gebruiken. Download gewoon de <a href="https://metager.de/app" target="_blank" rel="noopener">MetaGer App</a> op je Android smartphone.',
    ],
    'widget' => [
        'title' => 'MetaGer Widget <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-widget" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Dit is een codegenerator waarmee je MetaGer in je website kunt integreren. Je kunt het gebruiken om naar wens zoekopdrachten uit te voeren op je eigen site of op het internet. Gebruik voor vragen <a href="/kontakt/" target="_blank" rel="noopener">ons contactformulier</a>.',
    ],
    'maps' => [
        'title' => 'MetaGer Kaarten <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-maps" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Het behoud van privacy in het tijdperk van wereldwijde datagiganten heeft er ook toe geleid dat we <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a> hebben ontwikkeld: de (voor zover wij weten) enige routeplanner die volledige functionaliteit biedt via een browser en app zonder locaties van gebruikers op te slaan. Dit is allemaal controleerbaar omdat onze software open source is. Voor het gebruik van maps.metager.de raden we onze snelle app-versie aan. Je kunt onze apps downloaden van <a href="/app" target="_blank">hier</a> (of natuurlijk ook van de Play Store).',
        '2' => 'Deze kaartfunctie is ook toegankelijk vanuit de MetaGer zoekfunctie (en omgekeerd). Zodra je hebt gezocht naar een term in MetaGer, zie je een nieuwe zoekfocus \'Kaarten\' in de rechterbovenhoek. Door erop te klikken ga je naar de bijbehorende kaart.',
        '3' => 'Bij het laden toont de kaart de punten (POI\'s = Points of Interest) gevonden door MetaGer, die ook in de rechterkolom staan. Bij het zoomen past deze lijst zich aan de kaartsectie aan. Als je met je muis over een marker op de kaart of in de lijst beweegt, wordt het overeenkomstige item gemarkeerd. Klik op \'Details\' voor meer informatie over dat punt uit de onderstaande database.',
    ],
    'easy-help' => 'Door op het symbool <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> te klikken, krijg je toegang tot een vereenvoudigde versie van de Help.',
    'services' => [
        'text' => "Aanvullende diensten rond het zoeken",
    ],
];
