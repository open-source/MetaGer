<?php
return [
    'title' => 'MetaGer - Hulp',
    'backarrow' => 'Terug',
    'urls' => [
        'explanation' => 'Je kunt zoekresultaten uitsluiten die specifieke woorden bevatten in hun resultaatlinks door "-url:" te gebruiken in je zoekopdracht.',
        'example_b' => '<i>mijn zoekopdracht</i> -url:hond',
        'example_a' => 'Voorbeeld: Je wilt resultaten uitsluiten waarin het woord "hond" voorkomt in de resultaatkoppeling:',
        'title' => 'URL\'s uitsluiten',
    ],
    'bang' => [
        'title' => 'Instellingen <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-settings" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'MetaGer ondersteunt in beperkte mate een schrijfstijl die vaak \'!bang\' syntax wordt genoemd.<br>Een \'!bang\' begint altijd met een uitroepteken en bevat geen spaties. Voorbeelden zijn \'!twitter\' of \'!facebook\'.<br>Wanneer een ondersteunde \'!bang\' wordt gebruikt in de zoekopdracht, verschijnt er een vermelding in onze sneltips, zodat u met een druk op de knop verder kunt zoeken bij de betreffende service (Twitter of Facebook).',
        '2' => 'Waarom worden !bangs niet direct geopend?',
        '3' => 'De !bang "redirects" maken deel uit van onze snelle tips en vereisen een extra "klik". Dit was een moeilijke beslissing voor ons, omdat het de !bangs minder nuttig maakt. Het is echter helaas noodzakelijk omdat de links waarnaar wordt omgeleid niet van ons afkomstig zijn, maar van een derde partij, DuckDuckGo.<p>We zorgen er altijd voor dat onze gebruikers de controle behouden. Daarom beschermen we op twee manieren: Ten eerste wordt de ingevoerde zoekterm nooit doorgegeven aan DuckDuckGo, alleen de !bang. Ten tweede bevestigt de gebruiker expliciet het bezoek aan het !bang-doel. Helaas kunnen we momenteel om personeelsredenen niet al deze !bangs zelf controleren of onderhouden.',
    ],
    'selist' => [
        'title' => 'MetaGer toevoegen aan de zoekmachinelijst van je browser <a title="For easy help, click here" href="/hilfe/easy-language/functions#eh-selist"><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'explanation_b' => 'Sommige browsers vereisen dat je een URL invoert; dit moet "https://metager.de/meta/meta.ger3?input=%s" zijn zonder aanhalingstekens. Je kunt de URL zelf genereren door iets op te zoeken met metager.de en dan wat achter "input=" staat in de adresbalk te vervangen door %s. Als u nog steeds problemen ondervindt, neem dan contact met ons op: <a href="/kontalt" target="_blank" rel="noopener">Contactformulier</a>',
        'explanation_a' => 'Probeer eerst de huidige plugin te installeren. Om te installeren klik je gewoon op de link direct onder het zoekvak. Je browser zou daar al gedetecteerd moeten zijn.',
    ],
    'key' => [
        '5' => 'Scan QR Code <br>Als alternatief kunt u ook de QR-code scannen die wordt weergegeven op de <a href = "/keys/key/enter">beheerpagina</a> om in te loggen met een ander apparaat.',
        '6' => 'MetaGer sleutel handmatig invoeren <br>U kunt de sleutel ook handmatig invoeren op een ander apparaat.',
        'colors' => [
            '1' => 'Om gemakkelijk te herkennen of je reclamevrij zoekt, hebben we onze belangrijkste symboolkleuren gegeven. Hieronder vind je uitleg over de bijbehorende kleuren:',
            'grey' => 'Grijs: Je hebt geen sleutel ingesteld. U gebruikt de gratis zoekfunctie.',
            'red' => 'Rood: Als je sleutelsymbool rood is, betekent dit dat deze sleutel leeg is. Je hebt alle advertentievrije zoekopdrachten opgebruikt. Je kunt de sleutel opnieuw opladen op de sleutelbeheerpagina.',
            'green' => 'Groen: Als je sleutelsymbool groen is, gebruik je een opgeladen sleutel.',
            'yellow' => 'Geel: Als je een gele sleutel ziet, heb je nog een saldo van 30 tokens. Je zoekopdrachten raken op. Het is aan te raden om de sleutel snel op te laden.',
            'title' => 'Gekleurde MetaGer-sleutel',
        ],
        'title' => 'MetaGer-sleutel toevoegen <a title="For easy help, click here" href="/hilfe/easy-language/functions#eh-keyexplain"><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'De MetaGer sleutel wordt automatisch ingesteld in je browser en gebruikt. Je hoeft verder niets te doen. Als je de MetaGer-sleutel op andere apparaten wilt gebruiken, zijn er verschillende manieren om de MetaGer-sleutel in te stellen:',
        '2' => 'Inlogcode <br>Op de <a href = "/keys/key/enter">beheerpagina</a> van de MetaGer sleutel, kun je de inlogcode gebruiken om je sleutel toe te voegen aan een ander apparaat. Voer gewoon de zescijferige cijfercode in wanneer je inlogt. De inlogcode kan maar één keer worden gebruikt en is alleen geldig zolang het venster geopend is.',
        '3' => 'URL kopiëren <br>Wanneer je op de <a href = "/keys/key/enter">beheerpagina</a> van de MetaGer sleutel bent, is er een optie om een URL te kopiëren. Deze URL kan worden gebruikt om alle MetaGer instellingen, inclusief de MetaGer sleutel, op te slaan op een ander apparaat.',
        '4' => 'Bestand opslaan <br>Wanneer je op de <a href = "/keys/key/enter">beheerpagina</a> van de MetaGer sleutel bent, is er een optie om een bestand op te slaan. Dit slaat je MetaGer sleutel op als een bestand. Je kunt dit bestand dan op een ander apparaat gebruiken om in te loggen met je sleutel.',
    ],
    'multiwordsearch' => [
        'title' => 'Zoeken op meerdere woorden <a title="For easy help, click here" href="/hilfe/easy-language/functions#eh-severalwords"><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '4' => [
            'example' => '"de ronde tafel"',
            'text' => "Met een zinsdeelzoekopdracht kun je zoeken naar woordcombinaties in plaats van individuele woorden. Zet de woorden die samen moeten voorkomen gewoon tussen aanhalingstekens.",
        ],
        '3' => [
            'example' => '"de" "ronde" "tafel"',
            'text' => "Als je ervoor wilt zorgen dat woorden uit je zoekopdracht ook in de resultaten verschijnen, moet je ze tussen aanhalingstekens zetten.",
        ],
        '2' => "Als dit niet genoeg voor je is, heb je 2 opties om nauwkeuriger te zoeken:",
        '1' => "Als je zoekt naar meer dan één woord in MetaGer, proberen we automatisch resultaten te geven waar alle woorden in voorkomen of zo dicht mogelijk in de buurt komen.",
    ],
    'exactsearch' => [
        'title' => 'Exact zoeken <a title="For easy help, click here" href="/hilfe/easy-language/functions#exactsearch"><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => "Als je een specifiek woord wilt vinden in de MetaGer zoekresultaten, kun je dat woord vooraf laten gaan door een plusteken. Als je een plusteken en aanhalingstekens gebruikt, wordt een zin doorzocht precies zoals je hem hebt ingevoerd.",
        '2' => "Voorbeeld: S",
        '3' => 'Voorbeeld: ',
        'example' => [
            '1' => "+voorbeeldwoord",
            '2' => '+"voorbeeldzin".',
        ],
    ],
    'searchfunction' => [
        'title' => "Zoekfuncties",
    ],
    'stopwords' => [
        'title' => 'Stopwoorden <a title="For easy help, click here" href="/hilfe/easy-language/functions#eh-stopwordsearch"><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '3' => "auto nieuw -bmw",
        '2' => "Voorbeeld: U bent op zoek naar een nieuwe auto, maar zeker geen BMW. Uw input zou zijn:",
        '1' => "Als je zoekresultaten in MetaGer wilt uitsluiten die specifieke woorden bevatten (uitsluitingswoorden / stopwoorden), kun je dit doen door deze woorden vooraf te laten gaan door een minteken.",
    ],
    'easy-help' => 'Door op het symbool <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> te klikken, krijg je toegang tot een vereenvoudigde versie van de Help.',
];
