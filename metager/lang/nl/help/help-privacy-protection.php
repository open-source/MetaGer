<?php
return [
    'title' => 'MetaGer - Hulp',
    'backarrow' => 'Terug',
    'tor' => [
        'title' => 'Verborgen TOR-service <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-torhidden" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Al vele jaren verbergt MetaGer IP-adressen en slaat ze niet op. Deze adressen zijn echter tijdelijk zichtbaar op de MetaGer server terwijl een zoekopdracht wordt uitgevoerd: als MetaGer in gevaar zou komen, zou een aanvaller je adressen kunnen lezen en opslaan. Om aan de hoogste veiligheidseisen te voldoen, hebben we een MetaGer-instantie op het Tor-netwerk: de MetaGer TOR Hidden Service - toegankelijk via: <a href="/tor/" target="_blank" rel="noopener">https://metager.de/tor/</a>. Om het te gebruiken heb je een speciale browser nodig, die je kunt downloaden van <a href="https://www.torproject.org/" target="_blank" rel="noopener">https://www.torproject.org/</a>.',
        '2' => 'Je kunt toegang krijgen tot MetaGer in de Tor browser op: http://metagerv65pwclop2rsfzg4jwowpavpwd6grhhlvdgsswvo6ii4akgyd.onion .',
    ],
    'proxy' => [
        'title' => 'MetaGer proxyserver anonimiseren <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-proxy" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Om het te gebruiken hoef je alleen maar te klikken op \'OPEN ANONYMOUSLY\' onderaan het resultaat op de resultatenpagina van MetaGer. Je verzoek wordt dan naar de doelwebsite geleid via onze anonimiserende proxyserver en je persoonlijke gegevens blijven volledig beschermd. Belangrijk: als je vanaf dit punt links op de pagina\'s volgt, blijf je beschermd door de proxy. U kunt echter geen nieuw adres invoeren in het adresveld bovenaan. In dat geval verlies je de bescherming. Je kunt zien of je nog steeds beschermd bent in het adresveld, dat weergeeft: https://proxy.suma-ev.de/?url=here is het huidige adres.',
    ],
    'content' => [
        'title' => 'Bedenkelijke inhoud / Jeugdbescherming <a title="For easy help, click here" href="/help/easy-language/privacy-protection#eh-content" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'explanation' => [
            '1' => 'Ik heb "hits" ontvangen die ik niet alleen vervelend vind, maar die naar mijn mening ook illegale inhoud bevatten!',
            '2' => 'Als je iets op internet vindt dat je illegaal of schadelijk voor minderjarigen vindt, kun je per e-mail contact opnemen met <a href="mailto:hotline@jugendschutz.net" target="_blank" rel="noopener">hotline@jugendschutz.net</a> of ga naar <a href="http://www.jugendschutz.net/" target="_blank" rel="noopener">www.jugendschutz.net</a> en vul het klachtenformulier in dat daar beschikbaar is. Het is handig om kort aan te geven wat je ontoelaatbaar vindt en hoe je aan deze inhoud bent gekomen. Je kunt twijfelachtige inhoud ook rechtstreeks aan ons melden. Stuur hiervoor een e-mail naar onze jeugdbeschermingsmedewerker (<a href="mailto:jugendschutz@metager.de" target="_blank" rel="noopener">jugendschutz@metager.de</a>).',
        ],
    ],
    'maps' => [
        'title' => 'MetaGer Kaarten',
        '1' => 'Het behoud van privacy in het tijdperk van wereldwijde datagiganten heeft er ook toe geleid dat we <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a> hebben ontwikkeld: de (voor zover wij weten) enige routeplanner die volledige functionaliteit biedt via een browser en app zonder locaties van gebruikers op te slaan. Dit is allemaal controleerbaar omdat onze software open source is. Voor het gebruik van maps.metager.de raden we onze snelle app-versie aan. Je kunt onze apps downloaden van <a href="/app" target="_blank">hier</a> (of natuurlijk ook van de Play Store).',
        '2' => 'Deze kaartfunctie is ook toegankelijk vanuit de MetaGer zoekfunctie (en omgekeerd). Zodra je hebt gezocht naar een term in MetaGer, zie je een nieuwe zoekfocus \'Kaarten\' in de rechterbovenhoek. Door erop te klikken ga je naar de bijbehorende kaart.',
        '3' => 'Bij het laden toont de kaart de punten (POI\'s = Points of Interest) gevonden door MetaGer, die ook in de rechterkolom staan. Bij het zoomen past deze lijst zich aan de kaartsectie aan. Als je met je muis over een marker op de kaart of in de lijst beweegt, wordt het overeenkomstige item gemarkeerd. Klik op \'Details\' voor meer informatie over dat punt uit de onderstaande database.',
    ],
    'easy-help' => 'Door op het symbool <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> te klikken, krijg je toegang tot een vereenvoudigde versie van de Help.',
    'privacy' => [
        'title' => "Anonimiteit en gegevensbeveiliging",
        '1' => 'Traceercookies, sessie-ID\'s en IP-adressen <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-tracking" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '2' => 'Niets hiervan wordt gebruikt, opgeslagen, bewaard of anderszins verwerkt hier bij MetaGer (uitzondering: kortstondige opslag voor bescherming tegen hacken en botaanvallen). Omdat we dit onderwerp erg belangrijk vinden, hebben we ook manieren gecreëerd om je te helpen het hoogste niveau van beveiliging te bereiken: de MetaGer TOR Hidden Service en onze anonimiserende proxyserver.',
        '3' => "Hieronder vind je meer informatie. De functies zijn toegankelijk onder 'Services' in de navigatiebalk.",
    ],
];
