<?php
return [
    'backarrow' => 'Terug',
    'title' => [
        '2' => 'De hoofdpagina\'s gebruiken',
        '1' => 'MetaGer - Hulp',
    ],
    'startpage' => [
        'title' => 'De startpagina <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-startpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'De startpagina bevat het zoekveld, een knop in de rechterbovenhoek om het menu te openen en twee links onder het zoekveld om MetaGer aan je browser toe te voegen en advertentievrij te zoeken. In het onderste gedeelte vind je informatie over MetaGer en de SUMA-EV vereniging. Bovendien worden onze focusgebieden <i>Gegarandeerde privacy, Non-profit vereniging, Divers & gratis</i> en <i>100% groene energie</i> onderaan weergegeven. Door op de betreffende onderdelen te klikken of door te scrollen kun je meer informatie vinden. ',
    ],
    'searchfield' => [
        'title' => 'Het zoekveld <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#eh-searchfield" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'Het zoekveld bestaat uit verschillende delen:',
        'memberkey' => 'het sleutelsymbool: Hier kun je je sleutel invoeren om reclamevrij zoeken te gebruiken. U kunt ook uw tokensaldo bekijken en uw sleutel beheren.',
        'slot' => 'het zoekveld: Voer hier je zoekterm in. Hoofdletters en kleine letters worden niet onderscheiden.',
        'search' => 'het vergrootglas: Start uw zoekopdracht door hier te klikken of op "Enter" te drukken.',
        'morefunctions' => 'Extra functies zijn te vinden onder het menu-item "<a href = "/hilfe/funktionen">Zoekfuncties</a>".',
    ],
    'resultpage' => [
        'title' => 'De pagina met resultaten <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-resultpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'foci' => 'Onder het zoekveld zijn er 3 verschillende zoekfocus (Web, Afbeeldingen, Nieuws), die elk zijn gekoppeld aan specifieke zoekmachines.',
        'choice' => 'Hieronder zie je twee items: "Filter" en "Instellingen" indien van toepassing.',
        'filter' => 'Filter: Hier kun je filteropties tonen en verbergen en filters toepassen. In elke zoekfocus heb je verschillende selectieopties. Sommige functies zijn alleen beschikbaar als je een MetaGer-toets gebruikt.',
        'settings' => 'Instellingen: Hier kun je permanente zoekinstellingen maken voor je MetaGer-zoekopdracht in de huidige focus. Je kunt ook zoekmachines selecteren en deselecteren die gekoppeld zijn aan de focus. Je instellingen worden opgeslagen met behulp van een niet-persoonlijk identificeerbare plaintext cookie. Je kunt de instellingenpagina ook openen via het menu in de rechterbovenhoek.',
    ],
    'result' => [
        'info' => [
            'open' => '"OPENEN: Klik op de kop of de link hieronder (de URL) om het resultaat in hetzelfde tabblad te openen.',
            'newtab' => '"OPEN IN NIEUW TAB" opent het resultaat in een nieuw tabblad. Je kunt ook een nieuw tabblad openen met CTRL en de linkermuisknop of de middelste muisknop.',
            'anonym' => '"OPEN ANONYMOUSLY" betekent dat het resultaat wordt geopend onder de bescherming van onze proxy. Meer informatie hierover kan worden gevonden in de sectie <a href = "/hilfe/datensicherheit#h-proxy">MetaGer Proxy Server</a> anonimiseren.',
            'more' => '<img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>: Als je op <img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/> klikt, krijg je nieuwe opties; het uiterlijk van het resultaat verandert:',
            '2' => 'De nieuwe opties zijn:',
            'domainnewsearch' => '"Start een nieuwe zoekopdracht op dit domein": Er wordt een meer gedetailleerde zoekopdracht uitgevoerd op het domein van het resultaat.',
            'hideresult' => '"verbergen": Hiermee kun je resultaten van dit domein verbergen. Je kunt deze schakeloptie ook direct achter je zoekterm zetten en aan elkaar rijgen; een "*" jokerteken is ook toegestaan. Zie ook Instellingen voor een permanente oplossing.',
            '1' => 'Alle resultaten worden gepresenteerd in de volgende indeling:',
        ],
        'title' => 'Resultaten <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-results" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    'settings' => [
        'title' => 'Instellingen <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-settings" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '2' => 'Gebruikte zoekmachines <br> Hier kunt u de zoekmachines die u gebruikt bekijken en aanpassen. Door op de betreffende naam te klikken, kunt u deze in- of uitschakelen.',
        '3' => 'Zoekfilters <br> Met zoekfilters kunt u uw zoekopdracht permanent filteren.',
        '4' => 'Zwarte lijst <br> Hier kunt u een persoonlijke zwarte lijst maken. U kunt deze gebruiken om specifieke domeinen uit te filteren en uw eigen zoekinstellingen te maken. Door op "Toevoegen" te klikken, worden deze instellingen toegevoegd aan de link in de "Opmerking" sectie.',
        '5' => 'Schakel naar donkere modus <br> Schakel hier eenvoudig naar donkere modus.',
        '6' => 'Resultaten openen in nieuw tabblad <br> Hier kunt u de functie om resultaten in een nieuw tabblad te openen permanent inschakelen.',
        '7' => 'Citaten <br> Hier kunt u de weergave van citaten in- en uitschakelen.',
        '1' => 'Advertentievrij zoeken <br> Hier kun je het saldo van je sleutel en je sleutel bekijken. Je hebt ook de opties om je sleutel op te waarderen of te verwijderen.',
        '8' => 'Alle huidige instellingen herstellen <br> Er wordt een link weergegeven die u kunt instellen als startpagina of bladwijzer om uw huidige instellingen te behouden.',
        '9' => 'Subtiele reclame voor onze eigen diensten <br> We tonen u subtiele reclame voor onze eigen diensten. U kunt onze zelfpromotie hier uitschakelen.',
    ],
    'easy-help' => 'Door op het symbool <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> te klikken, krijg je toegang tot een vereenvoudigde versie van de Help.',
];
