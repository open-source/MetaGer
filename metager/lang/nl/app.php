<?php
return [
    'metager' => [
        '1' => 'Deze app brengt de volledige Metager-kracht naar je smartphone. Doorzoek het web met één aanraking en behoud je privacy.',
        '2' => 'Er zijn twee manieren om onze App te krijgen: installeer hem via de Google Playstore of (beter voor je privacy) haal hem direct van onze server.',
        'fdroid' => 'F-Droid winkel',
        'manuell' => 'Handmatige installatie',
        'playstore' => 'Google-winkel',
    ],
    'maps' => [
        '1' => 'Deze app biedt een native integratie van <a href="https://maps.metager.de" target="_blank">MetaGer Maps</a> (mogelijk gemaakt door <a href="https://www.openstreetmap.de/" target="_blank">Openstreetmap</a>) op je mobiele Android-toestel.',
        '2' => 'Daarom werkt de routeplanner en de navigatieservice erg snel op je smartphone. De app is sneller vergeleken met het gebruik in een mobiele webbrowser. En er zijn nog meer voordelen - bekijk het zelf!',
        '3' => 'De APK voor handmatige installatie is ongeveer 4x zo groot als de installatie in de Playstore (~250MB) omdat het bibliotheken bevat voor alle gangbare CPU-architecturen. De geïntegreerde updater kent de CPU-architectuur van je apparaat en installeert de juiste (kleine) versie van de app bij de eerste update. Als je de architectuur van je apparaat zelf kent, kun je ook <a href="https://gitlab.metager.de/metagermaps/android/-/releases/permalink/latest" target="_blank">direct het kleine pakket</a> installeren.',
        'list' => [
            '1' => 'Toegang tot plaatsbepalingsgegevens => Met geactiveerde GPS kunnen we betere zoekresultaten bieden. Hiermee krijg je toegang tot de stap-voor-stap navigatie. <b> Natuurlijk slaan we je gegevens niet op en geven we je gegevens niet door aan derden.</b>',
            '2' => 'De APK voor handmatige installatie heeft een geïntegreerde updater. Om het updateprogramma te laten werken, vraagt de app toestemming om meldingen te plaatsen om je op de hoogte te stellen van een beschikbare update en gebruikt hij de Android-toestemming REQUEST_INSTALL_PACKAGES zodat hij je kan vragen om de update van de app te installeren.',
        ],
        '4' => 'Na de eerste start wordt om de volgende rechten gevraagd:',
    ],
    'head' => [
        '1' => 'Metager Apps',
        '2' => 'MetaGer-app',
        '3' => 'MetaGer Kaarten App',
        '4' => 'Installatie',
    ],
    'disclaimer' => [
        '1' => 'Op dit moment hebben we alleen een Android-versie van onze app.',
    ],
];
