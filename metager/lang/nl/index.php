<?php
return [
    'plugin' => 'MetaGer installeren',
    'plugin-title' => 'MetaGer aan uw browser toevoegen',
    'key' => [
        'placeholder' => 'Lidmaatschapsleutel invoeren',
        'tooltip' => [
            'nokey' => 'Advertentievrij zoeken instellen',
            'empty' => 'Fiche opgebruikt. Nu herladen.',
            'low' => 'Muntje snel opgebruikt. Nu herladen.',
            'full' => 'Zoeken zonder advertenties ingeschakeld.',
        ],
    ],
    'foki' => [
        'web' => 'Web',
        'bilder' => 'Afbeeldingen',
        'nachrichten' => 'Nieuws',
        'science' => 'Wetenschap',
        'produkte' => 'Producten',
        'maps' => 'Kaarten',
    ],
    'placeholder' => 'MetaGer: Beschermd zoeken en vinden',
    'searchbutton' => 'MetaGer zoeken starten',
    'adfree' => 'Gebruik MetaGer reclamevrij',
    'skip' => [
        'search' => 'Doorgaan naar invoer zoekopdracht',
        'navigation' => 'Ga naar navigatie',
        'fokus' => 'Doorgaan naar selectie van zoekfocus',
    ],
    'lang' => 'Witch-taal',
    'searchreset' => 'invoer zoekopdracht verwijderen',
    'searchbar-replacement' => [
        'login' => 'Inloggen met sleutel',
        'start' => 'Een nieuwe sleutel maken',
        'message' => 'De MetaGer zoekmachine is nu alleen advertentie-vrij beschikbaar!',
        'why' => 'waarom?',
        'key_error' => "De ingevoerde sleutel is ongeldig. Controleer de invoer.",
        'login_code_error' => "De ingevoerde inlogcode was niet geldig. Tip: Inlogcodes zijn alleen geldig als ze zichtbaar zijn op een ander apparaat!",
        'payment_id_error' => "Je hebt een betalings-id ingevoerd die geen correcte sleutel is. Je sleutel is 36 tekens lang.",
    ],
];
