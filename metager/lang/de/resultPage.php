<?php
return [
    'opensearch' => 'MetaGer: Sicher suchen & finden',
    'startseite' => 'MetaGer-Startseite',
    'impressum' => 'Impressum',
    'search-placeholder' => 'Suchbegriff(e) eingeben',
    'metager3' => 'Sie befinden sich auf einer MetaGer Testversion.',
    'engines' => [
        'queried' => "Abgefragte Suchdienste",
        'disabled' => "Suchdienste zur Abfrage hinzufügen",
        'payment_required' => "Suchdienste verfügbar mit <a href=':link'>MetaGer Schlüssel</a>",
    ],
    'skiplinks' => [
        'heading' => 'Schnelles Springen zum Inhalt',
        'results' => 'Zu den Suchergebnissen springen',
        'query' => 'Sprung zum Eingabefeld für die Suchanfrage',
        'settings' => 'Zu den Sucheinstellungen springen',
        'navigation' => 'Zur Navigation springen',
        'return' => 'Sie können jederzeit zu diesem Menü zurückkehren, indem Sie die Escape-Taste drücken',
    ],
];
