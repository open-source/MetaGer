<?php
return [
    'default-search-v15' => [
        //Instructions for Edge version 15-17
        '1' => 'Klicken Sie oben rechts im Browser auf "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">" und öffnen Sie die "Einstellungen".',
        '2' => 'Scrollen Sie nach unten und klicken auf "Erweiterte Einstellungen".',
        '3' => 'Scrollen Sie erneut nach unten bis zum Punkt "In Adressleiste suchen mit" und klicken Sie auf "Ändern".',
        '4' => 'Wählen Sie "MetaGer: Sicher suchen & finden..." und klicken Sie auf "Als Standard".',
    ],
    'default-search-v18' => [
        //Instructions for Edge version 18
        '1' => 'Klicken Sie oben rechts im Browser auf "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">" und öffnen Sie die "Einstellungen".',
        '2' => 'Wählen Sie den Reiter "Erweitert".',
        '3' => 'Scrollen Sie nach unten bis zum Punkt "In Adressleiste suchen mit" und klicken Sie auf "Ändern".',
        '4' => 'Wählen Sie "MetaGer: Sicher suchen & finden..." und klicken Sie auf "Als Standard".',
    ],
    'default-search-v80' => [
        //Instructions for Edge version >= 80
        '1' => 'Öffnen Sie einen neuen Tab und geben Sie "edge://settings/searchEngines" in der Adressleiste ein, um in die Suchmaschineneinstellungen zu gelangen.',
        '2' => 'Klicken Sie neben dem Eintrag von MetaGer auf "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">" und wählen Sie "Als Standard".',
    ],
    'default-page-v15' => [
        '1' => 'Klicken Sie oben rechts im Browser auf "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">" und öffnen Sie die "Einstellungen".',
        '2' => 'Wählen Sie unter "Startseite festlegen" bzw. "Edge öffnen mit" "eine bestimmte Seite" und tragen dort ":link" ein.',
        '3' => 'Klicken Sie auf <img class= "mg-icon" src="/img/floppy.svg">, um MetaGer als Standardsuchmaschine zu speichern.',
    ],
    'default-page-v80' => [
        '1' => 'Geben Sie "edge://settings/onStartup" in die Adressleiste ein, um in die Einstellungen "Beim Start" zu gelangen.',
        '2' => 'Wählen Sie "Bestimmte Seite oder Seiten öffnen" und tragen ":link" als URL bei "Neue Seite hinzufügen" ein.',
        '3' => 'Hinweis: Alle hier sichtbaren Webseiten werden nun beim Start des Browsers geöffnet. Sie können Einträge entfernen, indem Sie mit der Maus darüber fahren und rechts auf "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">" klicken.',
    ],
    'plugin' => 'Wenn Sie unsere <a href="https://microsoftedge.microsoft.com/addons/detail/fdckbcmhkcoohciclcedgjmchbdeijog" target="_blank" rel="noopener">Browsererweiterung</a> installieren, wird MetaGer automatisch als Standardsuchmaschine installiert. Wahrscheinlich müssen Sie die Erweiterung nach der Installation manuell in den Browsereinstellungen aktivieren.',
];
