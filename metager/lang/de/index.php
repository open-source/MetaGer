<?php
return [
    'plugin' => 'MetaGer Installieren',
    'plugin-title' => 'MetaGer-Plugin hinzufügen',
    'key' => [
        'placeholder' => 'Schlüssel für werbefreie Suche eingeben',
        'tooltip' => [
            'nokey' => 'Werbefreie Suche neu einrichten',
            'empty' => 'Token aufgebraucht. Jetzt aufladen.',
            'low' => 'Token bald aufgebraucht. Jetzt aufladen.',
            'full' => 'Werbefreie Suche aktiviert.',
        ],
    ],
    'placeholder' => 'MetaGer - Mehr als eine Suchmaschine',
    'searchbutton' => 'MetaGer-Suche starten',
    'foki' => [
        'web' => 'Web',
        'bilder' => 'Bilder',
        'nachrichten' => 'Nachrichten',
        'science' => 'Wissenschaft',
        'produkte' => 'Produkte',
        'maps' => 'Maps',
    ],
    'adfree' => 'MetaGer werbefrei nutzen',
    'skip' => [
        'search' => 'Weiter zur Eingabe der Suchanfrage',
        'navigation' => 'Zur Navigation springen',
        'fokus' => 'Zur Auswahl des Suchfokus springen',
    ],
    'lang' => 'Hexensprache',
    'searchreset' => 'Eingabe der Suchanfrage löschen',
    'searchbar-replacement' => [
        'why' => 'Warum?',
        'message' => 'Die Suchmaschine MetaGer ist jetzt nur noch werbefrei verfügbar!',
        'start' => 'Neuen Schlüssel erstellen',
        'key_error' => "Der eingegebene Schlüssel war nicht korrekt. Bitte prüfen Sie die Eingabe.",
        'login' => "Mit Schlüssel Anmelden",
        'login_code_error' => "Der eingegebene Login-Code war nicht gültig. Hinweis: Login-Codes sind nur gültig, solange sie auf einem anderen Gerät angezeigt werden!",
        'payment_id_error' => "Sie haben eine Zahlungskennung eingegeben, die kein korrekter Schlüssel ist. Ihr Schlüssel ist 36 Zeichen lang.",
    ],
];
