<?php
return [
    'app' => [
        '1' => "Sie können MetaGer auch als App verwenden. Laden Sie einfach die <a href=\"https://metager.de/app\" target=\"_blank\" rel=\"noopener\">MetaGer App</a> auf Ihr Android-Smartphone herunter.",
        'title' => 'Android-App <a title="Zur einfachen Hilfe" href="/hilfe/easy-language/services#help-app" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    'backarrow' => 'Zurück',
    'easy-help' => 'Durch Klicken auf das Symbol <a title="Zur einfachen Hilfe" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> kommen Sie zu einer einfacheren Version der Hilfe.',
    'services' => [
        'text' => "Weitere Dienste um die Suche herum",
    ],
    'maps' => [
        '1' => 'Der Schutz der Privatsphäre im Zeitalter globaler Datengiganten hat uns auch dazu veranlasst, <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a> zu entwickeln: der (unseres Wissens) einzige Routenplaner, der die volle Funktionalität über einen Browser und eine App bietet, ohne den Standort des Nutzers zu speichern. Das alles ist überprüfbar, denn unsere Software ist Open Source. Für die Nutzung von maps.metager.de empfehlen wir unsere schnelle App-Version. Sie können unsere Apps unter <a href="/app" target="_blank">hier</a> (oder natürlich auch im Play Store) herunterladen.',
        '2' => "Diese Kartenfunktion kann auch über die MetaGer-Suche aufgerufen werden (und umgekehrt). Wenn Sie in MetaGer nach einem Begriff gesucht haben, sehen Sie in der oberen rechten Ecke einen neuen Suchfokus \"Karten\". Wenn Sie diesen anklicken, gelangen Sie zu einer entsprechenden Karte.",
        '3' => "Die Karte zeigt beim Laden die von MetaGer gefundenen Punkte (POIs = Points of Interest) an, die auch in der rechten Spalte aufgelistet sind. Beim Zoomen passt sich diese Liste dem Kartenausschnitt an. Wenn Sie mit der Maus über einen Marker auf der Karte oder in der Liste fahren, wird der entsprechende Punkt hervorgehoben. Klicken Sie auf \"Details\", um weitere Informationen zu diesem Punkt aus der untenstehenden Datenbank zu erhalten.",
        'title' => 'MetaGer Maps <a title="Zur einfachen Hilfe" href="/hilfe/easy-language/services#eh-maps" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    'title' => 'MetaGer - Hilfe',
    'widget' => [
        '1' => "Dies ist ein Code-Generator, mit dem Sie MetaGer in Ihre Website einbinden können. Sie können damit auf Ihrer eigenen Website oder im Internet nach Belieben Suchen durchführen. Bei Fragen benutzen Sie bitte <a href=\"/kontakt/\" target=\"_blank\" rel=\"noopener\">unser Kontaktformular</a>.",
        'title' => 'MetaGer Widget <a title="Zur einfachen Hilfe" href="/hilfe/easy-language/services#eh-widget" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
];
