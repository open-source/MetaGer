<?php
return [
    'easy-help' => 'Durch Klicken auf das Symbol <a title="Zur einfachen Hilfe" href="/hilfe/easy-language/functions#bangs" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> kommen Sie zu einer einfacheren Version der Hilfe.',
    'backarrow' => 'Zurück',
    'result' => [
        'info' => [
            '1' => 'Alle Ergebnisse werden in folgender Form ausgegeben:',
            '2' => 'Die neuen Optionen sind:',
            'anonym' => '"OPEN ANONYMOUSLY" bedeutet, dass das Ergebnis unter dem Schutz unseres Proxys geöffnet wird. Einige Informationen dazu finden Sie im Abschnitt <a href = "/hilfe/datensicherheit#h-proxy">Anonymisierung MetaGer Proxy Server</a>.',
            'domainnewsearch' => '"Starten Sie eine neue Suche in diesem Bereich": Es wird eine detailliertere Suche in der Domäne des Ergebnisses durchgeführt.',
            'hideresult' => '"ausblenden": Hiermit blenden Sie Ergebnisse dieser Domain aus. Sie können diesen Schalter auch direkt hinter Ihr Suchwort schreiben und auch verketten; ebenso ist ein "*" als Wildcard erlaubt. Siehe auch Einstellungen für eine dauerhafte Lösung.',
            'more' => '<img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt= "Mehr"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt= "Mehr"/>: Wenn Sie auf<img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt= "Mehr"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt= "Mehr"/>klicken, dann erhalten Sie neue Optionen; das Aussehen des Ergebnisses ändert sich:',
            'newtab' => '"IN NEUEM TAB ÖFFNEN" öffnet das Ergebnis in einem neuen Tab. Alternativ können Sie auch ein neues Tab öffnen, indem Sie STRG und Linksklick oder die mittlere Maustaste verwenden.',
            'open' => '"Zum Öffnen": Klicken Sie auf die Überschrift oder den darunter stehenden Link (die URL), um das Ergebnis im selben Tab zu öffnen.',
        ],
        'title' => 'Ergebnisse <a title="Zur einfachen Hilfe" href="/hilfe/easy-language/mainpages#help-results" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    'resultpage' => [
        'choice' => 'Darunter sehen Sie zwei Punkte: "Filter" und "Einstellungen", falls zutreffend.',
        'filter' => 'Filter: Hier können Sie Filtermöglichkeiten ein- und ausblenden und Filter anwenden. In jedem Suchfokus haben Sie hier andere Auswahlmöglichkeiten. Einige Funktionen sind nur mit Nutzung eines MetaGer Schlüssels möglich.',
        'foci' => 'Unter dem Suchfeld sind 6 verschiedene Suchfokusse (drei im englischsprachigen Bereich) (Web, Bilder…...) abgebildet, denen intern auch spezifische Suchmaschinen zugeordnet sind.',
        'settings' => 'Einstellungen: Hier können Sie dauerhafte Sucheinstellungen für Ihre MetaGer-Suche im aktuellen Fokus vornehmen. Sie können auch Suchmaschinen, die mit dem Fokus verbunden sind, auswählen und abwählen. Ihre Einstellungen werden in einem nicht persönlich identifizierbaren Klartext-Cookie gespeichert. Sie können die Einstellungsseite auch über das Menü in der oberen rechten Ecke aufrufen.',
        'title' => 'Die Ergebnisseite <a title="Zur einfachen Hilfe" href="/hilfe/easy-language/mainpages#help-resultpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    'searchfield' => [
        'info' => 'Das Suchfeld besteht aus mehreren Teilen:',
        'memberkey' => 'dem Schlüsselsymbol: Hier können Sie Ihren Schlüssel eingeben, um die werbefreie Suche nutzen zu können. Zusätzlich können Sie Ihren Tokenstand einsehen und Ihren Schlüssel verwalten.',
        'morefunctions' => 'Weitere Funktionen finden sie unter dem Menüpunkt "<a href = "/hilfe/funktionen">Suchfunktionen</a>".',
        'search' => 'das Vergrößerungsglas: Starten Sie Ihre Suche, indem Sie hier klicken oder "Enter" drücken.',
        'slot' => 'dem Suchfeld: Geben Sie hier Ihren Suchbegriff ein. Zwischen Groß- und Kleinschreibung wird nicht unterschieden.',
        'title' => 'Das Suchfeld <a title="Zur einfachen Hilfe" href="/hilfe/easy-language/mainpages#eh-searchfield" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    'settings' => [
        '1' => 'Werbefreie Suche <br> Hier können Sie das Guthaben Ihres Schlüssels, sowie Ihren Schlüssel einsehen. Sie haben ebenfalls die Optionen, Ihren Schlüssel aufzuladen oder zu entfernen.',
        '2' => 'Verwendete Suchmaschinen <br> Hier können Sie die Suchmaschinen, die Sie verwenden, anzeigen und anpassen. Mit einem Klick auf den entsprechenden Namen können Sie diese entsprechend aktivieren oder deaktivieren.',
        '3' => 'Suchfilter <br> Mit Suchfiltern können Sie Ihre Suche permanent filtern.',
        '4' => 'Schwarze Liste <br> Hier können Sie eine persönliche schwarze Liste erstellen. Damit können Sie bestimmte Domains herausfiltern und Ihre eigenen Sucheinstellungen vornehmen. Wenn Sie auf "Hinzufügen" klicken, werden diese Einstellungen an den Link im Abschnitt "Hinweis" angehängt.',
        '5' => 'Umschalten in den dunklen Modus <br> Hier können Sie ganz einfach in den dunklen Modus wechseln.',
        '6' => 'Ergebnisse in neuem Tab öffnen <br> Hier können Sie die Funktion zum Öffnen der Ergebnisse in einem neuen Tab dauerhaft aktivieren.',
        '7' => 'Zitate <br> Hier können Sie die Anzeige von Zitaten ein- und ausschalten.',
        '8' => 'Wiederherstellen aller aktuellen Einstellungen <br> Es wird Ihnen ein Link angezeigt, den Sie als Startseite oder Lesezeichen einrichten können, um Ihre aktuell gesetzen Einstellungen beizubehalten.',
        '9' => 'Subtile Werbung für unseren eigenen Service <br> Wir zeigen Ihnen subtile Werbung für unsere eigenen Dienste. Hier können Sie unsere Eigenwerbung austellen.',
        'title' => 'Einstellungen <a title="Zur einfachen Hilfe" href="/hilfe/easy-language/mainpages#help-settings" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    'startpage' => [
        'info' => 'Die Startseite beinhaltet das Suchfeld, oben rechts einen Button zum Aufrufen des Menus und unter dem Suchfeld zwei Links, mit dem Sie MetaGer zu Ihrem Browser hinzufügen und MetaGer werbefrei nutzen können. Im unteren Bereich finden Sie Informationen zu MetaGer und dem Trägerverein SUMA-EV. Zusätzlich werden unten unsere Schwerpunkte <i>Garantierte Privatsphäre, Gemeinnütziger Verein, Vielfältig & Frei</i> und <i>100% Ökostrom</i> angezeigt. Durch einen Klick auf die entsprechenden Bereiche oder durch Scrollen können mehr Informationen gefunden werden. ',
        'title' => 'Die Startseite <a title="Zur einfachen Hilfe" href="/hilfe/easy-language/mainpages#help-startpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    'title' => [
        '1' => 'MetaGer - Hilfe',
        '2' => 'Verwendung der Hauptseiten',
    ],
];
