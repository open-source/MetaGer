<?php

return [
    "title"                 => 'MetaGer - Einfache Hilfe',
    "backarrow"             => 'Zurück',
    'glossary'              => 'Durch Klicken auf das Symbol<a title="Dieses Symbol führt zum Glossar" href="/hilfe/easy-language/glossary" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>  kommen Sie zur Erklärung von den schwierigen Wörtern.',
    "title.2"               => 'Die Benutzung von den Haupt-Seiten',
    "startpage"  => [
        "title"       => 'Die Start-Seite',
        "info"  => [
            "1"        => 'Die Start-Seite hat viele Funktionen. <br>Oben rechts auf der Seite ist ein Knopf. <br> So sieht der Knopf aus.',
            "2"      => 'Sie können auf diesen Knopf drücken. <br> Dann sehen Sie ein Menü. <br> In diesem Menü können Sie viele verschiedene Sachen auswählen. ',
            "3"      => 'In der Mitte von der Start-Seite ist das Such-Feld. <br> Unter dem Such-Feld steht <strong>MetaGer installieren</strong>. <br> Sie können darauf klicken. <br> Damit können Sie MetaGer herunterladen. <br> Dann können Sie MetaGer als Standard-Such-Maschine<a title="Die Standard-Such-Maschine ist die Such-Maschine mit der Sie immer suchen. Für mehr Informationen klicken" href="/hilfe/easy-language/glossary#glstandardsearch" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> einstellen.',
            "4"      => 'Unten auf der Start-Seite sind bunte Felder. <br> Sie können auf diese Felder klicken. <br> Dort können Sie mehr über MetaGer erfahren.',
        ]
    ], 
    "searchfield"  => [
        "title"     => 'Das Such-Feld',
        "info"      => 'Das Such-Feld hat 3 Teile: ',
        "memberkey"  => [
            "1" => 'Das linke Feld:', 
            "2" => 'Links ist ein Schlüssel.<br> So sieht das linke Feld aus:',
            "3" => 'Sie können bei uns suchen, ohne Werbung zu sehen. <br> Dafür klicken Sie auf den Schlüssel. <br> Dann klicken Sie auf den Knopf <strong>Werbefreie Suche neu einrichten</strong>.<br> Danach klicken Sie auf den Button <strong>Schlüssel jetzt erstellen</strong>.<br> Dann bekommen Sie ein Passwort. <br> Das Passwort nennt man Schüssel. <br> Nun können Sie den Schlüssel für Geld mit Guthaben aufladen<br> Diesen Schlüssel können Sie in dem linken Feld eintragen. <br> Dann können Sie ohne Werbung suchen. <br> Mehr Infos finden sie hier: <a href = "/hilfe/easy-language/functions#eh-keyexplain">MetaGer Schlüssel</a>',
        ],
        "slot"  => [
            "1"      => 'Das mittlere Feld:',
            "2"      => 'In das mittlere Feld schreiben Sie das was Sie suchen wollen. <br> Es ist egal, ob Sie die Wörter GROß oder klein schreiben.<br> Das mittlere Feld sieht so aus:',
        ],
        "search"  => [
            "1"    => 'Das rechte Feld:',
            "2"    => 'Das rechte Feld zeigt eine Lupe.',
            "3"    => 'Wenn Sie auf das Feld drücken, starten Sie die Suche. <br> Sie können auch auf Ihrer Tastatur den Knopf ENTER drücken.',
        ],
        "morefunctions" => 'Bei <a href = "/hilfe/easy-language/functions">Such-Funktionen</a> finden Sie mehr über das Thema <strong>Suche</strong>.',
        ], 
    "resultpage"  => [
        "title"      => 'Die Ergebnis-Seite',
        "foci"  => [
            "1"       => 'Unter dem Such-Feld gibt es eine Zeile mit mehreren Knöpfen. <br>Das sind verschiedene Such-Kategorien<a title="Ergebnisse werden sortiert, damit man sie leichter findet. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glsearchcategories" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>. <br> So sieht die Zeile aus:',
            "2"      => 'Wenn Sie auf diese Knöpfe drücken sehen Sie verschiedene Arten von Ergebnissen. <br> Das sind zum Beispiel Bilder oder Nachrichten.',
        ],
        "choice"     => 'Unter der Zeile mit den mehreren Knöpfen sind 2 andere Knöpfe. <br> Diese Knöpfe sind Einstellungen und Filter.',
        "filter"  => [
            "title"     => 'Filter:',
            "1"     => 'Wenn Sie auf Filter<a title="Damit kann man bestimmen, was die Such-Maschine anzeigen soll. Für mehr Informationen klicken" href="/hilfe/easy-language/glossary#glfilter" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> klicken, können Sie ihre Suche filtern. <br> Hier können Sie ihre Such-Ergebnisse filtern. <br> Jede Such-Kategorie hat verschiedene Möglichkeiten zum Filtern.<br> Sie können zum Beispiel nach Ergebnissen der letzten 30 Tagen suchen.<br> Dann sehen Sie nur Ergebnisse von den letzen 30 Tagen.<br> In unseren Einstellungen können Sie den Filter einstellen.',
        ],
        "settings"  => [
            "title" => 'Einstellungen:',
            "1" => 'Wenn Sie auf Einstellungen klicken, sehen Sie die Einstellungen. <br> Da können Sie die Einstellungen für die ausgewählte Such-Kategorie<a title="Ergebnisse werden sortiert, damit man sie leichter findet. Für mehr Informationen klicken" href="/hilfe/easy-language/glossary#glsearchcategories" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> einstellen.',
        ],
    ],
    "result"  => [
        "title" => 'Ergebnisse',
        "info"  => [
            "1" => 'Ergebnisse sehen so aus:',
            "open"  => [
                "title" => 'Öffnen:',
                "0"=>'Es gibt mehrere Möglichkeiten das Ergebnis anzuschauen:',
                "1" => 'Sie können auf die Überschrift klicken.', 
                "2"=> 'Sie können auf den blauen Link unter der Überschrift klicken. <br> Dieser Link nennt sich URL<a title="Eine URL ist eine Internet-Adresse. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glurl" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>.',
            ],
            "newtab"  => [
                "title" => 'In neuem Tab öffnen:',
                "1" => 'Dieser Knopf öffnet das Ergebnis in einem neuen Tab<a title="Ein Browser kann mehrere Seiten gleichzeitig anzeigen. Jede Seite ist dann in einem Tab. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#gltab" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>. <br> Mit der Tastenkombination STRG und Linksklick kann man ein Ergebnis auch im neuen Tab<a title="Ein Browser kann mehrere Seiten gleichzeitig anzeigen. Jede Seite ist dann in einem Tab. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#gltab" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> öffnen. <br> Oder Sie drücken die mittlere Maustaste. <br> Man kann das Ergebnis auch immer im neuen Tab<a title="Ein Browser kann mehrere Seiten gleichzeitig anzeigen. Jede Seite ist dann in einem Tab. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#gltab" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> öffnen. <br> Dafür öffnet man unsere Einstellungen: <br> Unter <strong>Weitere Einstellungen</strong> finden Sie die Option "Ergebnisse in neuem Tab<a title="Ein Browser kann mehrere Seiten gleichzeitig anzeigen. Jede Seite ist dann in einem Tab. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#gltab" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> öffnen". <br> Dort können Sie das auf <strong>an</strong> schalten. <br> Dann öffnen sich die Ergebnisse immer im neuen Tab<a title="Ein Browser kann mehrere Seiten gleichzeitig anzeigen. Jede Seite ist dann in einem Tab. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#gltab" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>.',
            ],
            "anonym"  => [
                "title" => 'Anonym öffnen<a title="Eine Person ist anonym. Also weiß niemand: Wer ist diese Person? Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glopenanonymously" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>:',
                "1" => 'Wenn Sie hier klicken, öffnen Sie das Ergebnis geschützt. <br>Das ist wie ein Schutz-Schild. <br>Der Schutz-Schild versteckt Ihre Identität und den Standort vor den Webseiten, die Sie anschauen.<br> Bei <a href = "/hilfe/easy-language/privacy-protection#eh-proxy">Anonym öffnen<a title="Eine Person ist anonym. Also weiß niemand: Wer ist diese Person? Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glopenanonymously" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a></a> finden Sie mehr über das Thema.',
            ],
            "more"  => [
                "title" => '<img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="Mehr"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="Mehr"/>:',
                "1" => 'Wenn Sie auf <img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="Mehr"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="Mehr"/> klicken, gibt es neue Möglichkeiten. <br> Das Ergebnis sieht dann anders aus. <br> So sieht das Ergebnis dann aus:',
            ],
            "2" => 'Es gibt neue Möglichkeiten:',           
            "domainnewsearch"  => [
                "title" => 'Auf dieser Domain<a title="In einer Domain sind alle Internetseiten die einer hat. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#gldomain" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> neu suchen:',
                "1" => 'Wenn Sie darauf drücken, suchen Sie nur auf dieser Seite.',
                "2" => 'Beispiel:',
                "3" => 'Sie finden ein Ergebnis von der Seite Duden. <br> Sie klicken bei dem Ergebnis auf <img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="Mehr"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="Mehr"/>. <br> Dann klicken Sie auf den Knopf <strong>Auf dieser Domain<a title="In einer Domain sind alle Internetseiten die einer hat. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#gldomain" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> suchen</strong>. <br> Dann sucht man nochmal, aber nur auf der Seite Duden.',
            ],
            "hideresult"  => [
                "title" => 'Ausblenden:',
                "1" => 'Wenn Sie auf den Knopf drücken, blenden Sie alle Ergebnisse von der Seite aus. <br> Dann ist es unsichtbar. <br> Wenn Sie das immer so haben wollen, schauen Sie unter <a href="#eh-settings">Einstellungen</a> nach. <br> Da können Sie das einstellen.',
            ],
        ],
    ],
    
    "settings"  => [
        "title" => 'Einstellungen',
        "metagerkey" => [
            "title" => "Werbefreie Suche",
            "1"=> 'Hier kann man das Guthaben für die werbe-freie Suche sehen. <br> Die werbe-freie Suche kann man mit einem MetaGer Schlüssel nutzen. <br>Unter dem Guthaben kann man den MetaGer Schlüssel sehen. <br> Darunter findet man zwei Knöpfe. <br> Man kann auf den linken Knopf <strong>Schlüssel aufladen</strong> drücken. <br> Dann kann man den Schlüssel aufladen. <br> Man auf den rechten Knopf <strong>Schlüssel entfernen</strong> drücken. <br> Dann wird der Schlüssel aus dem Browser<a title="Mit dem Browser kann man Seiten im Internet anschauen. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glbrowser" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> gelöscht. <br> Mehr Informationen über den MetaGer Schlüssel findet man hier:  <a href = "/hilfe/easy-language/functions#eh-keyexplain">MetaGer Schlüssel</a> <br> Wenn man keinen Schlüssel hat, sieht das so aus:',
            "2"=> "Es werden 3 Knöpfe angezeigt. <br> Man kann auf den linken Knopf mit dem Text <strong>Was ist das?</strong> drücken. <br> Dann kommt man auf die Informations-seite vom MetaGer Schlüssel. <br> Man kann auf den mittleren Knopf mit dem Text <strong>Vorhandenen Schlüssel einrichten</strong> drücken. <br> Wenn man schon einen MetaGer Schlüssel hat, kann man den da eingeben. <br> Wenn man keinen Schlüssel hat, kann man auf den rechten Knopf <strong>Neuen Schlüssel einrichten</strong> drücken um einen neuen Schlüssel zu kaufen.",
        ],
        "cookies"  => [
            "title" => 'Wiederherstellen aller aktuellen Einstellungen',
            "1" => 'Manche Browser<a title="Mit dem Browser kann man Seiten im Internet anschauen. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glbrowser" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> löschen Ihre Cookies<a title="Mit dem Cookie kann der Computer sich an Sachen erinnern. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glcookies" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> nach einer Sitzung. <br> Dann werden auch die Einstellungen von MetaGer gelöscht. <br> Mit dem Link können Sie die Einstellungen wieder her-stellen. <br> Sie können diesen Link als Start-Seite speichern. <br> Dann sind beim Öffnen des Browsers<a title="Mit dem Browser kann man Seiten im Internet anschauen. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glbrowser" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> die Einstellungen wieder da. ',
        ],
        "searchengine"  => [
            "title" => 'Verwendete Such-Maschinen:',
            "1" => 'Hier sehen Sie alle Such-Maschinen<a title="Mit einer Such-Maschine können Sie nach Informationen im Internet suchen. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glsearchengine" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> die MetaGer nutzt. <br> Sie können auswählen welche Such-Maschinen<a title="Mit einer Such-Maschine können Sie nach Informationen im Internet suchen. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glsearchengine" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> Sie nutzen wollen. <br> Sie können auf die einzelnen Knöpfe klicken. <br> Wenn die Knöpfe durchgestrichen sind, dann nutzen Sie die Such-Maschinen<a title="Mit einer Such-Maschine können Sie nach Informationen im Internet suchen. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glsearchengine" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> nicht. <br> Einige Such-Maschinen sind nur mit einem <a href = "/hilfe/easy-language/functions#eh-keyexplain">MetaGer-Schlüssel</a> nutzbar.',
            "2" => 'Such-Maschinen die man nur mit MetaGer Schlüssel verwenden kann, sind grau.',
        ],
        "filter"  => [
            "title" => 'Such-Filter<a title="Damit kann man bestimmen, was die Such-Maschine anzeigen soll. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glfilter" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>:', 
            "1"  => [   
                "0" => 'Mit dem Such-Filter<a title="Damit kann man bestimmen, was die Such-Maschine anzeigen soll. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glfilter" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> können Sie Ihre Suche filtern. <br> Sie können also auswählen was Sie sehen wollen.',
                "1" => 'Datum:',
                "2" => 'Hier können Sie das Datum vom Such-Ergebnis aussuchen.<br> Man zeigt dann nur Ergebnisse mit dem ausgewählten Datum an. <br> Diese Funktion ist nur mit einem MetaGer Schlüssel möglich.',
                "3" => 'Sprache:',
                "4" => 'Hier können Sie die Sprache vom Such-Ergebnis aussuchen. <br> Man zeigt dann nur Ergebnisse mit der ausgewählten Sprache an. <br> Für einige Sprachen braucht man einen MetaGer Schlüssel.',
            ],
            "2" => 'Safesearch<a title="Damit kann man einstellen ob die Suchmaschine auch Sachen mit Sex anzeigen soll. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glsafesearch" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>:', 
            "3" => 'Mit dem Safesearch<a title="Damit kann man einstellen ob die Suchmaschine auch Sachen mit Sex anzeigen soll. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glsafesearch" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> Filter (Jugend-Schutz-Filter) verstecken Sie unangenehme Ergebnisse. <br> <strong>Strikt</strong> findet fast keine unangenehmen Ergebnisse. <br> <strong>Aus</strong> findet alle Ergebnisse, also auch unangenehme Ergebnisse. <br> <strong>Moderat</strong> findet ein paar unangenehme Ergebnisse. <br> Bei <strong>Beliebig</strong> entscheiden die einzelnen Such-Maschinen selber. <br> Für den moderaten Modus braucht man einen MetaGer Schlüssel.', 
        ],
        "blacklist"  => [
            "title" => 'Blacklist / Schwarze Liste:',
            "1" => 'Hier können Sie Web-Seiten angeben, die Sie nicht in den Ergebnissen sehen wollen. <br> In das Feld geben Sie die Seite ein. <br> Dann drücken Sie auf den Knopf <Strong>Speichern</strong>. <br> Hier können Sie mehrere Ergebnisse eintragen.<br> Dann verstecken Sie die Seite in den Ergebnissen. ',
            "2" => 'Wenn Sie eine Seite doch wieder sehen wollen, können Sie die Web-Seite aus dem Feld löschen.',
        ],
        "moresettings" => 'Weitere Einstellungen:',
        "advertisement" =>[
            "title" =>'Subtile Werbung für unseren eigenen Service',
            "1" => 'Wir zeigen Ihnen Werbung von MetaGer. <br> Diese Werbung ist nicht sehr auffällig. <br> Sie können unsere Eigen-Werbung ausschalten. <br> Dafür benötigen Sie aber einen <a href = "/hilfe/easy-language/functions#eh-keyexplain">MetaGer Schlüssel</a>.',
        ],
        "darkmode"  => [
            "title" => 'Dunklen Modus umschalten',
            "1" => 'Hier können Sie den dunklen Modus einschalten. <br> Oder ausschalten. <br> Bei dem dunklen Modus wird der Hintergrund dunkel und die Schrift hell.',
        ],
        "newtab"  => [
            "title" => 'Ergebnisse in neuem Tab öffnen',
            "1" => 'Hier können Sie die Ergebnisse in einem neuen Tab<a title="Ein Browser kann mehrere Seiten gleichzeitig anzeigen. Jede Seite ist dann in einem Tab. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#gltab" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> anzeigen. <br> Das geht dann immer für jedes Ergebnis.',
        ],
        "cite"  => [
        "title" => 'Zitate:',
        "1" => 'Hier können Sie auswählen ob Sie Zitate sehen wollen. <br> Zitate stehen auf der Ergebnis-Seite rechts.',
        ],
    ],

];