<?php

return [
    'title' => 'MetaGer - Einfache Hilfe',
    "backarrow" => 'Zurück',
    'glossary'  => 'Durch Klicken auf das Symbol<a title="Dieses Symbol führt zum Glossar" href="/hilfe/easy-language/glossary" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>  kommen Sie zur Erklärung von den schwierigen Wörtern.',
    "suchfunktion"  => [
        "title" => "Such-Funktionen",
    ],
    "stopworte"  => [
        "title" => 'Stopp-Worte',
        "1" => "Stopp-Worte sind Wörter die man nicht sehen will. <br> Wenn Sie ein Wort nicht sehen wollen dann machen Sie das so:",
        "2" => "Beispiel: <br> Sie wollen nach einem neuen Auto suchen. <br> Sie wollen das Wort <strong>BMW</strong> nicht sehen. <br> Also schreiben Sie:",
        "3" => "auto neu -bmw",
        "4" => "Sie schreiben ein Minus vor das Wort. <br>Dann zeigt man das Wort in den Ergebnissen nicht mehr an.",
    ],
    "mehrwortsuche"  => [
        "title" => "Mehr-Wort-Suche",
        "1" => "Die Mehr-Wort-Suche hat 2 Arten.",
        "2" => "Ein Wort soll in den Ergebnissen da sein. <br> Dann schreiben Sie das in Anführungs-Striche. <br> Das sieht so aus:",
        "3"  => [
            "0" => "Beispiel: <br> Sie suchen nach <strong>der runde Tisch</strong>. <br> Sie wollen das Wort <strong>rund</strong> in den Ergebnissen finden. <br> Also schreiben Sie das Wort so:",
            "example" => 'der "runde" tisch',
        ],
        "4" => 'Es gibt noch eine weitere Art von der Mehr-Wort-Suche. <br> Sie können auch ganze Sätze suchen. <br> Sie wollen einen Satz in genau der Reihen-Folge sehen. <br> Dann macht man das so:',
        "5"  => [
            "0" => "Beispiel: <br> Sie suchen nach <strong>der runde Tisch</strong>.<br> Sie wollen es in genau der Reihen-Folge sehen. <br> Das schreibt man dann so:",
            "example" => '"der runde tisch"',
        ],
    ],
    "exactsearch" =>[
        "title" => "Exakte Suche",
        "1" =>"Bei der exakten Suche wird genau das was Sie schreiben gesucht. <br> Ein Wort soll genau so wie geschrieben in den Ergebnissen vorkommen. <br> Dann schreibt man das mit einem Plus vor dem Wort. <br> Das sieht so aus: ",
        "2" =>"Beispiel: Sie suchen nach Beispielwort. <br> Sie möchten das Wort soll genau so wie geschrieben in den Ergebnissen finden. <br> Also schreiben Sie das Wort so: ",
        "3" =>'Sie können auch nach ganzen Sätzen suchen. <br> Sie wollen einen Satz in genau so wie geschrieben in den Ergebnissen sehen.',
        "4" => 'Beispiel: Sie suchen nach einem Beispielsatz. <br> Dann schreibt man das so: ',
        "example" => [
            "1" => "+Beispielwort",
            "2" => '+"Beispiel Phrase"',
        ],
    ],
    "bang"  => [
        "title" => "!bangs",
        "1" => 'MetaGer unterstützt eine Schreib-Weise die !bang<a title="Eine Abkürzung um schneller zu suchen. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glbangs" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> heißt. <br> Wenn man das benutzen will sieht das so aus: <br> <strong>!twitter</strong> oder <strong>!facebook</strong><br> Beispiel:<br> Sie möchten auf Twitter nach Katzen suchen. <br> Also geben Sie das so ein:',
        "example" => "!twitter katze",
        "2" => "Damit zeigt man bei dem Suchen rechts ein Feld an. <br> So sieht das Feld aus:",
        "3" => "Man kann auf den blauen Knopf drücken. <br> Dann öffnet sich die Web-Seite von Twitter mit der Suche nach Katzen. <br> Diese Funktion funktioniert bei kleinen Bild-Schirmen wie Handys nicht.",
    ],
    "key"  => [
        "maintitle" => 'MetaGer Schlüssel',

        "title" => [
            "1" => "MetaGer Schlüssel hinzufügen",
            "2" => "Farbiger MetaGer Schlüssel",

        ],
        "alt" => [
            "empty" =>'Abbildung eines rot/orangenen Schlüssels',
            "low" =>'Abbildung eines gelben Schlüssels',
            "full" =>'Abbildung eines grünen Schlüssels',
            "none" =>'Abbildung eines grauen Schlüssels',

        ],

        "1" => 'Sie können bei uns suchen, ohne Werbung zu sehen. <br> Dafür braucht man einen MetaGer Schlüssel. <br> Den kann man bei uns kaufen. <br> Sie können den Schlüssel auf mehreren Geräten gleich-zeitig benutzen. <br> Dafür muss man den Schlüssel einrichten. <br> Als Erstes öffnet man die Verwaltungs-Seite vom MetaGer Schlüssel. <br> Hier kommt man zur Verwaltungs-Seite: <br> <a href = "/keys/key/enter">Verwaltungs-Seite</a> <br> Dort gibt es diese Möglichkeiten:',
        "2" => [
            '1' => 'Log-in Code <br> Ein weiteres Gerät mit dem Log-in Code zu registrieren geht so: <br> Sie klicken auf den Knopf <strong>Login Code erzeugen</strong>. <br> So sieht der Knopf aus:',   
            '2' => 'Dann werden 6 Zahlen angezeigt. <br> So sehen die Zahlen zum Beispiel aus:',
            '3' => 'Diese Zahlen geben Sie dann bei dem Gerät ein, was Sie hin-zu-fügen wollen. <br> Die 6 Zahlen sind nur einmal gültig. <br> Wenn Sie also mehrere Geräte einrichten wollen, macht man das jedes Mal neu.', 
        ],
        "3" => [
            '1' => 'URL kopieren <br> Sie können auch die Internet-Adresse kopieren lassen. <br> Dafür klicken Sie auf den Knopf <strong>URL Kopieren</strong> <br> So sieht der Knopf aus:',
            '2' => 'Jetzt haben Sie den Link kopiert. <br> Den Link können Sie benutzen um mit dem MetaGer Schlüssel zu suchen.',   
        ],
        "4" => [
            '1' => 'Datei sichern <br> Sie können Ihren MetaGer Schlüssel auch als Datei speichern. <br> Dafür klicken Sie auf den Knopf <strong>In Datei sichern</strong>. <br> Nun haben Sie Ihren Schlüssel als Datei gespeichert. <br> Jetzt öffnen Sie die Seite zum Einrichten vom Schlüssel auf dem neuen Gerät. <br> So sieht die Seite aus:',
            '2' => 'Dann klicken Sie auf den Knopf <strong>Sicherungsdatei hochladen</strong>. <br> So sieht der Knopf aus:',
            '3' => 'Nun wählen Sie die Datei mit dem MetaGer Schlüssel aus. <br> Dann können den MetaGer Schlüssel nutzen.',
        ],
        "5" => [
            '1' =>'QR-Code scannen <br> Ein weiteres Gerät mit dem QR-Code zu registieren geht so: <br> Sie öffnen die Seite zum Einrichten vom Schlüssel auf dem neuen Gerät. <br> So sieht die Seite aus:',
            '2' => 'Dann klicken Sie auf den Knopf <strong>QR Code scannen</strong>. <br> So sieht dieser Knopf aus:',
            '3' => 'Nun scannen Sie den QR-Code ein. <br> Danach können Sie werbefrei auf Ihrem Gerät suchen.',   
        ],
        "6" => 'Manuell eingeben <br> Es geht natürlich auch, den Schlüssel manuell einzugeben. <br> Dafür tippen Sie die lange Folge von Zahlen und Buchstaben ab. ',   
        "7" => 'Manchmal sehen Sie bei uns einen bunten Schlüssel. <br> Das hat einen Grund. <br> Die Farben sagen, wie oft man noch ohne Werbung suchen kann. <br> Es gibt folgende Farben: ',   
        "8" => 'Grauer Schlüssel: <br> Sie sehen einen grauen Schlüssel. <br> Dann haben Sie keinen Schlüssel eingerichtet.',   
        "9" => 'Roter Schlüssel: <br> Sie sehen einen rot/orangenen Schlüssel. <br> Dann ist Ihr Guthaben leer. <br> Sie können also nicht mehr ohne Werbung suchen. <br> Wir empfehlen Ihnen den Schlüssel wieder aufzuladen. <br> Dann können Sie wieder ohne Werbung suchen.', 
        "10" => 'Grüner Schlüssel: <br> Sie sehen einen grünen Schlüssel. <br> Dann suchen Sie aktuell ohne Werbung. <br> Sie können also weiter ohne Werbung suchen.',   
        "11" => 'Gelber Schlüssel: <br> Sie sehen einen gelben Schlüssel. <br> Dann haben Sie bald Ihr Guthaben aufgebraucht. <br> Sie haben aktuell weniger als 30 Token, zum Benutzen. <br> Wir empfehlen Ihnen den Schlüssel bald aufzuladen. ',   
  

   
    ],
    "selist"  => [
        "title"  => [
            '0' => 'MetaGer zur Such-Maschinen-Liste des Browsers hinzufügen',
            '1' => 'MetaGer installieren',
        ],
        "explanation"  => [
            '1' => 'Auf der Start-Seite gibt es ein Feld <strong>MetaGer installieren</strong>.<br> Das Feld ist unter dem Such-Feld. <br> So sieht das Feld <strong>MetaGer installieren</strong> aus:<br>',
            '2' => 'Manchmal muss man auch eine URL eingeben. <br> Diese sieht so aus: <br>https://metager.de/meta/meta.ger3?eingabe=%s <br> Wenn Sie Probleme haben, wenden Sie sich an uns mit dem <a href="/kontakt" target="_blank" rel="noopener">Kontakt-Formular</a>.',
        ],
    ],
];