<?php

return [
    "achtung"               => 'Achtung, da unsere Suchmaschine ständig weiterentwickelt und verbessert wird, kann es dazu kommen, dass sich immer wieder Änderungen an Aufbau und Funktion ergeben. Wir versuchen zwar die Hilfe schnellstmöglich den Änderungen entsprechend anzupassen, können jedoch nicht ausschließen, dass es zu temporären Unstimmigkeiten in Teilen der Erklärungen kommt.',
    "title"                 => 'MetaGer - Einfache Hilfe',
    "easy.language.back"    => "Zurück",
    
    "tableofcontents"  => [
        'title' => '',
        "1"  => [
            '0' => 'Die Haupt-Seiten',
            '1' => 'Die Start-Seite',
            '2' => 'Das Such-Feld',
            '3' => 'Die Ergebnis-Seite',
            '4' => 'Einstellungen',
        ],
        "2"  => [
            '0' => 'Nützliche Funktionen und Hinweise',
            '1' => 'Such-Funktionen',
            '2' => '!bangs',
            '3' => 'Schlüssel',
            '4' => 'MetaGer herunterladen',
        ],

        "3"  => [
            '0' => 'Funktionen zur Anonymität und Daten-Sicherheit',
            '2' => 'Cookies & Co.',
            '3' => 'Tor-Hidden-Service',
            '4' => 'Anonym öffnen',
            '5' => 'Jugend-Schutz',
        ],
        
        "4"  => [
            '0' => 'Weitere Dienste um die Suche herum',
            '1' => 'Android-App',
            '3' => 'Such-Wort-Assoziator',
            '4' => 'MetaGer Widget',
            '5' => 'MetaGer Karte',
        ],

    ],
 ];
