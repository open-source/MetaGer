<?php

return [
    "title"=>"Glossar",
    "tableofcontents"=>"Inhaltsverzeichnis",
    "backarrow"=>"Zurück",
    "uparrow"=>"Hoch",

    "entry" => [
        "1"=>"Such-Maschine",
        "2"=>"Standard-Such-Maschine",
        "3"=>"Such-Kategorien",
        "4"=>"Filter",
        "5"=>"URL",
        "6"=>"Tab",
        "7"=>"Anonym öffnen",
        "8"=>"Safesearch",
        "9"=>"!bangs",
        "10"=>"Cookies",
        "11"=>"Domain",
        "12"=>"Browser",
        "18"=> "Tor",
        "13"=>"Tor-Hidden-Service",
        "14"=>"Tor-Browser",
        "15"=>"App",

    ],

    "explanation" => [
        "entry1"=> [
            '1'=>'Eine Such-Maschine ist wie ein Programm auf einem Computer. <br> Mit einer Such-Maschine können Sie nach Informationen im Internet suchen. <br> Sie können nach Informationen zu bestimmten Themen suchen. <br> Sie können auch nach Bildern und Videos suchen.',
            '2'=>'Es gibt viele verschiedene Such-Maschinen. <br> Viele davon speichern Ihre Daten. <br> Oft werden Ihre Daten an andere Unternehmen weitergegeben die Ihnen Werbung zeigen. <br> Diese Werbung wird dann auf Ihre Interessen angepasst. <br> Die Such-Maschinen verdienen damit Geld. <br>MetaGer ist eine besondere Such-Maschine. <br> Wir speichern Ihre Daten <strong>nicht</strong>. <br> Uns ist es wichtig, Ihre Daten zu schützen. <br>',
            '3'=>'Was ist eine Meta-Such-Maschine? ',
            '4'=>'MetaGer ist eine Meta-Such-Maschine. <br> Eine Meta-Such-Maschine sammelt Ergebnisse von mehreren Such-Maschinen. <br> Diese Ergebnisse werden dann neu sortiert. <br> Dann werden alle Ergebnisse auf einer Seite zusammen gezeigt. <br> Man sucht also bei einer Meta-Such-Maschine bei ganz vielen Such-Maschinen gleichzeitig. ',
        ],
        "entry2"=> [
            '1'=>'Das ist die Such-Maschine mit der Sie immer suchen. <br> Sie ist bei der Suche immer ausgewählt. <br> Sie öffnen Ihren Browser. <br> Sie geben das was Sie suchen wollen oben in die Adress-Leiste ein. <br> Dann sucht man mit der Standard-Such-Maschine.',
        ],
        "entry3"=> [
            '1'=>'Manche Sachen sind in der Suchmaschine schon sortiert. <br> So kann man sie leichter finden. <br> Beispiel: Die Such-Kategorie <strong>Produkte</strong> kann man benutzen, wenn man etwas kaufen will.',
            '2'=>'Bei MetaGer gibt es für manche Such-Kategorien passende Such-Maschinen. <br>Beispiel: spezialisierte Produktsuchmaschinen.',
        ],
        "entry4"=> [
            '1'=>'Ein Filter ist eine Software. <br> Damit kann man bestimmen, was die Such-Maschine anzeigen soll.<br> Zum Beispiel nur neue Sachen.<br> Oder nur eine bestimmte Sprache.',
            '2'=>'Bei MetaGer gibt es verschiedene Filter-Möglichkeiten. <br> Man kann sortieren nach: Datum, Größe, Dokumentenart oder Safesearch.',
        ],
        "entry5"=> [
            '1'=>'Eine URL ist eine Internet-Adresse. <br> Damit kann der Browser eine Seite im Internet finden. <br> Suchmaschinen können also URLs finden. <br> Die URL wird im Adress-Feld eines Browsers angezeigt.',
        ],
        "entry6"=> [
            '1'=>'Ein Browser kann mehrere Seiten gleichzeitig anzeigen. <br> Jede Seite ist dann in einem Tab. <br> Einen Tab kann man anklicken.',
            '2'=>'Die meisten Browser nutzen Tabs.<br> Tabs können gruppiert werden. <br> Manche Browser bieten anonyme Tabs an.',
        ],
        "entry7"=> [
            '0'=>'Was ist anonym?',
            '1'=>'Eine Seite im Internet kann man anonym öffnen. <br> Man klickt bei MetaGer auf den Knopf <strong>ANONYM ÖFFNEN</strong>. Dann weiß niemand dass man sich die Seite angesehen hat.',
            '2'=>'Die angeforderte Seite wird bei MetaGer an den Proxy-Dienst übergeben. Proxies verhindern dass die eigene Adresse sichtbar wird.',
            '3'=>'Eine Person ist anonym. <br> Also weiß niemand: Wer ist diese Person?<br> Es gibt keine Hinweise darauf, wer eine Person ist.',
            '4'=>'Im Internet geht es meist darum die eigene IP-Adresse nicht zu übertragen.',
        ],
        "entry8"=> [
            '1'=>'Safesearch ist ein englisches Wort. <br> Safesearch ist ein Filter. <br>Damit kann man einstellen ob die Suchmaschine auch Sachen mit Sex anzeigen soll.',
        ],
        "entry9"=> [
            '1'=>'!bangs bezeichnet eine Abkürzung um schneller zu suchen. <br> Mit !bangs kann man also bestimmte Suchen schneller durchführen.',
        
        ],
        "entry10"=> [
            '1'=>'Cookies sind Software. <br>Die schreibt der Computer selbst. <br>Mit dem Cookie kann der Computer sich an Sachen erinnern. <br> Damit merkt sich der Computer also Einstellungen wie den dunklen Modus.',
            '2'=>'Es werden verschiedene Arten von Cookies unterschieden. Manche dienen dem merken von Einstellungen. Diese werden auch von MetaGer verwendet. Andere dienen der Steuerung von Sessions. Darüber hinaus gibt es Third-Party-Cookies die ohne technische Notwendigkeit geschrieben werden.',
        ],
        "entry11"=> [
            '1'=>'Computer benutzen Zahlen. <br> Daraus werden Wörter gemacht. <br> Die kann man sich leichter merken. <br> In einer Domain sind alle Internetseiten die einer hat.',
            '2'=>'Das Domain Name System ist ein Dienst. Das kurze Wort dafür ist DNS. Das DNS ist eine Auskunft für Computer·netze. Die Auskunft übersetzt Adress·zahlen in Adress·wörter. Damit kann man sich einfacher Computer·adressen merken.',
        ],
        "entry12"=> [
            '1' => 'Ein Browser ist ein Computer-Programm. <br> Mit dem Browser kann man Seiten im Internet anschauen. <br> Browser ist ein englisches Wort. <br> So spricht man das Wort: Brauser.',
            '2'=>'Es gibt verschiedene Browser. <br> Die Unterschiede sind gering. <br> Es gibt Browser für das Onionweb das auch Darkweb genannt wird.',
        ],
        "entry18"=> [
            '1' => 'Was ist Tor?',
            '2'=>'Tor lässt sich anhand eines vereinfachten Beispiels leichter erklären. <br> Peter möchte gerne wissen, wann Birgit Geburtstag hat. <br> Birgit soll aber nicht erfahren, dass Peter dies wissen will. <br> Was kann Peter tun? <br> Er fragt Lena, ob sie Birgit nach ihrem Geburtstag fragt. <br> Es gibt hierbei jedoch ein Problem. <br> Lena weiß, wer Birgit nach ihrem Geburtstag gefragt hat. <br> Um zu verhindern, dass man erkannt wird tut Peter folgendes:',
            '3' =>'Peter gibt Lena einen verschlossenen Brief. <br> In diesem Brief fragt er nach dem Geburtstag von Birgit.',
            '4'=> 'Lena öffnet diesen Brief nicht. <br> Also weiß Lena nicht was Peter wissen will. <br> Lena gibt diesen Brief weiter an Martin.',
            '5'=> 'Martin kennt weder Peter noch Birgit. <br> Auch Martin öffnet den Brief nicht. <br> Er gibt den Brief an Sophie weiter.',
            '6'=> 'Sophie weiß nicht, dass dieser Brief von Peter ist. <br> Sie öffnet den Brief. <br> Sie sieht, dass jemand wissen möchte, wann Birgit Geburtstag hat. <br> Sophie fragt Birgit nach ihrem Geburtstag. <br> Diese Info schreibt sie auf einen Zettel und verschließt diesen in einen Brief. <br> Diesen Brief gibt Sie an Martin.',
            '7'=> 'Martin gibt den verschlossenen Brief weiter an Lena',
            '8'=> 'Lena gibt den verschlossenen Brief an Peter zurück. <br> Auch hier weiß Lena nicht, was in dem Brief steht.',
            '9'=> 'Peter kann diesen Brief nun öffnen und kennt jetzt den Geburtstag von Birgit.',


        ],
        "entry13"=> [
            '1' => 'Mit dem Tor-Hidden-Service kann man MetaGer über das Tor-Netzwerk nutzen.',
            '2'=>'Eine vereinfachte Erklärung des Tor-Netzwerks finden Sie auf dieser Seite unter <a href = "/hilfe/easy-language/glossary#gltor">Tor</a>.<br> Weitere Informationen zu Tor finden Sie auf der Webseite von Tor: <a href =https://www.torproject.org/>https://www.torproject.org</a>',
        ],
        "entry14"=> [
            '1' => 'Der Tor-Browser bietet eine vollständig voreingestellte Möglichkeit Tor zu nutzen.',
            '2'=>'Eine vereinfachte Erklärung des Tor-Netzwerks finden Sie auf dieser Seite unter <a href = "/hilfe/easy-language/glossary#gltor">Tor</a>.<br> Weitere Informationen zu Tor finden Sie auf der Webseite von Tor: <a href =https://www.torproject.org/>https://www.torproject.org</a>',
        ],
        "entry15"=> [
            '1' => 'Eine App ist ein Programm für Handys. <br>Um eine App nutzen zu können braucht man eine besondere Art von Handys.<br>Diese Handys heißen auch Smart-Phones.<br>Apps brauchen Internet.<br>Smart-Phones haben Internet.<br>Also kann man mit Smart-Phones Apps benutzen.',
        ],

    ],
];