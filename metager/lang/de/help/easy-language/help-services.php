<?php

return [
    "title"                 => 'MetaGer - Einfache Hilfe',
    "backarrow"             => 'Zurück',
    'glossary'              => 'Durch Klicken auf das Symbol<a title="Dieses Symbol führt zum Glossar" href="/hilfe/easy-language/glossary" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a>  kommen Sie zur Erklärung von den schwierigen Wörtern.',
    "dienste"  => [
        "1" => "Weitere Dienste um die Suche herum",
    ],
    "app"  => [
        "title" => "Android-App",
        "1" => 'MetaGer gibt es auch als App<a title="Ein Programm für Handys und Tablets. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glapp" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> für ihr Handy. <br> Es gibt die MetaGer App<a title="Ein Programm für Handys und Tablets. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glapp" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> für Android-Geräte. <br> Diese können Sie einfach auf ihr Android Smartphone laden. <br> Hier können Sie die MetaGer App<a title="Ein Programm für Handys und Tablets. Für mehr Informationen klicken." href="/hilfe/easy-language/glossary#glapp" ><img class="glossary-icon lm-only" src="/img/glossary-icon-lm.svg"/><img class="glossary-icon dm-only" src="/img/glossary-icon-dm.svg"/></a> runter-laden: <a href=\"https://play.google.com/store/apps/details?id=de.metager.metagerapp\" target=\"_blank\" rel=\"noopener\">MetaGer App</a>',
    ],
        "widget"  => [
        "title" => "MetaGer Widget",
        "1" => "Mit dem MetaGer Widget kann man unsere Suche auf andere Web-seiten packen. <br> Sie haben eine eigene Web-seite. <br> Dann können Sie MetaGer auf ihrer Seite hinzufügen. <br> Sie können dann auf ihrer eigenen Seite mit MetaGer suchen. <br> Sie können nach Inhalten auf ihrer eigenen Seite suchen. <br> Oder Sie können im Internet suchen. <br> Bei Fragen können Sie unser <a href=\"/kontakt/\" target=\"_blank\" rel=\"noopener\">Kontakt-Formular</a> nutzen.",
    ],
    "maps"  => [
        "title" => "MetaGer Maps",
        "1" => ' <a href="https://maps.metager.de" target="_blank">MetaGer Maps</a> ist ein von MetaGer entwickelter Karten-Dienst. <br> MetaGer Maps gibt es auch für das Handy. <br> Hier können Sie die App runterladen: <a href="/app" target="_blank">App runterladen</a>. <br> Wenn man bei MetaGer sucht kann man auch MetaGer Maps in der Suche nutzen. <br> Dafür können Sie einfach auf den neuen Such-Fokus <strong>Maps</strong> klicken. <br> So sieht der Such-Fokus aus:',
        "2" => "Wenn Sie darauf geklickt haben kommen Sie zu der gesuchten Karte. <br> Dann sehen Sie auf der rechten Seite eine Spalte. <br> Diese Spalte zeigt gefundene Punkte auf der Karte an. <br> Diese Punkte passen zu der Suche. <br> So sieht diese Spalte aus:",
    ],

];