<?php
return [
    'achtung' => 'Achtung, da unsere Suchmaschine ständig weiterentwickelt und verbessert wird, kann es dazu kommen, dass sich immer wieder Änderungen an Aufbau und Funktion ergeben. Wir versuchen zwar die Hilfe schnellstmöglich den Änderungen entsprechend anzupassen, können jedoch nicht ausschließen, dass es zu temporären Unstimmigkeiten in Teilen der Erklärungen kommt.',
    'title' => 'MetaGer - Hilfe',
    'easy.language' => "Zur einfachen Hilfe",
    'tableofcontents' => [
        '1' => [
            '0' => 'Die Benutzung der Hauptseiten',
            '1' => 'Die Startseite',
            '2' => 'Das Suchfeld',
            '3' => 'Die Ergebnisseite',
            '4' => 'Einstellungen',
        ],
        '2' => [
            '0' => 'Nützliche Funktionen und Hinweise',
            '1' => 'Suchfunktionen',
            '2' => '!bangs',
            '3' => 'MetaGer Schlüssel',
            '4' => 'MetaGer hinzufügen',
        ],
        '3' => [
            '0' => 'Datenschutz',
            '2' => 'Cookies & Co.',
            '3' => 'Tor-Hidden-Service',
            '4' => 'MetaGer-Proxy',
            '5' => 'Jugendschutz',
        ],
        '4' => [
            '0' => 'Weitere Dienste um die Suche herum',
            '1' => 'Android-App',
            '3' => 'Suchwortassoziator',
            '4' => 'MetaGer Widget',
            '5' => 'MetaGer Maps',
        ],
    ],
];
