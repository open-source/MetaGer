<?php
return [
    'head' => [
        '1' => 'MetaGer Apps',
        '2' => 'MetaGer App',
        '3' => 'MetaGer Maps App',
        '4' => 'Installation',
    ],
    'disclaimer' => [
        '1' => 'Aktuell können wir unsere Apps nur für Android Geräte zur Verfügung stellen.',
    ],
    'metager' => [
        '1' => 'Diese App bringt die volle Power unserer Suchmaschine auf ihr Smartphone. Durchsuchen Sie das Internet unter Wahrung ihrer Privatsphäre mit nur einem Fingerwisch.',
        '2' => 'Sie können die App für unsere Suche entweder über F-Droid oder den Google Playstore installieren, oder sie datengeschützt manuell von unserem Server auf ihrem Smartphone installieren.',
        'playstore' => 'Google Playstore',
        'fdroid' => 'F-Droid Store',
        'manuell' => 'Manual Installation',
    ],
    'maps' => [
        '1' => 'Diese App bietet eine native Integration von <a href="https://maps.metager.de" target="_blank">MetaGer Maps</a> (powered by <a href="https://www.openstreetmap.de/" target="_blank">Openstreetmap</a>) auf Ihrem mobilen Android-Gerät.',
        '2' => 'Dadurch läuft der Karten- und Navigationsdienst auch auf Ihrem Smartphone optimal und schnell. Die App steigert die Performance im Vergleich zur Verwendung im mobilen Browser und bietet einige weitere Vorteile. Probieren Sie es aus!',
        '3' => 'Die APK für die manuelle Installation ist etwa 4x so groß wie die Playstore-Installation (~250MB), da sie Bibliotheken für alle gängigen CPU-Architekturen enthält. Der integrierte Updater kennt die CPU-Architektur Ihres Geräts und installiert die richtige (kleine) Version der App beim ersten Update. Wenn Sie Ihre Gerätearchitektur selbst kennen, können Sie auch <a href="https://gitlab.metager.de/metagermaps/android/-/releases/permalink/latest" target="_blank">das kleine Paket</a> direkt installieren.',
        'list' => [
            '1' => 'Zugriff auf Positionsdaten  => Falls GPS an ihrem Mobiltelefon aktiviert ist, können wir dadurch Ihre Suchergebnisse verbessern. Außerdem schalten Sie damit die Funktion Schritt-für-Schritt Navigation frei. <b>Selbstverständlich werden diese Daten nirgendwo gespeichert und erst recht nicht an Dritte weiter gegeben!</b>',
            '2' => 'Die APK für die manuelle Installation hat einen integrierten Updater. Damit der Updater funktioniert, bittet die App um die Erlaubnis, Benachrichtigungen zu versenden, um Sie über ein verfügbares Update zu informieren, und verwendet die Android-Erlaubnis REQUEST_INSTALL_PACKAGES, damit sie Sie bitten kann, das App-Update zu installieren',
        ],
        '4' => 'Die App fragt nach dem ersten Start nach folgenden Berechtigungen:',
    ],
];
