<?php
return [
    'plugin' => 'Installera MetaGer',
    'plugin-title' => 'Lägg till MetaGer i din webbläsare',
    'key' => [
        'placeholder' => 'Ange medlemsnyckel',
        'tooltip' => [
            'nokey' => 'Skapa annonsfri sökning',
            'empty' => 'Token förbrukad. Ladda om nu.',
            'low' => 'Token snart förbrukad. Ladda om nu.',
            'full' => 'Annonsfri sökning aktiverad.',
        ],
    ],
    'placeholder' => 'MetaGer: Integritetsskyddad sökning och sökning',
    'searchbutton' => 'Starta MetaGer-sökning',
    'foki' => [
        'web' => 'Webb',
        'bilder' => 'Bilder',
        'nachrichten' => 'Nyheter',
        'science' => 'Vetenskap',
        'produkte' => 'Produkter',
        'maps' => 'Kartor',
    ],
    'adfree' => 'Använd MetaGer annonsfritt',
    'skip' => [
        'search' => 'Hoppa till inmatning av sökfråga',
        'navigation' => 'Hoppa till navigering',
        'fokus' => 'Hoppa till val av sökfokus',
    ],
    'lang' => 'wwitch språk',
    'searchreset' => 'radera sökfråga inmatning',
    'searchbar-replacement' => [
        'message' => 'MetaGer-sökmotorn är nu endast tillgänglig utan reklam!',
        'login' => 'Logga in med nyckel',
        'start' => 'Skapa en ny nyckel',
        'why' => 'Varför?',
        'key_error' => "Den inmatade nyckeln var inte giltig. Vänligen kontrollera inmatningen.",
        'login_code_error' => "Den angivna inloggningskoden var inte giltig. Tips: Inloggningskoder är endast giltiga när de är synliga på en annan enhet!",
        'payment_id_error' => "Du har angett ett betalnings-id som inte är en korrekt nyckel. Din nyckel är 36 tecken lång.",
    ],
];
