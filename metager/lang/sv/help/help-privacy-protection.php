<?php
return [
    'title' => 'MetaGer - Hjälp',
    'backarrow' => 'Tillbaka',
    'tor' => [
        'title' => 'Dold tjänst för TOR <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-torhidden" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '2' => 'Du kan komma åt MetaGer i Tor-webbläsaren på: http://metagerv65pwclop2rsfzg4jwowpavpwd6grhhlvdgsswvo6ii4akgyd.onion .',
        '1' => 'I många år har MetaGer dolt och inte lagrat IP-adresser. Dessa adresser är dock tillfälligt synliga på MetaGer-servern medan en sökning pågår: om MetaGer skulle äventyras skulle en angripare kunna läsa och lagra dina adresser. För att uppfylla de högsta säkerhetskraven driver vi en MetaGer-instans i Tor-nätverket: MetaGer TOR Hidden Service - tillgänglig via: <a href="/tor/" target="_blank" rel="noopener">https://metager.de/tor/</a>. För att använda den behöver du en speciell webbläsare, som du kan ladda ner från <a href="https://www.torproject.org/" target="_blank" rel="noopener">https://www.torproject.org/</a>.',
    ],
    'proxy' => [
        'title' => 'Anonymisering av MetaGer proxyserver <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-proxy" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'För att använda den behöver du bara klicka på "OPEN ANONYMOUSLY" längst ner i resultatet på MetaGers resultatsida. Din begäran kommer sedan att dirigeras till målwebbplatsen via vår anonymiserande proxyserver, och dina personuppgifter kommer att förbli helt skyddade. Viktigt: Om du följer länkar på sidorna från och med nu kommer du fortfarande att skyddas av proxyn. Du kan dock inte ange en ny adress i adressfältet längst upp. I så fall förlorar du ditt skydd. Du kan se om du fortfarande är skyddad i adressfältet, som kommer att visa: https://proxy.suma-ev.de/?url=here är den faktiska adressen.',
    ],
    'content' => [
        'title' => 'Tvivelaktigt innehåll / Skydd av ungdomar <a title="For easy help, click here" href="/help/easy-language/privacy-protection#eh-content" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'explanation' => [
            '1' => 'Jag har fått "träffar" som jag inte bara tycker är irriterande utan som också innehåller, enligt min mening, olagligt innehåll!',
            '2' => 'Om du hittar något på Internet som du anser vara olagligt eller skadligt för minderåriga, kan du kontakta <a href="mailto:hotline@jugendschutz.net" target="_blank" rel="noopener">hotline@jugendschutz.net</a> via e-post eller besöka <a href="http://www.jugendschutz.net/" target="_blank" rel="noopener">www.jugendschutz.net</a> och fylla i det klagomålsformulär som finns där. Det är bra att ge en kort anteckning om vad du anser vara otillåtet och hur du kom över detta innehåll. Du kan också rapportera tvivelaktigt innehåll direkt till oss. Skicka då ett e-postmeddelande till vår ungdomsskyddsombud (<a href="mailto:jugendschutz@metager.de" target="_blank" rel="noopener">jugendschutz@metager.de</a>).',
        ],
    ],
    'maps' => [
        'title' => 'MetaGer-kartor',
        '1' => 'Att värna om integriteten i en tid av globala datajättar har också lett oss till att utveckla <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a>: den (såvitt vi vet) enda ruttplaneraren som erbjuder full funktionalitet via en webbläsare och app utan att lagra användarens position. Allt detta är verifierbart eftersom vår programvara är öppen källkod. För att använda maps.metager.de rekommenderar vi vår snabba appversion. Du kan ladda ner våra appar från <a href="/app" target="_blank">här</a> (eller naturligtvis också från Play Store).',
        '2' => 'Denna kartfunktion kan också nås från MetaGer-sökningen (och vice versa). När du har sökt efter en term i MetaGer kommer du att se ett nytt sökfokus "Maps" i det övre högra hörnet. Om du klickar på den kommer du till en motsvarande karta.',
        '3' => 'När kartan laddas visas de punkter (POI = Points of Interest) som MetaGer har hittat och som också listas i den högra kolumnen. Vid zoomning anpassar sig denna lista till kartutsnittet. Om du håller muspekaren över en markör på kartan eller i listan markeras motsvarande objekt. Klicka på "Detaljer" för att få mer information om den punkten från databasen nedan.',
    ],
    'easy-help' => 'Genom att klicka på symbolen <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , kommer du till en förenklad version av hjälpen.',
    'privacy' => [
        'title' => "Anonymitet och datasäkerhet",
        '1' => 'Spårning av cookies, sessions-ID och IP-adresser <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-tracking" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '2' => 'Ingen av dessa används, lagras, behålls eller behandlas på annat sätt här på MetaGer (undantag: kortvarig lagring för skydd mot hackning och botattacker). Eftersom vi anser att detta ämne är extremt viktigt har vi också skapat sätt att hjälpa dig att uppnå högsta säkerhetsnivå: MetaGer TOR Hidden Service och vår anonymiserande proxyserver.',
        '3' => "Mer information hittar du nedan. Funktionerna är tillgängliga under \"Tjänster\" i navigeringsfältet.",
    ],
];
