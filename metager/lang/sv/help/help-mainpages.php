<?php
return [
    'backarrow' => 'Tillbaka',
    'title' => [
        '2' => 'Använda huvudsidorna',
        '1' => 'MetaGer - Hjälp',
    ],
    'settings' => [
        '7' => 'Citat <br> Här kan du aktivera och avaktivera visningen av citat.',
        'title' => 'Inställningar <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-settings" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '2' => 'Använda sökmotorer <br> Här kan du visa och justera de sökmotorer du använder. Genom att klicka på respektive namn kan du aktivera eller inaktivera den på motsvarande sätt.',
        '3' => 'Sökfilter <br> Med sökfilter kan du filtrera din sökning permanent.',
        '4' => 'Svart lista <br> Här kan du skapa en personlig svart lista. Du kan använda den för att filtrera bort specifika domäner och skapa dina egna sökinställningar. Genom att klicka på "Lägg till" kommer dessa inställningar att bifogas länken i avsnittet "Notera".',
        '5' => 'Växla till mörkt läge <br> Växla till mörkt läge enkelt här.',
        '6' => 'Öppna resultat i ny flik <br> Här kan du permanent aktivera funktionen för att öppna resultat i en ny flik.',
        '8' => 'Återställ alla aktuella inställningar <br> En länk visas som du kan använda som startsida eller bokmärke för att behålla dina aktuella inställningar.',
        '9' => 'Subtil reklam för vår egen tjänst <br> Vi visar dig subtil reklam för våra egna tjänster. Du kan stänga av vår egenreklam här.',
        '1' => 'Annonsfri sökning <br> Här kan du se saldot på din nyckel och din nyckel. Du har också möjlighet att fylla på eller ta bort din nyckel.',
    ],
    'startpage' => [
        'title' => 'Startsidan <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-startpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'Startsidan innehåller sökfältet, en knapp i övre högra hörnet för att komma till menyn och två länkar under sökfältet för att lägga till MetaGer i din webbläsare och söka annonsfritt. I den nedre delen hittar du information om MetaGer och SUMA-EV-föreningen. Dessutom visas våra fokusområden <i>Garanterad integritet, Ideell förening, Diverse & gratis</i> och <i>100% grön energi</i> längst ner. Genom att klicka på respektive avsnitt eller genom att scrolla kan du hitta mer information. ',
    ],
    'searchfield' => [
        'title' => 'Sökfältet <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#eh-searchfield" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'Sökfältet består av flera delar:',
        'memberkey' => 'symbolen för nyckeln: Här kan du ange din nyckel för att använda annonsfri sökning. Du kan också se ditt token-saldo och hantera din nyckel.',
        'slot' => 'sökfältet: Ange din sökterm här. Stora och små bokstäver särskiljs inte.',
        'search' => 'förstoringsglaset: Börja din sökning genom att klicka här eller trycka på "Enter".',
        'morefunctions' => 'Ytterligare funktioner hittar du under menyalternativet "<a href = "/hilfe/funktionen">Sökfunktioner</a>"',
    ],
    'resultpage' => [
        'title' => 'Sidan med resultat <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-resultpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'foci' => 'Under sökfältet finns 3 olika sökfokus (Web, Images, News), som var och en är kopplade till specifika sökmotorer.',
        'choice' => 'Nedan ser du två objekt: "Filter" och "Inställningar" om tillämpligt.',
        'filter' => 'Filter: Här kan du visa och dölja filteralternativ och tillämpa filter. I varje sökfokus har du olika urvalsalternativ. Vissa funktioner är endast tillgängliga när du använder en MetaGer-nyckel.',
        'settings' => 'Inställningar: Här kan du göra permanenta sökinställningar för din MetaGer-sökning i det aktuella fokuset. Du kan också välja och välja bort sökmotorer som är associerade med fokus. Dina inställningar sparas med hjälp av en icke-personligt identifierbar cookie i klartext. Du kan också komma till inställningssidan via menyn i det övre högra hörnet.',
    ],
    'result' => [
        'title' => 'Resultat <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-results" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => [
            '1' => 'Alla resultat presenteras i följande format:',
            'open' => '"ÖPPNA": Klicka på rubriken eller länken nedan (URL) för att öppna resultatet på samma flik.',
            'newtab' => '"ÖPPNA I NY Flik" öppnar resultatet i en ny flik. Alternativt kan du också öppna en ny flik genom att använda CTRL och vänsterklicka eller den mittersta musknappen.',
            'anonym' => '"OPEN ANONYMOUSLY" innebär att resultatet öppnas under skydd av vår proxy. Mer information om detta finns i avsnittet <a href = "/hilfe/datensicherheit#h-proxy">Anonymisering MetaGer Proxy Server</a>.',
            'more' => '<img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>: När du klickar på <img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/> får du nya alternativ; resultatets utseende ändras:',
            '2' => 'De nya alternativen är:',
            'domainnewsearch' => '"Starta en ny sökning på denna domän": En mer detaljerad sökning utförs på domänen för resultatet.',
            'hideresult' => '"dölja": Detta gör att du kan dölja resultat från denna domän. Du kan också skriva denna switch direkt efter din sökterm och konkatenera den; ett "*" wildcard är också tillåtet. Se även Inställningar för en permanent lösning.',
        ],
    ],
    'easy-help' => 'Genom att klicka på symbolen <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , kommer du till en förenklad version av hjälpen.',
];
