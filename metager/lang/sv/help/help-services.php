<?php
return [
    'title' => 'MetaGer - Hjälp',
    'backarrow' => 'Tillbaka',
    'app' => [
        'title' => 'Android-app <a title="For easy help, click here" href="/hilfe/easy-language/services#help-app" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Du kan också använda MetaGer som en app. Ladda helt enkelt ner <a href="https://metager.de/app" target="_blank" rel="noopener">MetaGer App</a> på din Android-smartphone.',
    ],
    'widget' => [
        'title' => 'Widget för MetaGer <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-widget" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Detta är en kodgenerator som gör att du kan bädda in MetaGer på din webbplats. Du kan använda den för att utföra sökningar på din egen webbplats eller på internet efter önskemål. Om du har några frågor kan du använda <a href="/kontakt/" target="_blank" rel="noopener">vårt kontaktformulär</a>.',
    ],
    'maps' => [
        'title' => 'MetaGer Kartor <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-maps" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Att värna om integriteten i en tid av globala datajättar har också lett oss till att utveckla <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a>: den (såvitt vi vet) enda ruttplaneraren som erbjuder full funktionalitet via en webbläsare och app utan att lagra användarens position. Allt detta är verifierbart eftersom vår programvara är öppen källkod. För att använda maps.metager.de rekommenderar vi vår snabba appversion. Du kan ladda ner våra appar från <a href="/app" target="_blank">här</a> (eller naturligtvis också från Play Store).',
        '2' => 'Denna kartfunktion kan också nås från MetaGer-sökningen (och vice versa). När du har sökt efter en term i MetaGer kommer du att se ett nytt sökfokus "Maps" i det övre högra hörnet. Om du klickar på den kommer du till en motsvarande karta.',
        '3' => 'När kartan laddas visas de punkter (POI = Points of Interest) som MetaGer har hittat och som också listas i den högra kolumnen. Vid zoomning anpassar sig denna lista till kartutsnittet. Om du håller muspekaren över en markör på kartan eller i listan markeras motsvarande objekt. Klicka på "Detaljer" för att få mer information om den punkten från databasen nedan.',
    ],
    'easy-help' => 'Genom att klicka på symbolen <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , kommer du till en förenklad version av hjälpen.',
    'services' => [
        'text' => "Ytterligare tjänster kring sökningen",
    ],
];
