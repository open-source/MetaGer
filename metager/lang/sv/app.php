<?php
return [
    'head' => [
        '1' => 'Metager Appar',
        '2' => 'MetaGer-app',
        '3' => 'Appen MetaGer Maps',
        '4' => 'Installation',
    ],
    'disclaimer' => [
        '1' => 'För närvarande har vi endast en Android-version av vår app.',
    ],
    'metager' => [
        '1' => 'Med den här appen får du hela Metagers kraft till din smartphone. Sök på webben med en enda knapptryckning samtidigt som du skyddar din integritet.',
        '2' => 'Det finns två sätt att hämta vår app: installera via Google Playstore eller (bättre för din integritet) hämta den direkt från vår server.',
        'playstore' => 'Google Playstore',
        'fdroid' => 'F-Droid-butik',
        'manuell' => 'Manuell installation',
    ],
    'maps' => [
        '1' => 'Denna app ger en inbyggd integration av <a href="https://maps.metager.de" target="_blank">MetaGer Maps</a> (drivs av <a href="https://www.openstreetmap.de/" target="_blank">Openstreetmap</a>) på din mobila Android-enhet.',
        '2' => 'Därför körs ruttplaneraren och navigeringstjänsten mycket snabbt på din smartphone. Appen är snabbare jämfört med användningen i en mobil webbläsare. Och det finns många fler fördelar - kolla in dem!',
        '3' => 'APK:n för manuell installation är ungefär 4x så stor som playstore-installationen (~250MB) eftersom den innehåller bibliotek för alla vanliga CPU-arkitekturer. Den integrerade uppdateraren känner till din enhets CPU-arkitektur och installerar rätt (liten) version av appen vid den första uppdateringen. Om du själv känner till din enhets arkitektur kan du också <a href="https://gitlab.metager.de/metagermaps/android/-/releases/permalink/latest" target="_blank">direkt installera det lilla paketet</a>.',
        'list' => [
            '1' => 'Tillgång till positionsdata => Med GPS aktiverad kan vi erbjuda bättre sökresultat. Med detta får du tillgång till steg-för-steg-navigering. <b> Naturligtvis lagrar vi inga av dina data och vi lämnar inte ut några av dina data till tredje part.</b>',
            '2' => 'APK för manuell installation har en integrerad uppdaterare. För att uppdateraren ska fungera kommer appen att be om tillstånd att skicka meddelanden för att meddela dig om en tillgänglig uppdatering och använder Android-behörigheten REQUEST_INSTALL_PACKAGES så att den kan be dig att installera appuppdateringen',
        ],
        '4' => 'Efter den första starten kommer du att bli tillfrågad om följande behörigheter:',
    ],
];
