<?php
return [
    'opensearch' => 'MetaGer: säker sökning och hitta, skydda privatlivet',
    'startseite' => 'MetaGer startsida',
    'impressum' => 'meddelande om webbplats',
    'search-placeholder' => 'Din sökfråga...',
    'metager3' => 'Du använder för närvarande en MetaGer-testversion.',
    'engines' => [
        'queried' => 'Söktjänster som efterfrågas',
        'disabled' => 'Lägg till söktjänster i frågan',
        'payment_required' => 'Söktjänster tillgängliga med <a href=\':link\'>MetaGer-nyckel</a>',
    ],
    'skiplinks' => [
        'navigation' => 'Hoppa till navigering',
        'heading' => 'Hoppa snabbt till innehållet',
        'results' => 'Hoppa till sökresultat',
        'query' => 'Hoppa till inmatningsfält för sökfråga',
        'settings' => 'Hoppa till sökinställningar',
        'return' => 'Du kan när som helst återgå till denna meny genom att trycka på escape-tangenten',
    ],
];
