<?php
return [
    'title' => 'MetaGer - Hjælp',
    'backarrow' => 'Tilbage',
    'app' => [
        'title' => 'Android-app <a title="For easy help, click here" href="/hilfe/easy-language/services#help-app" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Du kan også bruge MetaGer som en app. Du skal blot downloade <a href="https://metager.de/app" target="_blank" rel="noopener">MetaGer App</a> på din Android-smartphone.',
    ],
    'widget' => [
        'title' => 'MetaGer Widget <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-widget" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Dette er en kodegenerator, der giver dig mulighed for at integrere MetaGer på din hjemmeside. Du kan bruge den til at udføre søgninger på din egen side eller på internettet efter ønske. Hvis du har spørgsmål, bedes du bruge <a href="/kontakt/" target="_blank" rel="noopener">vores kontaktformular</a>.',
    ],
    'maps' => [
        'title' => 'MetaGer-kort <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-maps" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'At bevare privatlivets fred i de globale datagiganters tidsalder har også fået os til at udvikle <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a>: den (så vidt vi ved) eneste ruteplanlægger, der tilbyder fuld funktionalitet via en browser og app uden at gemme brugerens placering. Alt dette er verificerbart, fordi vores software er open source. Til brug af maps.metager.de anbefaler vi vores hurtige app-version. Du kan downloade vores apps fra <a href="/app" target="_blank">her</a> (eller selvfølgelig også fra Play Store).',
        '2' => 'Denne kortfunktion kan også tilgås fra MetaGer-søgningen (og vice versa). Når du har søgt efter en term i MetaGer, vil du se et nyt søgefokus \'Maps\' i øverste højre hjørne. Hvis du klikker på det, kommer du til et tilsvarende kort.',
        '3' => 'Når kortet indlæses, viser det de punkter (POI\'er = Points of Interest), som MetaGer har fundet, og som også er anført i højre kolonne. Når du zoomer, tilpasser denne liste sig til kortudsnittet. Hvis du holder musen over en markør på kortet eller i listen, fremhæves det tilsvarende element. Klik på \'Detaljer\' for at få flere oplysninger om det pågældende punkt fra databasen nedenfor.',
    ],
    'services' => [
        'text' => "Yderligere tjenester omkring søgningen",
    ],
    'easy-help' => 'Ved at klikke på symbolet <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , får du adgang til en forenklet version af hjælpen.',
];
