<?php
return [
    'searchfield' => [
        'search' => 'forstørrelsesglasset: Start din søgning ved at klikke her eller trykke på "Enter".',
        'title' => 'Søgefeltet <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#eh-searchfield" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'Søgefeltet består af flere dele:',
        'memberkey' => 'nøglesymbolet: Her kan du indtaste din nøgle for at bruge annoncefri søgning. Du kan også se din token-saldo og administrere din nøgle.',
        'slot' => 'søgefeltet: Indtast dit søgeord her. Der skelnes ikke mellem store og små bogstaver.',
        'morefunctions' => 'Yderligere funktioner kan findes under menupunktet "<a href = "/hilfe/funktionen">Søgefunktioner</a>"',
    ],
    'backarrow' => 'Tilbage',
    'title' => [
        '2' => 'Brug af hovedsiderne',
        '1' => 'MetaGer - Hjælp',
    ],
    'startpage' => [
        'title' => 'Startsiden <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-startpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'Startsiden indeholder søgefeltet, en knap i øverste højre hjørne for at få adgang til menuen og to links under søgefeltet for at tilføje MetaGer til din browser og søge reklamefrit. I den nederste sektion finder du information om MetaGer og SUMA-EV-foreningen. Derudover vises vores fokusområder <i>Garanteret privatliv, Nonprofit-forening, Mangfoldig & gratis</i> og <i>100% grøn energi</i> nederst. Ved at klikke på de respektive sektioner eller ved at scrolle, kan du finde mere information. ',
    ],
    'resultpage' => [
        'title' => 'Siden med resultater <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-resultpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'foci' => 'Under søgefeltet er der 3 forskellige søgefokus (Web, Billeder, Nyheder), som hver især er forbundet med specifikke søgemaskiner.',
        'choice' => 'Nedenfor vil du se to punkter: "Filter" og "Indstillinger", hvis det er relevant.',
        'filter' => 'Filter: Her kan du vise og skjule filterindstillinger og anvende filtre. I hvert søgefokus har du forskellige valgmuligheder. Nogle funktioner er kun tilgængelige, når du bruger en MetaGer-tast.',
        'settings' => 'Indstillinger: Her kan du foretage permanente søgeindstillinger for din MetaGer-søgning i det aktuelle fokus. Du kan også vælge og fravælge søgemaskiner, der er knyttet til fokus. Dine indstillinger gemmes ved hjælp af en ikke-personligt identificerbar plaintext-cookie. Du kan også få adgang til indstillingssiden via menuen i øverste højre hjørne.',
    ],
    'result' => [
        'title' => 'Resultater <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-results" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => [
            '1' => 'Alle resultater præsenteres i følgende format:',
            'open' => '"ÅBN": Klik på overskriften eller linket nedenfor (URL\'en) for at åbne resultatet i samme fane.',
            'newtab' => '"ÅBN I NY FANE" åbner resultatet i en ny fane. Alternativt kan du også åbne et nyt faneblad ved at bruge CTRL og venstreklik eller den midterste museknap.',
            'anonym' => '"OPEN ANONYMOUSLY" betyder, at resultatet åbnes under beskyttelse af vores proxy. Nogle oplysninger om dette kan findes i afsnittet <a href = "/hilfe/datensicherheit#h-proxy">Anonymizing MetaGer Proxy Server</a>.',
            'more' => '<img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>: Når du klikker på <img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>, får du nye muligheder; resultatets udseende ændres:',
            '2' => 'De nye muligheder er:',
            'domainnewsearch' => '"Start en ny søgning på dette domæne": En mere detaljeret søgning udføres på domænet for resultatet.',
            'hideresult' => '"skjul": Dette giver dig mulighed for at skjule resultater fra dette domæne. Du kan også skrive denne switch direkte efter dit søgeord og sammenkæde det; et "*" wildcard er også tilladt. Se også Indstillinger for en permanent løsning.',
        ],
    ],
    'settings' => [
        'title' => 'Indstillinger <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-settings" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '2' => 'Brugte søgemaskiner <br> Her kan du se og justere de søgemaskiner, du bruger. Ved at klikke på det tilsvarende navn kan du aktivere eller deaktivere det i overensstemmelse hermed.',
        '3' => 'Søgefiltre <br> Søgefiltre giver dig mulighed for at filtrere din søgning permanent.',
        '4' => 'Blacklist <br> Her kan du oprette en personlig blacklist. Du kan bruge den til at filtrere bestemte domæner fra og oprette dine egne søgeindstillinger. Ved at klikke på "Tilføj" tilføjes disse indstillinger til linket i afsnittet "Bemærk".',
        '5' => 'Skift til mørk tilstand <br> Skift nemt til mørk tilstand her.',
        '6' => 'Åbn resultater i ny fane <br> Her kan du permanent aktivere funktionen til at åbne resultater i en ny fane.',
        '7' => 'Citater <br> Her kan du slå visning af citater til og fra.',
        '1' => 'Annoncefri søgning <br> Her kan du se saldoen på din nøgle og din nøgle. Du har også mulighed for at fylde op eller fjerne din nøgle.',
        '8' => 'Gendan alle nuværende indstillinger <br> Der vises et link, som du kan indstille som din startside eller dit bogmærke for at beholde dine nuværende indstillinger.',
        '9' => 'Subtil reklame for vores egen tjeneste <br> Vi viser dig subtil reklame for vores egne tjenester. Du kan slå vores selvpromovering fra her.',
    ],
    'easy-help' => 'Ved at klikke på symbolet <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , får du adgang til en forenklet version af hjælpen.',
];
