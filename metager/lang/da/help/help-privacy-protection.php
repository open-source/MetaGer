<?php
return [
    'title' => 'MetaGer - Hjælp',
    'backarrow' => 'Tilbage',
    'tor' => [
        'title' => 'TOR skjult tjeneste <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-torhidden" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'I mange år har MetaGer skjult og ikke gemt IP-adresser. Disse adresser er dog midlertidigt synlige på MetaGer-serveren, mens en søgning kører: Hvis MetaGer skulle blive kompromitteret, kunne en angriber læse og gemme dine adresser. For at opfylde de højeste sikkerhedskrav driver vi en MetaGer-instans på Tor-netværket: MetaGer TOR Hidden Service - tilgængelig via: <a href="/tor/" target="_blank" rel="noopener">https://metager.de/tor/</a>. For at bruge den skal du bruge en særlig browser, som du kan downloade fra <a href="https://www.torproject.org/" target="_blank" rel="noopener">https://www.torproject.org/</a>.',
        '2' => 'Du kan få adgang til MetaGer i Tor-browseren på: http://metagerv65pwclop2rsfzg4jwowpavpwd6grhhlvdgsswvo6ii4akgyd.onion .',
    ],
    'proxy' => [
        'title' => 'Anonymisering af MetaGer Proxy Server <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-proxy" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'For at bruge den skal du blot klikke på \'OPEN ANONYMOUSLY\' nederst i resultatet på MetaGers resultatside. Din anmodning vil derefter blive dirigeret til målwebstedet via vores anonymiserende proxyserver, og dine personlige data vil forblive fuldt beskyttede. Vigtigt: Hvis du følger links på siderne fra dette tidspunkt, vil du fortsat være beskyttet af proxyen. Du kan dog ikke indtaste en ny adresse i adressefeltet øverst. I så fald vil du miste beskyttelsen. Du kan se, om du stadig er beskyttet i adressefeltet, som vil vise: https://proxy.suma-ev.de/?url=here er den aktuelle adresse.',
    ],
    'content' => [
        'title' => 'Tvivlsomt indhold / beskyttelse af unge <a title="For easy help, click here" href="/help/easy-language/privacy-protection#eh-content" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'explanation' => [
            '1' => 'Jeg har modtaget "hits", som jeg ikke bare synes er irriterende, men som efter min mening også indeholder ulovligt indhold!',
            '2' => 'Hvis du finder noget på internettet, som du mener er ulovligt eller skadeligt for mindreårige, kan du kontakte <a href="mailto:hotline@jugendschutz.net" target="_blank" rel="noopener">hotline@jugendschutz.net</a> via e-mail eller besøge <a href="http://www.jugendschutz.net/" target="_blank" rel="noopener">www.jugendschutz.net</a> og udfylde den klageformular, der findes der. Det er nyttigt at give en kort beskrivelse af, hvad du anser for at være ulovligt, og hvordan du er stødt på dette indhold. Du kan også rapportere tvivlsomt indhold direkte til os. For at gøre det skal du sende en e-mail til vores ungdomsbeskyttelsesansvarlige (<a href="mailto:jugendschutz@metager.de" target="_blank" rel="noopener">jugendschutz@metager.de</a>).',
        ],
    ],
    'maps' => [
        'title' => 'MetaGer-kort',
        '1' => 'At bevare privatlivets fred i de globale datagiganters tidsalder har også fået os til at udvikle <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a>: den (så vidt vi ved) eneste ruteplanlægger, der tilbyder fuld funktionalitet via en browser og app uden at gemme brugerens placering. Alt dette er verificerbart, fordi vores software er open source. Til brug af maps.metager.de anbefaler vi vores hurtige app-version. Du kan downloade vores apps fra <a href="/app" target="_blank">her</a> (eller selvfølgelig også fra Play Store).',
        '2' => 'Denne kortfunktion kan også tilgås fra MetaGer-søgningen (og vice versa). Når du har søgt efter en term i MetaGer, vil du se et nyt søgefokus \'Maps\' i øverste højre hjørne. Hvis du klikker på det, kommer du til et tilsvarende kort.',
        '3' => 'Når kortet indlæses, viser det de punkter (POI\'er = Points of Interest), som MetaGer har fundet, og som også er anført i højre kolonne. Når du zoomer, tilpasser denne liste sig til kortudsnittet. Hvis du holder musen over en markør på kortet eller i listen, fremhæves det tilsvarende element. Klik på \'Detaljer\' for at få flere oplysninger om det pågældende punkt fra databasen nedenfor.',
    ],
    'easy-help' => 'Ved at klikke på symbolet <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , får du adgang til en forenklet version af hjælpen.',
    'privacy' => [
        'title' => "Anonymitet og datasikkerhed",
        '1' => 'Sporing af cookies, sessions-ID\'er og IP-adresser <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-tracking" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '2' => 'Ingen af disse bruges, gemmes, opbevares eller på anden måde behandles her hos MetaGer (undtagelse: kortvarig opbevaring til beskyttelse mod hacking og botangreb). Fordi vi anser dette emne for ekstremt vigtigt, har vi også skabt måder at hjælpe dig med at opnå det højeste niveau af sikkerhed: MetaGer TOR Hidden Service og vores anonymiserende proxyserver.',
        '3' => "Du kan finde flere oplysninger nedenfor. Funktionerne er tilgængelige under 'Services' i navigationslinjen.",
    ],
];
