<?php
return [
    'opensearch' => 'MetaGer: sikker søgning og find, beskyttelse af privatlivets fred',
    'startseite' => 'MetaGer startside',
    'impressum' => 'meddelelse om websted',
    'search-placeholder' => 'Din søgeforespørgsel...',
    'metager3' => 'Du er i øjeblikket på en MetaGer-testversion.',
    'engines' => [
        'queried' => 'Forespurgte søgetjenester',
        'disabled' => 'Tilføj søgetjenester til forespørgslen',
        'payment_required' => 'Søgetjenester tilgængelige med <a href=\':link\'>MetaGer-nøgle</a>',
    ],
    'skiplinks' => [
        'heading' => 'Hop hurtigt til indhold',
        'results' => 'Spring til søgeresultater',
        'query' => 'Spring til inputfeltet for søgeforespørgslen',
        'settings' => 'Spring til søgeindstillinger',
        'navigation' => 'Spring til navigation',
        'return' => 'Du kan til enhver tid vende tilbage til denne menu ved at trykke på escape-tasten.',
    ],
];
