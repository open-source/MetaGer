<?php
return [
    'options' => [
        'savetab' => 'Gem resultatet i TAB',
        'headline' => 'Valgmuligheder',
        '1' => 'Start en ny søgning på dette domæne',
        '2' => 'Skjul :host',
        '3' => 'skjul *.:domæne',
        '4' => 'Partner',
        '5' => 'ÅBN ANONYMT',
        '6' => 'ÅBN I NY FANE',
        '7' => 'ÅBEN',
        'direct' => 'Direkte åben',
        '8' => 'FJERN REKLAMER',
        'togglelabel' => 'Skift mellem resultatindstillinger',
    ],
    'gefVon' => 'af',
    'advertisement' => 'Annonce',
    'providers' => 'udbydere',
    'proxytext' => 'Resultatlinket åbnes anonymt. Dine data vil ikke blive overført til destinationsservere. Nogle websider fungerer muligvis ikke som normalt.',
    'metagerkeytext' => 'Brug MetaGer uden reklamer',
    'news' => 'Relevante nyheder',
    'videos' => 'Videoer',
    'adblocker' => 'Et <a href=":resultlink">søgeresultat</a> fra <code>:host</code> blev fjernet her af din ad blocker. <a href=":infolink">Læs mere</a>',
    'image' => [
        'download' => 'Download billede',
        'copyright' => 'Billeder kan være ophavsretligt beskyttet.',
    ],
    'more-news' => 'mere',
    'alt' => [
        'more' => 'Mere',
    ],
];
