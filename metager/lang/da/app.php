<?php
return [
    'head' => [
        '1' => 'Metager Apps',
        '2' => 'MetaGer App',
        '3' => 'MetaGer Maps App',
        '4' => 'Installation',
    ],
    'disclaimer' => [
        '1' => 'På nuværende tidspunkt har vi kun en Android-version af vores app.',
    ],
    'metager' => [
        '1' => 'Denne app bringer den fulde Metager-kraft til din smartphone. Søg på nettet med ét tryk, mens du bevarer dit privatliv.',
        '2' => 'Der er to måder at hente vores app på: installer via Google Playstore eller (bedre for dit privatliv) hent den direkte fra vores server.',
        'playstore' => 'Google Playstore',
        'fdroid' => 'F-Droid Store',
        'manuell' => 'Manuel installation',
    ],
    'maps' => [
        '1' => 'Denne app giver en indbygget integration af <a href="https://maps.metager.de" target="_blank">MetaGer Maps</a> (drevet af <a href="https://www.openstreetmap.de/" target="_blank">Openstreetmap</a>) på din mobile Android-enhed.',
        '2' => 'Derfor kører ruteplanlæggeren og navigationstjenesten meget hurtigt på din smartphone. Appen er hurtigere sammenlignet med brugen i en mobil webbrowser. Og der er flere fordele - tjek det ud!',
        '3' => 'APK\'en til manuel installation er omkring 4 gange så stor som playstore-installationen (~250 MB), fordi den indeholder biblioteker til alle almindelige CPU-arkitekturer. Den integrerede updater kender din enheds CPU-arkitektur og installerer den korrekte (lille) version af appen ved den første opdatering. Hvis du selv kender din enheds arkitektur, kan du også <a href="https://gitlab.metager.de/metagermaps/android/-/releases/permalink/latest" target="_blank">installere den lille pakke direkte</a>.',
        'list' => [
            '1' => 'Adgang til positioneringsdata => Med GPS aktiveret kan vi levere bedre søgeresultater. Med dette får du adgang til trin-for-trin-navigation. <b> Vi gemmer naturligvis ikke dine data, og vi videregiver dem ikke til tredjeparter.</b>',
            '2' => 'APK\'en til manuel installation har en integreret updater. For at updateren kan fungere, beder appen om tilladelse til at sende notifikationer for at underrette dig om en tilgængelig opdatering og bruger Android-tilladelsen REQUEST_INSTALL_PACKAGES, så den kan bede dig om at installere app-opdateringen.',
        ],
        '4' => 'Efter den første start vil du blive bedt om følgende tilladelser:',
    ],
];
