<?php
return [
    'plugin' => 'Installer MetaGer',
    'plugin-title' => 'Tilføj MetaGer til din browser',
    'key' => [
        'placeholder' => 'Indtast medlemsnøgle',
        'tooltip' => [
            'nokey' => 'Opsæt annoncefri søgning',
            'empty' => 'Token brugt op. Genoplad nu.',
            'low' => 'Token snart opbrugt. Genoplad nu.',
            'full' => 'Annoncefri søgning aktiveret.',
        ],
    ],
    'placeholder' => 'MetaGer: Privatlivsbeskyttet søgning og find',
    'searchbutton' => 'Start MetaGer-søgning',
    'foki' => [
        'web' => 'Web',
        'bilder' => 'Billeder',
        'nachrichten' => 'Nyheder',
        'science' => 'Videnskab',
        'produkte' => 'Produkter',
        'maps' => 'Kort',
    ],
    'adfree' => 'Brug MetaGer uden reklamer',
    'skip' => [
        'search' => 'Spring til indtastning af søgeforespørgsel',
        'navigation' => 'Spring til navigation',
        'fokus' => 'Spring til valg af søgefokus',
    ],
    'lang' => 'wwitch-sprog',
    'searchreset' => 'Slet input til søgeforespørgsel',
    'searchbar-replacement' => [
        'message' => 'MetaGer-søgemaskinen er nu kun tilgængelig uden reklamer!',
        'login' => 'Login med nøgle',
        'start' => 'Opret en ny nøgle',
        'why' => 'Hvorfor?',
        'key_error' => "Den indtastede nøgle var ikke gyldig. Kontroller venligst indtastningen.",
        'login_code_error' => "Den indtastede login-kode var ikke gyldig. Tip: Login-koder er kun gyldige, når de er synlige på en anden enhed!",
        'payment_id_error' => "Du har indtastet et betalingsid, som ikke er en korrekt nøgle. Din nøgle er 36 tegn lang.",
    ],
];
