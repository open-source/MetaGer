<?php
return [
    'title' => 'Meddelelse om websted',
    'headline' => [
        '1' => '<a href="http://suma-ev.de/" target="_blank" rel="noopener">SUMA-EV - Verein für freien Wissenszugang (e.V.)</a>',
    ],
    'info' => [
        '2' => "SUMA-EV\r
Postfach 51 01 43\r
D-30631 Hannover\r
Deutschland/Tyskland",
        '1' => 'Wikipedia-artikel om <a href="http://de.wikipedia.org/wiki/Suma_e.V." target="_blank" rel="noopener">SUMA-EV</a>',
        '3' => "Kontakt til os:\r
Tlf: +4951134000070\r
E-mail: <a href=\"/en-GB/kontakt\">Krypteret kontaktformular</a>",
        '4' => 'Bestyrelsen: Dominik Hebeler, Phil Höfer, Carsten Riel, Manuela Branz',
        '6' => 'Kommissær for beskyttelse af unge: Manuela Branz <a href="mailto:jugendschutz@metager.de">jugendschutz@metager.de</a>',
        '8' => '"SUMA-EV - Verein für freien Wissenszugang" er en velgørende forening, der er registreret i foreningsregistret ved Amtsgericht Hannover under VR200033. Identifikationsnummer for moms: DE 300 464 091. "Gottfried Wilhelm Leibniz Universität Hannover" er et lovpligtigt organ.',
        '9' => 'Note om ansvar:',
        '10' => 'Trods grundig indholdskontrol påtager vi os intet ansvar for indholdet af eksterne links. For indholdet af eksterne links er kun deres operatører pålidelige.',
    ],
];
