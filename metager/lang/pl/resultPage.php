<?php
return [
    'opensearch' => 'MetaGer: bezpieczne wyszukiwanie i znajdowanie, ochrona prywatności',
    'startseite' => 'Strona startowa MetaGer',
    'impressum' => 'zawiadomienie o witrynie',
    'search-placeholder' => 'Twoje zapytanie...',
    'metager3' => 'Obecnie korzystasz z wersji testowej MetaGer.',
    'engines' => [
        'queried' => 'Zapytania dotyczące usług wyszukiwania',
        'disabled' => 'Dodaj usługi wyszukiwania do zapytania',
        'payment_required' => 'Usługi wyszukiwania dostępne z kluczem <a href=\':link\'>MetaGer</a>',
    ],
    'skiplinks' => [
        'return' => 'Do tego menu można powrócić w dowolnym momencie, naciskając klawisz escape',
        'heading' => 'Szybkie przejście do treści',
        'results' => 'Przejdź do wyników wyszukiwania',
        'query' => 'Przejdź do pola wprowadzania zapytania wyszukiwania',
        'settings' => 'Przejdź do ustawień wyszukiwania',
        'navigation' => 'Przejdź do nawigacji',
    ],
];
