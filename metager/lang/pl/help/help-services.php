<?php
return [
    'title' => 'MetaGer - Pomoc',
    'backarrow' => 'Powrót',
    'app' => [
        'title' => 'Aplikacja na Androida <a title="For easy help, click here" href="/hilfe/easy-language/services#help-app" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Możesz również używać MetaGer jako aplikacji. Wystarczy pobrać aplikację <a href="https://metager.de/app" target="_blank" rel="noopener">MetaGer App</a> na smartfon z systemem Android.',
    ],
    'widget' => [
        'title' => 'MetaGer Widget <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-widget" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Jest to generator kodu, który umożliwia osadzenie MetaGer na swojej stronie internetowej. Można go używać do wyszukiwania na własnej stronie lub w Internecie. W przypadku jakichkolwiek pytań, prosimy o skorzystanie z <a href="/kontakt/" target="_blank" rel="noopener">naszego formularza kontaktowego</a>.',
    ],
    'maps' => [
        'title' => 'MetaGer Maps <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-maps" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Zachowanie prywatności w dobie globalnych gigantów danych doprowadziło nas również do opracowania <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a>: (według naszej wiedzy) jedynego narzędzia do planowania tras, które oferuje pełną funkcjonalność za pośrednictwem przeglądarki i aplikacji bez przechowywania lokalizacji użytkownika. Wszystko to można zweryfikować, ponieważ nasze oprogramowanie jest open source. Do korzystania z maps.metager.de zalecamy naszą szybką wersję aplikacji. Nasze aplikacje można pobrać ze strony <a href="/app" target="_blank">tutaj</a> (lub oczywiście również ze Sklepu Play).',
        '2' => 'Ta funkcja mapy może być również dostępna z wyszukiwarki MetaGer (i odwrotnie). Po wyszukaniu terminu w MetaGer, w prawym górnym rogu pojawi się nowe pole wyszukiwania "Mapy". Kliknięcie go spowoduje przejście do odpowiedniej mapy.',
        '3' => 'Po załadowaniu mapa wyświetla punkty (POI = Points of Interest) znalezione przez MetaGer, które są również wymienione w prawej kolumnie. Podczas powiększania lista ta dostosowuje się do sekcji mapy. Najechanie kursorem myszy na znacznik na mapie lub na liście powoduje podświetlenie odpowiedniego elementu. Kliknij "Szczegóły", aby uzyskać więcej informacji o tym punkcie z poniższej bazy danych.',
    ],
    'easy-help' => 'Klikając na symbol <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , można uzyskać dostęp do uproszczonej wersji pomocy.',
    'services' => [
        'text' => "Dodatkowe usługi związane z wyszukiwaniem",
    ],
];
