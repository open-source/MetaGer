<?php
return [
    'backarrow' => 'Powrót',
    'title' => [
        '2' => 'Korzystanie ze stron głównych',
        '1' => 'MetaGer - Pomoc',
    ],
    'startpage' => [
        'title' => 'Strona startowa <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-startpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'Strona startowa zawiera pole wyszukiwania, przycisk w prawym górnym rogu, aby uzyskać dostęp do menu, oraz dwa linki poniżej pola wyszukiwania, aby dodać MetaGer do przeglądarki i wyszukiwać bez reklam. W dolnej części znajdują się informacje o MetaGer i stowarzyszeniu SUMA-EV. Ponadto na dole wyświetlane są nasze obszary tematyczne <i>Gwarantowana prywatność</i>, Stowarzyszenie non-profit</i>, Różnorodność i bezpłatność</i> oraz <i>100% zielonej energii</i>. Klikając odpowiednie sekcje lub przewijając, można znaleźć więcej informacji. ',
    ],
    'searchfield' => [
        'title' => 'Pole wyszukiwania <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#eh-searchfield" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'Pole wyszukiwania składa się z kilku części:',
        'memberkey' => 'symbol klucza: Tutaj możesz wprowadzić swój klucz, aby korzystać z wyszukiwania bez reklam. Możesz także wyświetlić saldo tokenów i zarządzać kluczem.',
        'slot' => 'pole wyszukiwania: Wpisz tutaj wyszukiwane hasło. Wielkie i małe litery nie są rozróżniane.',
        'search' => 'szkło powiększające: Rozpocznij wyszukiwanie, klikając tutaj lub naciskając "Enter".',
        'morefunctions' => 'Dodatkowe funkcje można znaleźć w punkcie menu "<a href = "/hilfe/funktionen">Funkcje wyszukiwania</a>".',
    ],
    'resultpage' => [
        'title' => 'Strona wyników <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-resultpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'foci' => 'Pod polem wyszukiwania znajdują się 3 różne obszary wyszukiwania (Sieć, Obrazy, Wiadomości), z których każdy jest powiązany z określonymi wyszukiwarkami.',
        'choice' => 'Poniżej znajdują się dwa elementy: "Filtr" i "Ustawienia", jeśli dotyczy.',
        'filter' => 'Filtr: Tutaj można wyświetlać i ukrywać opcje filtrowania oraz stosować filtry. W każdym fokusie wyszukiwania dostępne są różne opcje wyboru. Niektóre funkcje są dostępne tylko przy użyciu klawisza MetaGer.',
        'settings' => 'Ustawienia: W tym miejscu można wprowadzić stałe ustawienia wyszukiwania MetaGer w bieżącym fokusie. Możesz także wybrać i odznaczyć wyszukiwarki powiązane z fokusem. Ustawienia są zapisywane za pomocą pliku cookie typu plaintext, który nie umożliwia identyfikacji użytkownika. Dostęp do strony ustawień można również uzyskać za pomocą menu w prawym górnym rogu.',
    ],
    'result' => [
        'title' => 'Wyniki <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-results" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => [
            '1' => 'Wszystkie wyniki są prezentowane w następującym formacie:',
            'open' => '"OPEN": Kliknij nagłówek lub poniższy link (adres URL), aby otworzyć wynik w tej samej karcie.',
            'newtab' => '"OTWÓRZ W NOWEJ KARCIE" otwiera wynik w nowej karcie. Alternatywnie, można również otworzyć nową kartę za pomocą CTRL i kliknięcia lewym przyciskiem myszy lub środkowym przyciskiem myszy.',
            'anonym' => '"OPEN ANONYMOUSLY" oznacza, że wynik jest otwierany pod ochroną naszego serwera proxy. Więcej informacji na ten temat można znaleźć w sekcji <a href = "/hilfe/datensicherheit#h-proxy">Anonimizacja serwera proxy MetaGer</a>.',
            'more' => '<img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>: Po kliknięciu na <img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/> pojawią się nowe opcje; wygląd wyniku ulegnie zmianie:',
            '2' => 'Nowe opcje to:',
            'domainnewsearch' => '"Rozpocznij nowe wyszukiwanie w tej domenie": Wykonywane jest bardziej szczegółowe wyszukiwanie w domenie wyniku.',
            'hideresult' => '"hide": Umożliwia ukrycie wyników z tej domeny. Możesz również wpisać ten przełącznik bezpośrednio po wyszukiwanym terminie i połączyć go; dozwolony jest również symbol wieloznaczny "*". Zobacz także Ustawienia, aby uzyskać stałe rozwiązanie.',
        ],
    ],
    'settings' => [
        'title' => 'Ustawienia <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-settings" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '2' => 'Używane wyszukiwarki <br> Tutaj można wyświetlić i dostosować używane wyszukiwarki. Klikając odpowiednią nazwę, można ją odpowiednio włączyć lub wyłączyć.',
        '3' => 'Filtry wyszukiwania <br> Filtry wyszukiwania pozwalają na stałe filtrowanie wyszukiwania.',
        '4' => 'Czarna lista <br> Tutaj można utworzyć osobistą czarną listę. Można jej używać do filtrowania określonych domen i tworzenia własnych ustawień wyszukiwania. Po kliknięciu przycisku "Dodaj" ustawienia te zostaną dołączone do linku w sekcji "Uwaga".',
        '5' => 'Przełączanie trybu ciemnego <br> Tutaj można łatwo przełączyć się na tryb ciemny.',
        '6' => 'Otwórz wyniki w nowej karcie <br> Tutaj można na stałe włączyć funkcję otwierania wyników w nowej karcie.',
        '7' => 'Cytaty <br> Tutaj można włączać i wyłączać wyświetlanie cytatów.',
        '1' => 'Wyszukiwanie bez reklam <br> Tutaj możesz sprawdzić saldo swojego klucza. Dostępne są również opcje doładowania lub usunięcia klucza.',
        '8' => 'Przywróć wszystkie bieżące ustawienia <br> Zostanie wyświetlony link, który można ustawić jako stronę główną lub zakładkę, aby zachować bieżące ustawienia.',
        '9' => 'Subtelna reklama naszych własnych usług <br> Pokazujemy subtelną reklamę naszych własnych usług. Możesz wyłączyć naszą autopromocję tutaj.',
    ],
    'easy-help' => 'Klikając na symbol <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , można uzyskać dostęp do uproszczonej wersji pomocy.',
];
