<?php
return [
    'title' => 'MetaGer - Pomoc',
    'backarrow' => 'Powrót',
    'tor' => [
        'title' => 'Ukryta usługa TOR <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-torhidden" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Od wielu lat MetaGer ukrywa i nie przechowuje adresów IP. Adresy te są jednak tymczasowo widoczne na serwerze MetaGer podczas wyszukiwania: jeśli MetaGer zostałby przejęty, atakujący mógłby odczytać i zapisać adresy użytkowników. Aby spełnić najwyższe wymagania bezpieczeństwa, obsługujemy instancję MetaGer w sieci Tor: MetaGer TOR Hidden Service - dostępną pod adresem: <a href="/tor/" target="_blank" rel="noopener">https://metager.de/tor/</a>. Aby z niej skorzystać, potrzebna jest specjalna przeglądarka, którą można pobrać ze strony <a href="https://www.torproject.org/" target="_blank" rel="noopener">https://www.torproject.org/</a>.',
        '2' => 'Dostęp do MetaGer w przeglądarce Tor można uzyskać pod adresem: http://metagerv65pwclop2rsfzg4jwowpavpwd6grhhlvdgsswvo6ii4akgyd.onion .',
    ],
    'proxy' => [
        'title' => 'Anonimizacja serwera proxy MetaGer <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-proxy" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Aby z niego skorzystać, wystarczy kliknąć "OPEN ANONYMOUSLY" na dole wyników na stronie wyników MetaGer. Twoje żądanie zostanie następnie przekierowane do strony docelowej za pośrednictwem naszego anonimizującego serwera proxy, a Twoje dane osobowe pozostaną w pełni chronione. Ważne: jeśli od tego momentu będziesz korzystać z linków na stronach, pozostaniesz chroniony przez serwer proxy. Nie można jednak wprowadzić nowego adresu w polu adresu u góry. W takim przypadku użytkownik utraci ochronę. Możesz sprawdzić, czy nadal jesteś chroniony w polu adresu, które wyświetli: https://proxy.suma-ev.de/?url=here to rzeczywisty adres.',
    ],
    'content' => [
        'title' => 'Wątpliwa zawartość / ochrona młodzieży <a title="For easy help, click here" href="/help/easy-language/privacy-protection#eh-content" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'explanation' => [
            '1' => 'Otrzymywałem "trafienia", które nie tylko mnie irytowały, ale także zawierały, moim zdaniem, nielegalne treści!',
            '2' => 'Jeśli znajdziesz w Internecie treści, które uważasz za nielegalne lub szkodliwe dla małoletnich, możesz skontaktować się z <a href="mailto:hotline@jugendschutz.net" target="_blank" rel="noopener">hotline@jugendschutz.net</a> pocztą elektroniczną lub odwiedzić <a href="http://www.jugendschutz.net/" target="_blank" rel="noopener">www.jugendschutz.net</a> i wypełnić dostępny tam formularz skargi. Pomocne jest podanie krótkiej notatki na temat tego, co uważasz za niedopuszczalne i w jaki sposób natknąłeś się na te treści. Wątpliwe treści można również zgłaszać bezpośrednio do nas. W tym celu należy wysłać wiadomość e-mail do naszego specjalisty ds. ochrony młodzieży (<a href="mailto:jugendschutz@metager.de" target="_blank" rel="noopener">jugendschutz@metager.de</a>).',
        ],
    ],
    'maps' => [
        'title' => 'MetaGer Maps',
        '1' => 'Zachowanie prywatności w dobie globalnych gigantów danych doprowadziło nas również do opracowania <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a>: (według naszej wiedzy) jedynego narzędzia do planowania tras, które oferuje pełną funkcjonalność za pośrednictwem przeglądarki i aplikacji bez przechowywania lokalizacji użytkownika. Wszystko to można zweryfikować, ponieważ nasze oprogramowanie jest open source. Do korzystania z maps.metager.de zalecamy naszą szybką wersję aplikacji. Nasze aplikacje można pobrać ze strony <a href="/app" target="_blank">tutaj</a> (lub oczywiście również ze Sklepu Play).',
        '2' => 'Ta funkcja mapy może być również dostępna z wyszukiwarki MetaGer (i odwrotnie). Po wyszukaniu terminu w MetaGer, w prawym górnym rogu pojawi się nowe pole wyszukiwania "Mapy". Kliknięcie go spowoduje przejście do odpowiedniej mapy.',
        '3' => 'Po załadowaniu mapa wyświetla punkty (POI = Points of Interest) znalezione przez MetaGer, które są również wymienione w prawej kolumnie. Podczas powiększania lista ta dostosowuje się do sekcji mapy. Najechanie kursorem myszy na znacznik na mapie lub na liście powoduje podświetlenie odpowiedniego elementu. Kliknij "Szczegóły", aby uzyskać więcej informacji o tym punkcie z poniższej bazy danych.',
    ],
    'easy-help' => 'Klikając na symbol <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , można uzyskać dostęp do uproszczonej wersji pomocy.',
    'privacy' => [
        'title' => "Anonimowość i bezpieczeństwo danych",
        '1' => 'Śledzące pliki cookie, identyfikatory sesji i adresy IP <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-tracking" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '3' => "Więcej informacji można znaleźć poniżej. Funkcje są dostępne w sekcji \"Usługi\" na pasku nawigacyjnym.",
        '2' => 'Żadne z nich nie są wykorzystywane, przechowywane, zatrzymywane ani w inny sposób przetwarzane w MetaGer (wyjątek: krótkoterminowe przechowywanie w celu ochrony przed atakami hakerów i botów). Ponieważ uważamy ten temat za niezwykle ważny, stworzyliśmy również sposoby, które pomogą ci osiągnąć najwyższy poziom bezpieczeństwa: MetaGer TOR Hidden Service i nasz anonimizujący serwer proxy.',
    ],
];
