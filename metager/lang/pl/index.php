<?php
return [
    'plugin' => 'Zainstaluj MetaGer',
    'plugin-title' => 'Dodaj MetaGer do swojej przeglądarki',
    'key' => [
        'placeholder' => 'Wprowadź klucz członkowski',
        'tooltip' => [
            'nokey' => 'Skonfiguruj wyszukiwanie bez reklam',
            'empty' => 'Żeton zużyty. Doładuj teraz.',
            'low' => 'Żeton wkrótce się zużyje. Doładuj teraz.',
            'full' => 'Wyszukiwanie bez reklam włączone.',
        ],
    ],
    'placeholder' => 'MetaGer: Wyszukiwanie i znajdowanie chronione prywatnością',
    'searchbutton' => 'Uruchom MetaGer-Search',
    'foki' => [
        'web' => 'Sieć',
        'bilder' => 'Obrazy',
        'nachrichten' => 'Aktualności',
        'science' => 'Nauka',
        'produkte' => 'Produkty',
        'maps' => 'Mapy',
    ],
    'adfree' => 'Korzystaj z MetaGer bez reklam',
    'skip' => [
        'search' => 'Przejdź do wprowadzania zapytania wyszukiwania',
        'navigation' => 'Przejdź do nawigacji',
        'fokus' => 'Przejdź do wyboru fokusu wyszukiwania',
    ],
    'lang' => 'język przełącznika',
    'searchreset' => 'usuń wprowadzone zapytanie wyszukiwania',
    'searchbar-replacement' => [
        'start' => 'Tworzenie nowego klucza',
        'message' => 'Wyszukiwarka MetaGer jest teraz dostępna wyłącznie bez reklam!',
        'login' => 'Logowanie za pomocą klucza',
        'key_error' => "Wprowadzony klucz był nieprawidłowy. Sprawdź wprowadzone dane.",
        'why' => 'dlaczego?',
        'login_code_error' => "Wprowadzony kod logowania był nieprawidłowy. Wskazówka: Kody logowania są ważne tylko wtedy, gdy są widoczne na innym urządzeniu!",
        'payment_id_error' => "Wprowadzono identyfikator płatności, który nie jest prawidłowym kluczem. Klucz ma długość 36 znaków.",
    ],
];
