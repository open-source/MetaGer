<?php
return [
    'head' => [
        '1' => 'Aplikacje Metager',
        '2' => 'Aplikacja MetaGer',
        '3' => 'Aplikacja MetaGer Maps',
        '4' => 'Instalacja',
    ],
    'disclaimer' => [
        '1' => 'W tej chwili mamy tylko wersję naszej aplikacji na Androida.',
    ],
    'metager' => [
        '1' => 'Ta aplikacja zapewnia pełną moc Metagera na smartfonie. Przeszukuj sieć jednym dotknięciem, zachowując prywatność.',
        '2' => 'Istnieją dwa sposoby pobrania naszej aplikacji: instalacja za pośrednictwem sklepu Google Playstore lub (lepiej dla prywatności) pobranie jej bezpośrednio z naszego serwera.',
        'playstore' => 'Google Playstore',
        'fdroid' => 'F-Droid Store',
        'manuell' => 'Instalacja ręczna',
    ],
    'maps' => [
        '1' => 'Ta aplikacja zapewnia natywną integrację <a href="https://maps.metager.de" target="_blank">MetaGer Maps</a> (obsługiwanej przez <a href="https://www.openstreetmap.de/" target="_blank">Openstreetmap</a>) na urządzeniu mobilnym z systemem Android.',
        '2' => 'Dzięki temu usługa planowania trasy i nawigacji działa bardzo szybko na smartfonie. Aplikacja jest szybsza w porównaniu do korzystania z mobilnej przeglądarki internetowej. Jest jeszcze kilka innych zalet - sprawdź!',
        '3' => 'APK do ręcznej instalacji ma około 4x większy rozmiar niż instalacja ze sklepu Playstore (~250 MB), ponieważ zawiera biblioteki dla wszystkich popularnych architektur procesorów. Zintegrowany aktualizator będzie znał architekturę procesora urządzenia i zainstaluje poprawną (małą) wersję aplikacji przy pierwszej aktualizacji. Jeśli znasz architekturę swojego urządzenia, możesz również <a href="https://gitlab.metager.de/metagermaps/android/-/releases/permalink/latest" target="_blank">bezpośrednio zainstalować mały pakiet</a>.',
        'list' => [
            '1' => 'Dostęp do danych pozycjonowania => Dzięki aktywacji GPS możemy zapewnić lepsze wyniki wyszukiwania. Dzięki temu uzyskujesz dostęp do nawigacji krok po kroku. <b> Oczywiście nie przechowujemy żadnych danych użytkownika i nie udostępniamy ich osobom trzecim.</b>',
            '2' => 'APK do ręcznej instalacji ma zintegrowany aktualizator. Aby aktualizator działał, aplikacja poprosi o pozwolenie na publikowanie powiadomień w celu powiadamiania o dostępnej aktualizacji i korzysta z uprawnienia Androida REQUEST_INSTALL_PACKAGES, dzięki czemu może poprosić o zainstalowanie aktualizacji aplikacji',
        ],
        '4' => 'Po pierwszym uruchomieniu zostaniesz poproszony o następujące uprawnienia:',
    ],
];
