<?php
return [
    'head' => [
        '2' => 'App MetaGer',
        '1' => 'Applicazioni MetaGer',
        '3' => 'App MetaGer Maps',
        '4' => 'Installazione',
    ],
    'maps' => [
        '3' => 'L\'APK per l\'installazione manuale è circa 4 volte più grande di quello del playstore (~250MB) perché contiene librerie per tutte le architetture CPU più comuni. Il programma di aggiornamento integrato conosce l\'architettura della CPU del dispositivo e installa la versione corretta (piccola) dell\'app al primo aggiornamento. Se si conosce l\'architettura del proprio dispositivo, si può anche <a href="https://gitlab.metager.de/metagermaps/android/-/releases/permalink/latest" target="_blank">installare direttamente il pacchetto piccolo</a>.',
        '1' => 'Questa App fornisce un\'integrazione nativa di <a href="https://maps.metager.de" target="_blank">MetaGer Maps</a> (powered by <a href="https://www.openstreetmap.de/" target="_blank">Openstreetmap</a>) sul vostro dispositivo mobile Android.',
        '2' => 'Pertanto, il pianificatore di itinerari e il servizio di navigazione funzionano molto velocemente sul vostro smartphone. L\'applicazione è più veloce rispetto all\'utilizzo in un browser web mobile. E ci sono altri vantaggi... scopriteli!',
        'list' => [
            '1' => 'Accesso ai dati di posizionamento => Con il GPS attivato possiamo fornire risultati di ricerca migliori. In questo modo si ottiene l\'accesso alla navigazione passo-passo. <b> Naturalmente, non memorizziamo i vostri dati e non li cediamo a terzi.</b>',
            '2' => 'L\'APK per l\'installazione manuale ha un programma di aggiornamento integrato. Affinché il programma di aggiornamento funzioni, l\'applicazione chiederà il permesso di inviare notifiche per avvisare l\'utente di un aggiornamento disponibile e utilizzerà il permesso Android REQUEST_INSTALL_PACKAGES per chiedere all\'utente di installare l\'aggiornamento dell\'applicazione.',
        ],
        '4' => 'Dopo il primo avvio verranno richieste le seguenti autorizzazioni:',
    ],
    'disclaimer' => [
        '1' => 'Al momento disponiamo solo di una versione Android della nostra applicazione.',
    ],
    'metager' => [
        '1' => 'Questa applicazione porta tutta la potenza di Metager sul vostro smartphone. Cercate sul web con un solo tocco, preservando la vostra privacy.',
        '2' => 'Ci sono due modi per ottenere la nostra App: installarla tramite il Playstore di Google o (meglio per la vostra privacy) ottenerla direttamente dal nostro server.',
        'playstore' => 'Google Playstore',
        'fdroid' => 'Negozio F-Droid',
        'manuell' => 'Installazione manuale',
    ],
];
