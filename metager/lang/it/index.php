<?php
return [
    'plugin' => 'Installare MetaGer',
    'plugin-title' => 'Aggiungi MetaGer al tuo browser',
    'key' => [
        'placeholder' => 'Inserire la chiave membro',
        'tooltip' => [
            'nokey' => 'Impostare la ricerca senza pubblicità',
            'empty' => 'Gettone esaurito. Ricarica ora.',
            'low' => 'Gettone presto esaurito. Ricarica ora.',
            'full' => 'Ricerca senza annunci attivata.',
        ],
    ],
    'placeholder' => 'MetaGer: Ricerca e ricerca protetta dalla privacy',
    'searchbutton' => 'Avviare la ricerca MetaGer',
    'foki' => [
        'web' => 'Web',
        'bilder' => 'Immagini',
        'nachrichten' => 'Notizie',
        'science' => 'Scienza',
        'produkte' => 'Prodotti',
        'maps' => 'Mappe',
    ],
    'adfree' => 'Utilizzare MetaGer senza pubblicità',
    'skip' => [
        'search' => 'Passa all\'inserimento della query di ricerca',
        'navigation' => 'Vai alla navigazione',
        'fokus' => 'Passa alla selezione del focus di ricerca',
    ],
    'lang' => 'linguaggio wwitch',
    'searchreset' => 'eliminare l\'input della query di ricerca',
    'searchbar-replacement' => [
        'message' => 'Il motore di ricerca MetaGer è ora disponibile solo senza pubblicità!',
        'login' => 'Accesso con chiave',
        'start' => 'Creare una nuova chiave',
        'why' => 'Perché?',
        'key_error' => "La chiave inserita non è valida. Controllare l'immissione.",
        'login_code_error' => "Il codice di accesso inserito non è valido. Suggerimento: i codici di accesso sono validi solo quando sono visibili su un altro dispositivo!",
        'payment_id_error' => "È stato inserito un ID di pagamento che non è una chiave corretta. La chiave è lunga 36 caratteri.",
    ],
];
