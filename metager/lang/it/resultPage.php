<?php
return [
    'opensearch' => 'MetaGer: ricerca e ricerca sicura, protezione della privacy',
    'startseite' => 'Pagina iniziale di MetaGer',
    'impressum' => 'avviso di sito',
    'search-placeholder' => 'La tua ricerca...',
    'metager3' => 'Siete attualmente su una versione di prova di MetaGer.',
    'engines' => [
        'queried' => 'Servizi di ricerca interrogati',
        'disabled' => 'Aggiungere servizi di ricerca alla query',
        'payment_required' => 'Servizi di ricerca disponibili con la chiave <a href=\':link\'>MetaGer</a>',
    ],
    'skiplinks' => [
        'heading' => 'Saltare rapidamente al contenuto',
        'results' => 'Vai ai risultati della ricerca',
        'query' => 'Vai al campo di immissione della query di ricerca',
        'settings' => 'Vai alle impostazioni di ricerca',
        'navigation' => 'Vai alla navigazione',
        'return' => 'È possibile tornare a questo menu in qualsiasi momento premendo il tasto di escape.',
    ],
];
