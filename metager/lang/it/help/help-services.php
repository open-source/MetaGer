<?php
return [
    'title' => 'MetaGer - Aiuto',
    'backarrow' => 'Indietro',
    'app' => [
        'title' => 'Applicazione Android <a title="For easy help, click here" href="/hilfe/easy-language/services#help-app" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'È possibile utilizzare MetaGer anche come applicazione. È sufficiente scaricare l\'App <a href="https://metager.de/app" target="_blank" rel="noopener">MetaGer</a> sul vostro smartphone Android.',
    ],
    'widget' => [
        'title' => 'Widget MetaGer <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-widget" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Questo è un generatore di codice che consente di incorporare MetaGer nel proprio sito web. È possibile utilizzarlo per eseguire ricerche sul proprio sito o su Internet, a seconda delle esigenze. Per qualsiasi domanda, utilizzate <a href="/kontakt/" target="_blank" rel="noopener">il nostro modulo di contatto</a>.',
    ],
    'maps' => [
        'title' => 'Mappe MetaGer <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-maps" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'La tutela della privacy nell\'era dei giganti globali dei dati ci ha portato anche a sviluppare <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a>: l\'unico (a nostra conoscenza) pianificatore di itinerari che offre tutte le funzionalità tramite browser e app senza memorizzare la posizione dell\'utente. Tutto questo è verificabile perché il nostro software è open source. Per l\'utilizzo di maps.metager.de, consigliamo la nostra versione veloce dell\'app. È possibile scaricare le nostre app da <a href="/app" target="_blank">qui</a> (o naturalmente anche dal Play Store).',
        '2' => 'È possibile accedere alla funzione mappe anche dalla ricerca di MetaGer (e viceversa). Dopo aver cercato un termine in MetaGer, nell\'angolo in alto a destra apparirà un nuovo focus di ricerca "Mappe". Facendo clic su di esso si accede alla mappa corrispondente.',
        '3' => 'Al momento del caricamento, la mappa visualizza i punti (POI = Point of Interest) trovati da MetaGer, che sono elencati anche nella colonna di destra. Quando si esegue lo zoom, l\'elenco si adatta alla sezione della mappa. Passando il mouse su un indicatore della mappa o dell\'elenco si evidenzia l\'elemento corrispondente. Fare clic su "Dettagli" per ottenere ulteriori informazioni su quel punto dal database sottostante.',
    ],
    'easy-help' => 'Facendo clic sul simbolo <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , si accede a una versione semplificata della guida.',
    'services' => [
        'text' => "Servizi aggiuntivi per la ricerca",
    ],
];
