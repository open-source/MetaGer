<?php
return [
    'backarrow' => 'Indietro',
    'title' => [
        '1' => 'MetaGer - Aiuto',
        '2' => 'Utilizzo delle pagine principali',
    ],
    'startpage' => [
        'title' => 'La pagina iniziale <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-startpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'La pagina iniziale comprende il campo di ricerca, un pulsante in alto a destra per accedere al menu e due link sotto il campo di ricerca per aggiungere MetaGer al browser e effettuare ricerche senza pubblicità. Nella sezione inferiore si trovano informazioni su MetaGer e sull\'associazione SUMA-EV. Inoltre, le nostre aree di interesse <i>Privacy garantita, Associazione senza scopo di lucro, Diverse e gratuite</i> e <i>100% Energia verde</i> sono visualizzate in basso. Facendo clic sulle rispettive sezioni o scorrendo, è possibile trovare ulteriori informazioni. ',
    ],
    'searchfield' => [
        'title' => 'Il campo di ricerca <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#eh-searchfield" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'Il campo di ricerca è composto da diverse parti:',
        'memberkey' => 'il simbolo della chiave: Qui è possibile inserire la chiave per utilizzare la ricerca senza pubblicità. È inoltre possibile visualizzare il saldo dei gettoni e gestire la chiave.',
        'slot' => 'il campo di ricerca: Inserire qui il termine di ricerca. Le lettere maiuscole e minuscole non vengono distinte.',
        'search' => 'la lente di ingrandimento: Avviare la ricerca facendo clic qui o premendo "Invio".',
        'morefunctions' => 'Ulteriori funzioni sono disponibili alla voce di menu "<a href = "/hilfe/funktionen">Funzioni di ricerca</a>".',
    ],
    'resultpage' => [
        'title' => 'La pagina dei risultati <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-resultpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'foci' => 'Sotto il campo di ricerca sono presenti 3 diversi campi di ricerca (Web, Immagini, Notizie), ognuno dei quali è associato a motori di ricerca specifici.',
        'choice' => 'In basso sono presenti due voci: "Filtro" e "Impostazioni", se applicabile.',
        'filter' => 'Filtro: Qui è possibile mostrare e nascondere le opzioni di filtro e applicare i filtri. In ogni focus di ricerca sono disponibili diverse opzioni di selezione. Alcune funzioni sono disponibili solo quando si utilizza una chiave MetaGer.',
        'settings' => 'Impostazioni: Qui è possibile effettuare impostazioni di ricerca permanenti per la ricerca MetaGer nel focus corrente. È inoltre possibile selezionare e deselezionare i motori di ricerca associati al focus. Le impostazioni vengono salvate tramite un cookie di testo non identificabile personalmente. È possibile accedere alla pagina delle impostazioni anche tramite il menu in alto a destra.',
    ],
    'result' => [
        'title' => 'Risultati <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-results" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => [
            '1' => 'Tutti i risultati sono presentati nel seguente formato:',
            'open' => '"APRI": Fare clic sul titolo o sul link sottostante (l\'URL) per aprire il risultato nella stessa scheda.',
            'newtab' => '"APRI IN NUOVA TABELLA" apre il risultato in una nuova scheda. In alternativa, è possibile aprire una nuova scheda utilizzando CTRL e il tasto sinistro del mouse o il tasto centrale del mouse.',
            'anonym' => '"APRIRE IN ANONIMATO" significa che il risultato viene aperto sotto la protezione del nostro proxy. Alcune informazioni al riguardo sono disponibili nella sezione <a href = "/hilfe/datensicherheit#h-proxy">Anonymizing MetaGer Proxy Server</a>.',
            'more' => '<img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>: Facendo clic su <img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>, si ottengono nuove opzioni; l\'aspetto del risultato cambia:',
            '2' => 'Le nuove opzioni sono:',
            'domainnewsearch' => '"Avvia una nuova ricerca su questo dominio": Viene eseguita una ricerca più dettagliata sul dominio del risultato.',
            'hideresult' => '"Nascondi": Consente di nascondere i risultati di questo dominio. È anche possibile scrivere questo interruttore direttamente dopo il termine di ricerca e concatenarlo; è consentito anche un carattere jolly "*". Vedere anche Impostazioni per una soluzione permanente.',
        ],
    ],
    'settings' => [
        'title' => 'Impostazioni <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-settings" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '2' => 'Motori di ricerca utilizzati <br> Qui è possibile visualizzare e regolare i motori di ricerca utilizzati. Facendo clic sul nome corrispondente, è possibile attivarlo o disattivarlo di conseguenza.',
        '3' => 'Filtri di ricerca <br> I filtri di ricerca consentono di filtrare la ricerca in modo permanente.',
        '4' => 'Lista nera <br> Qui è possibile creare una lista nera personale. È possibile utilizzarla per filtrare domini specifici e creare impostazioni di ricerca personalizzate. Facendo clic su "Aggiungi", queste impostazioni verranno aggiunte al link nella sezione "Nota".',
        '5' => 'Attivare la modalità scura <br> Passare facilmente alla modalità scura.',
        '6' => 'Aprire i risultati in una nuova scheda <br> Qui è possibile attivare in modo permanente la funzione di apertura dei risultati in una nuova scheda.',
        '7' => 'Citazioni <br> Qui è possibile attivare e disattivare la visualizzazione delle citazioni.',
        '1' => 'Ricerca senza pubblicità <br> Qui è possibile visualizzare il saldo della chiave e la propria chiave. È inoltre possibile ricaricare o rimuovere la chiave.',
        '8' => 'Ripristina tutte le impostazioni correnti <br> Verrà visualizzato un link che è possibile impostare come homepage o segnalibro per mantenere le impostazioni correnti.',
        '9' => 'Pubblicità sottile per i nostri servizi <br> Vi mostriamo pubblicità sottile per i nostri servizi. Potete disattivare la nostra autopromozione qui.',
    ],
    'easy-help' => 'Facendo clic sul simbolo <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , si accede a una versione semplificata della guida.',
];
