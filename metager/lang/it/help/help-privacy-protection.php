<?php
return [
    'title' => 'MetaGer - Aiuto',
    'backarrow' => 'Indietro',
    'tor' => [
        'title' => 'Servizio nascosto TOR <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-torhidden" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '2' => 'È possibile accedere a MetaGer nel browser Tor all\'indirizzo: http://metagerv65pwclop2rsfzg4jwowpavpwd6grhhlvdgsswvo6ii4akgyd.onion .',
        '1' => 'Da molti anni MetaGer nasconde e non memorizza gli indirizzi IP. Tuttavia, questi indirizzi sono temporaneamente visibili sul server MetaGer durante l\'esecuzione di una ricerca: se MetaGer venisse compromesso, un malintenzionato potrebbe leggere e memorizzare i vostri indirizzi. Per soddisfare i massimi requisiti di sicurezza, gestiamo un\'istanza di MetaGer sulla rete Tor: il MetaGer TOR Hidden Service, accessibile tramite: <a href="/tor/" target="_blank" rel="noopener">https://metager.de/tor/</a>. Per utilizzarlo, è necessario un browser speciale, che si può scaricare da <a href="https://www.torproject.org/" target="_blank" rel="noopener">https://www.torproject.org/</a>.',
    ],
    'proxy' => [
        'title' => 'Anonimizzazione del server proxy MetaGer <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-proxy" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Per utilizzarlo, è sufficiente fare clic su "APRIRE ANONIMAMENTE" in fondo ai risultati della pagina dei risultati di MetaGer. La vostra richiesta verrà quindi indirizzata al sito web di destinazione attraverso il nostro server proxy anonimizzante e i vostri dati personali rimarranno completamente protetti. Importante: se da questo momento in poi seguite i link delle pagine, rimarrete protetti dal proxy. Tuttavia, non è possibile inserire un nuovo indirizzo nel campo dell\'indirizzo in alto. In questo caso, si perde la protezione. È possibile verificare se si è ancora protetti nel campo dell\'indirizzo, che visualizzerà: https://proxy.suma-ev.de/?url=here è l\'indirizzo attuale.',
    ],
    'content' => [
        'title' => 'Contenuti discutibili / Protezione dei giovani <a title="For easy help, click here" href="/help/easy-language/privacy-protection#eh-content" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'explanation' => [
            '1' => 'Ho ricevuto "visite" che non solo sono fastidiose, ma che contengono anche, a mio avviso, contenuti illegali!',
            '2' => 'Se trovate su Internet qualcosa che ritenete illegale o dannoso per i minori, potete contattare <a href="mailto:hotline@jugendschutz.net" target="_blank" rel="noopener">hotline@jugendschutz.net</a> via e-mail o visitare <a href="http://www.jugendschutz.net/" target="_blank" rel="noopener">www.jugendschutz.net</a> e compilare il modulo di reclamo disponibile. È utile fornire una breve nota su ciò che si ritiene inammissibile e su come ci si è imbattuti in questo contenuto. Potete anche segnalarci direttamente i contenuti discutibili. Per farlo, inviate un\'e-mail al nostro responsabile della tutela dei giovani (<a href="mailto:jugendschutz@metager.de" target="_blank" rel="noopener">jugendschutz@metager.de</a>).',
        ],
    ],
    'maps' => [
        'title' => 'Mappe MetaGer',
        '1' => 'La tutela della privacy nell\'era dei giganti globali dei dati ci ha portato anche a sviluppare <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a>: l\'unico (a nostra conoscenza) pianificatore di itinerari che offre tutte le funzionalità tramite browser e app senza memorizzare la posizione dell\'utente. Tutto questo è verificabile perché il nostro software è open source. Per l\'utilizzo di maps.metager.de, consigliamo la nostra versione veloce dell\'app. È possibile scaricare le nostre app da <a href="/app" target="_blank">qui</a> (o naturalmente anche dal Play Store).',
        '2' => 'È possibile accedere alla funzione mappe anche dalla ricerca di MetaGer (e viceversa). Dopo aver cercato un termine in MetaGer, nell\'angolo in alto a destra apparirà un nuovo focus di ricerca "Mappe". Facendo clic su di esso si accede alla mappa corrispondente.',
        '3' => 'Al momento del caricamento, la mappa visualizza i punti (POI = Point of Interest) trovati da MetaGer, che sono elencati anche nella colonna di destra. Quando si esegue lo zoom, l\'elenco si adatta alla sezione della mappa. Passando il mouse su un indicatore della mappa o dell\'elenco si evidenzia l\'elemento corrispondente. Fare clic su "Dettagli" per ottenere ulteriori informazioni su quel punto dal database sottostante.',
    ],
    'easy-help' => 'Facendo clic sul simbolo <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , si accede a una versione semplificata della guida.',
    'privacy' => [
        'title' => "Anonimato e sicurezza dei dati",
        '1' => 'Cookie di monitoraggio, ID di sessione e indirizzi IP <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-tracking" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '2' => 'Nessuno di questi dati viene utilizzato, memorizzato, conservato o altrimenti elaborato qui a MetaGer (eccezione: memorizzazione a breve termine per la protezione contro gli attacchi di hacking e bot). Poiché consideriamo questo argomento estremamente importante, abbiamo anche creato dei modi per aiutarvi a raggiungere il massimo livello di sicurezza: il servizio MetaGer TOR Hidden e il nostro server proxy anonimizzante.',
        '3' => "Ulteriori informazioni sono disponibili di seguito. Le funzioni sono accessibili sotto \"Servizi\" nella barra di navigazione.",
    ],
];
