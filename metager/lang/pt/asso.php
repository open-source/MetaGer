<?php
return [
    '1' => [
        '1' => 'Quando os seus termos de pesquisa não lhe dão os resultados desejados, será que não são os corretos? Pergunte ao nosso associador: Introduza um ou mais termos e ser-lhe-á apresentada uma lista completa de termos relacionados. Pode procurar estas novas palavras clicando nelas. O nosso ',
        '2' => 'política de privacidade',
    ],
    'reasso' => [
        'title' => 'Associar este termo',
    ],
    'searchasso' => [
        'title' => 'Procurar este termo com MetaGer Search',
    ],
    'search' => [
        'placeholder' => 'Termo para associar',
    ],
    'head' => [
        '1' => 'Associador MetaGer',
    ],
];
