<?php
return [
    'Icon-settings' => 'Cog',
    'ellipsis' => [
        'alt' => 'reticências verticais',
    ],
    'cogs' => [
        'alt' => 'Cogs',
    ],
    'x' => [
        'alt' => 'x',
    ],
    'home' => [
        'alt' => 'casa',
    ],
    'floppy' => [
        'alt' => 'disquete',
    ],
    'menu' => [
        'alt' => 'três traços horizontais',
    ],
    'ellipsis-horizontal' => [
        'alt' => 'elipse horizontal',
    ],
    'trashcan' => [
        'alt' => 'caixote do lixo',
    ],
    'angle-double-right' => [
        'alt' => 'Ângulo duplo direito',
    ],
    'icon-lupe' => [
        'alt' => 'Pesquisar',
    ],
];
