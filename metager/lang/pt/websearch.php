<?php
return [
    'head' => [
        '7' => 'Código',
        '5' => 'Pesquisar com MetaGer...',
        '8' => 'Pesquisar',
        '2' => 'Aqui encontra um Metager-Widget para o seu sítio Web.',
        '3' => 'Pré-visualização',
        'copy' => 'Cópia',
        '1' => 'MetaGer Websearch-Widget',
        '4' => 'Pesquisar e encontrar em segurança com o MetaGer',
        '6' => 'en',
    ],
    'alert' => [
        'failure' => 'Alguma coisa correu mal!',
        'success' => 'Copiado!',
    ],
];
