<?php
return [
    'title' => 'A sua adesão à SUMA-EV',
    'non-de' => 'Infelizmente, atualmente só podemos aceitar pedidos de admissão para países de língua alemã. É muito bem-vindo se nos apoiar com um donativo <a href=":donationlink"></a> .',
    'back' => 'Voltar à página inicial',
];
