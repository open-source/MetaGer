<?php
return [
    'title' => 'Oops. Isso não deveria ter acontecido.',
    'text' => 'Algo correu mal com o seu pedido. Estamos a tratar do assunto o mais rapidamente possível.',
];
