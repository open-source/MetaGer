<?php
return [
    'failed' => 'Estas credenciais não correspondem aos nossos registos.',
    'throttle' => 'Demasiadas tentativas de início de sessão. Por favor, tente novamente em :segundos segundos.',
];
