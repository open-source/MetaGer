<?php
return [
    'tableofcontents' => [
        '3' => [
            '3' => 'Serviço Tor Hidden',
            '4' => 'Servidor proxy MetaGer',
            '0' => 'Privacidade',
            '2' => 'Rastreio e cookies',
            '5' => 'Proteção dos jovens',
        ],
        '4' => [
            '3' => 'MetaGer-Web-Associator',
            '5' => 'Mapas MetaGer',
            '0' => 'Serviços',
            '1' => 'Android-App',
            '4' => 'MetaGer Widget',
        ],
        '1' => [
            '1' => 'A página inicial',
            '0' => 'Utilização do motor de busca',
            '2' => 'O campo de pesquisa',
            '3' => 'A página de resultados',
            '4' => 'Definições',
        ],
        '2' => [
            '3' => 'Pesquisar na pesquisa',
            '2' => '!bangs',
            '4' => 'Adicionar MetaGer',
            '0' => 'Funções e dicas úteis',
            '1' => 'Funções de pesquisa',
        ],
    ],
    'achtung' => 'Aviso! A estrutura e a funcionalidade do nosso sítio Web estão sujeitas a constantes desenvolvimentos e alterações. Tentamos atualizar as nossas páginas de ajuda o mais rapidamente possível, mas não podemos evitar erros temporários.',
    'easy.language' => "para uma ajuda fácil",
    'title' => 'MetaGer - Ajuda',
];
