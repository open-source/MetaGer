<?php
return [
    'resultpage' => [
        'title' => 'A página de resultados <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-resultpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'settings' => 'Definições: Aqui, pode fazer definições de pesquisa permanentes para a sua pesquisa MetaGer no foco atual. Também pode selecionar e desmarcar os motores de busca associados ao foco. As suas definições são guardadas através de um cookie de texto simples não identificável pessoalmente. Também pode aceder à página de definições através do menu no canto superior direito.',
        'foci' => 'No campo de pesquisa, existem 3 focos de pesquisa diferentes (Web, Imagens, Notícias), cada um dos quais está associado a motores de pesquisa específicos.',
        'choice' => 'Em baixo, verá dois itens: "Filtro" e "Definições", se aplicável.',
        'filter' => 'Filtro: Aqui, pode mostrar e ocultar opções de filtro e aplicar filtros. Em cada foco de pesquisa, existem diferentes opções de seleção. Algumas funções só estão disponíveis quando se utiliza uma tecla MetaGer.',
    ],
    'searchfield' => [
        'search' => 'a lupa: Comece a sua pesquisa clicando aqui ou premindo "Enter".',
        'memberkey' => 'o símbolo da chave: Aqui, pode introduzir a sua chave para utilizar a pesquisa sem anúncios. Também pode ver o seu saldo de fichas e gerir a sua chave.',
        'slot' => 'o campo de pesquisa: Introduza aqui o seu termo de pesquisa. As letras maiúsculas e minúsculas não são distinguidas.',
        'morefunctions' => 'Funções adicionais podem ser encontradas no item de menu "<a href = "/hilfe/funktionen">Funções de pesquisa</a>"',
        'title' => 'O campo de pesquisa <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#eh-searchfield" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'O campo de pesquisa é composto por várias partes:',
    ],
    'settings' => [
        '5' => 'Alternar o modo escuro <br> Mudar para o modo escuro facilmente aqui.',
        '6' => 'Abrir resultados num novo separador <br> Aqui, pode ativar permanentemente a função para abrir resultados num novo separador.',
        '8' => 'Restaurar todas as definições actuais <br> Será apresentada uma ligação que pode definir como página inicial ou favorito para manter as definições atualmente definidas.',
        '3' => 'Filtros de pesquisa <br> Os filtros de pesquisa permitem-lhe filtrar permanentemente a sua pesquisa.',
        '7' => 'Citações <br> Pode ativar e desativar a apresentação de citações aqui.',
        '4' => 'Lista negra <br> Aqui, pode criar uma lista negra pessoal. Pode utilizá-la para filtrar domínios específicos e criar as suas próprias definições de pesquisa. Ao clicar em "Adicionar", estas definições serão anexadas à ligação na secção "Nota".',
        '2' => 'Motores de pesquisa usados <br> Aqui, pode ver e ajustar os motores de pesquisa que está a utilizar. Ao clicar no nome correspondente, pode activá-lo ou desactivá-lo em conformidade.',
        '9' => 'Publicidade subtil ao nosso próprio serviço <br> Mostramos-lhe publicidade subtil aos nossos próprios serviços. Pode desativar a nossa auto-promoção aqui.',
        'title' => 'Definições <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-settings" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Pesquisa sem anúncios <br> Aqui, pode ver o saldo da sua chave e a sua chave. Tem também a opção de carregar ou remover a sua chave.',
    ],
    'startpage' => [
        'info' => 'A página inicial inclui o campo de pesquisa, um botão no canto superior direito para aceder ao menu e duas ligações abaixo do campo de pesquisa para adicionar o MetaGer ao seu browser e pesquisar sem anúncios. Na secção inferior, encontrará informações sobre o MetaGer e a associação SUMA-EV. Para além disso, as nossas áreas de foco <i>Privacidade Garantida, Associação Sem Fins Lucrativos, Diversidade e Liberdade</i> e <i>Energia 100% Verde</i> são apresentadas na parte inferior. Ao clicar nas respectivas secções ou ao percorrer o ecrã, pode encontrar mais informações. ',
        'title' => 'A página inicial <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-startpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    'title' => [
        '2' => 'Utilizar as páginas principais',
        '1' => 'MetaGer - Ajuda',
    ],
    'result' => [
        'info' => [
            'open' => '"ABRIR": Clique no título ou na ligação abaixo (o URL) para abrir o resultado no mesmo separador.',
            'hideresult' => '"esconder": Isto permite-lhe ocultar resultados deste domínio. Também pode escrever esta opção diretamente após o termo de pesquisa e concatená-la; também é permitido um "*" curinga. Ver também Definições para uma solução permanente.',
            'more' => '<img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>: Quando clica em <img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>, obtém novas opções; o aspeto do resultado muda:',
            'newtab' => '"ABRIR NUM NOVO FICHA" abre o resultado num novo separador. Em alternativa, também pode abrir um novo separador utilizando CTRL e o botão esquerdo do rato ou o botão do meio do rato.',
            '1' => 'Todos os resultados são apresentados no seguinte formato:',
            '2' => 'As novas opções são:',
            'anonym' => '"OPEN ANONYMOUSLY" significa que o resultado é aberto sob a proteção do nosso proxy. Algumas informações sobre este assunto podem ser encontradas na secção <a href = "/hilfe/datensicherheit#h-proxy">Anonymizing MetaGer Proxy Server</a>.',
            'domainnewsearch' => '"Iniciar uma nova pesquisa neste domínio": É efectuada uma pesquisa mais detalhada no domínio do resultado.',
        ],
        'title' => 'Resultados <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-results" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
    ],
    'easy-help' => 'Ao clicar no símbolo <a title="For easy help, click here" href="/hilfe/easy-language/mainpages" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , acederá a uma versão simplificada da ajuda.',
    'backarrow' => 'Voltar',
];
