<?php
return [
    'maps' => [
        '2' => "Esta função de mapa também pode ser acedida a partir da pesquisa no MetaGer (e vice-versa). Depois de ter pesquisado um termo no MetaGer, verá um novo foco de pesquisa \"Mapas\" no canto superior direito. Clicando nele, acederá a um mapa correspondente.",
        'title' => 'Mapas MetaGer <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-maps" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Preservar a privacidade na era dos gigantes globais de dados também nos levou a desenvolver <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a>: o (até onde sabemos) único planeador de rotas que oferece todas as funcionalidades através de um navegador e de uma aplicação sem armazenar as localizações dos utilizadores. Tudo isto é verificável porque o nosso software é de código aberto. Para utilizar o maps.metager.de, recomendamos a nossa versão rápida da aplicação. Pode descarregar as nossas aplicações a partir de <a href="/app" target="_blank">aqui</a> (ou, claro, também a partir da Play Store).',
        '3' => "Aquando do carregamento, o mapa apresenta os pontos (POIs = Pontos de Interesse) encontrados pelo MetaGer, que também são listados na coluna da direita. Ao fazer zoom, esta lista adapta-se à secção do mapa. Ao passar o rato sobre um marcador no mapa ou na lista, o item correspondente é realçado. Clique em 'Detalhes' para obter mais informações sobre esse ponto a partir da base de dados abaixo.",
    ],
    'app' => [
        'title' => 'Aplicação para Android <a title="For easy help, click here" href="/hilfe/easy-language/services#help-app" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => "Também pode utilizar o MetaGer como uma aplicação. Basta descarregar a aplicação <a href=\"https://metager.de/app\" target=\"_blank\" rel=\"noopener\">MetaGer</a> no seu smartphone Android.",
    ],
    'widget' => [
        'title' => 'MetaGer Widget <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-widget" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => "Este é um gerador de código que lhe permite incorporar o MetaGer no seu sítio Web. Pode utilizá-lo para efetuar pesquisas no seu próprio site ou na Internet, conforme desejar. Para quaisquer questões, utilize <a href=\"/kontakt/\" target=\"_blank\" rel=\"noopener\">o nosso formulário de contacto</a>.",
    ],
    'services' => [
        'text' => "Serviços adicionais em torno da pesquisa",
    ],
    'easy-help' => 'Ao clicar no símbolo <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> , acederá a uma versão simplificada da ajuda.',
    'backarrow' => 'Voltar',
    'title' => 'MetaGer - Ajuda',
];
