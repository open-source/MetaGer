<?php
return [
    'default-search-v60' => [
        '4' => 'Agora pode escolher o MetaGer como motor de pesquisa tocando no ícone no canto superior esquerdo de um novo separador.',
        '1' => 'Toque e mantenha premida a barra de pesquisa no final da instrução.',
        '2' => 'Escolha "Adicionar motor de busca...".',
        '3' => 'Toque em "OK" na janela de contexto.',
    ],
];
