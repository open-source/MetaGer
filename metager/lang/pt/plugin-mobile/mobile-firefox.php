<?php
return [
    'default-search-vlt80' => [
        '1' => 'Toque e mantenha premida a barra de pesquisa no final das instruções.',
        '2' => 'Selecione "Adicionar motor de busca" no menu de contexto',
        '3' => 'Agora pode ir às definições do seu navegador e escolher o MetaGer como motor de busca predefinido na secção "Pesquisar".',
    ],
    'search-string' => 'Cadeia de pesquisa:',
    'default-search-v80' => [
        '3' => 'Em "Geral", selecione "Procurar".',
        '2' => 'Selecionar Definições.',
        '7' => 'Por fim, selecione a entrada MetaGer da lista.',
        '4' => 'Toque em "+ Adicionar motor de busca".',
        '5' => 'Escolha "Outro", introduza "MetaGer" como nome e a cadeia de pesquisa a utilizar.',
        '6' => 'Toque em ""<img class= "mg-icon" src="/img/icon-check.svg">" no canto superior direito para guardar.',
        '1' => 'Toque em "<img class= "mg-icon" src="/img/ellipsis.svg">" no canto inferior direito.',
    ],
];
