<?php
return [
    'default-search-v45' => [
        '1' => 'Toque em "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">" na parte inferior central para abrir o menu.',
        '2' => 'Selecione "Definições" no lado direito.',
        '3' => 'Desloque-se para baixo e selecione "Procurar" na categoria "Avançadas".',
        '4' => 'Escolha "Motor de pesquisa predefinido" e selecione MetaGer na lista "Visitados recentemente".',
        '5' => 'Se o MetaGer não aparecer na lista, utilize a barra de pesquisa no final destas instruções e tente novamente.',
    ],
    'default-page-v45' => [
        '1' => 'Toque em <a href="/" target="_blank" rel="noopener">aqui</a> para abrir o MetaGer num novo separador.',
        '2' => 'Toque em "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">" na parte inferior central para abrir o menu e selecione "Definições" no lado direito.',
        '3' => 'Escolha "Página inicial" na categoria "Básico".',
        '4' => 'Escolha "Uma página específica", toque em "Utilizar página atual" e "guardar".',
    ],
];
