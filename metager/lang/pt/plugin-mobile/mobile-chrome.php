<?php
return [
    'default-search-v83' => [
        '1' => 'Toque em "<img class= "mg-icon" src="/img/ellipsis.svg">" no canto superior direito.',
        '2' => 'Selecionar "Definições".',
        '3' => 'Em "Básicos", selecione "Motor de busca".',
        '4' => 'Em "Visitados recentemente", selecione "MetaGer".',
        '5' => 'Se o MetaGer não aparecer na lista, utilize a barra de pesquisa no final destas instruções e tente novamente.',
    ],
    'default-page-v83' => [
        '1' => 'Toque em "<img class= "mg-icon" src="/img/ellipsis.svg">" no canto superior direito.',
        '2' => 'Selecionar "Definições".',
        '3' => 'Em "Avançadas", selecione "Página inicial".',
        '4' => 'Escolha "Introduzir endereço Web personalizado" e introduza ":link" .',
    ],
];
