<?php
return [
    'search-string' => 'Cadeia de pesquisa:',
    'default-search-v8-8' => 'Tenha cuidado para não adicionar acidentalmente um espaço no final da cadeia de pesquisa. Caso contrário, o Firefox Focus não lhe permitirá guardar.',
];
