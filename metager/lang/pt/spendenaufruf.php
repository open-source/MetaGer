<?php
return [
    'heading' => 'Juntos consigo',
    'text' => 'desenvolvemos e mantemos o MetaGer. Com a sua ajuda, podemos salvar a privacidade e a proteção de dados:',
    'button' => 'Pedido de donativos',
    'link' => '/pt/spende',
];
