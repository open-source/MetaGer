<?php
return [
    'title' => 'Erro 404, Página não encontrada',
    'text' => 'Talvez tenha introduzido uma ligação errada ou antiga.',
];
