<?php
return [
    'head' => [
        '1' => 'Sobre os nossos motores de pesquisa',
        '2' => 'O MetaGer utiliza vários índices de pesquisa',
        '3' => 'Como integrar o seu motor de busca com o MetaGer',
    ],
    'text' => [
        '2' => [
            '7' => 'Versão MetaGer utilizada: ',
            '6' => 'Páginas estimadas no índice: ',
            '3' => 'Fundada: ',
            '5' => 'Mantenedor: ',
            '4' => 'Sede social: ',
            '1' => 'Nome: ',
            '2' => 'Índice usado: ',
        ],
        '1' => [
            '1' => 'O MetaGer é um motor de meta-pesquisa <a href=":transparenz"></a> . Por isso, utilizamos os índices de vários outros motores de busca. Para cada um destes motores de busca, criámos um pequeno perfil com as informações mais importantes. Uma vez que não actualizamos constantemente este resumo, todas as informações não são garantidas.',
            '3' => 'Infelizmente, só podemos integrar pesquisas gerais na Web ou pesquisas na área dos nossos focos. Assim, se oferecer pesquisas na Web, de imagens, de notícias ou de produtos, a integração é possível. Para mais informações, não hesite em contactar-nos através de <a href=":contact"></a> .',
            '2' => 'O seu motor de pesquisa pode ser incluído no MetaGer. Para que isso seja possível, temos os seguintes requisitos:',
        ],
    ],
];
