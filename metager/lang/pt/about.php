<?php
return [
    'timeline' => [
        '15' => [
            '2' => 'As dicas rápidas, que oferecem sugestões para além dos resultados de pesquisa normais, foram expandidas em termos de funcionalidade e transformadas num projeto separado. Em vez de estarem acima dos resultados, estão agora localizadas numa barra lateral.',
            '1' => 'setembro de 2017: Revisão das dicas rápidas',
        ],
        '4' => [
            '2' => 'O MetaGer é transferido para a associação sem fins lucrativos <i>SUMA-EV - Associação para o Acesso Livre ao Conhecimento</i>. A cooperação com a Universidade de Hannover será mantida.',
            '1' => '01.10.2012: Mudança de patrocínio para a SUMA-EV',
        ],
        '3' => [
            '1' => '1997-2006: Página inicial do MetaGer',
            '2' => 'Este é o aspeto do MetaGer quando estiver online pela primeira vez. O MetaGer oferece assim um serviço novo e inovador.',
        ],
        '8' => [
            '1' => 'março de 2014: Implementação da função "Abrir anonimamente',
            '2' => 'Um serviço de proxy é oferecido com a nova função "Abrir anonimamente". Desta forma, os utilizadores do MetaGer permanecem protegidos mesmo após a pesquisa',
        ],
        '20' => [
            '1' => '11.01.2024: Ajuda fácil e glossário',
            '2' => 'O MetaGer implementou uma versão mais simples da página de ajuda. Esta apresenta uma explicação mais pormenorizada sobre como utilizar o motor de busca. Para além disso, existe agora um glossário que explica certas palavras que podem ser difíceis de compreender. ',
        ],
        '18' => [
            '2' => 'A página de ajuda do MetaGer tem uma nova visão geral. Esta estrutura e visualiza as páginas de ajuda com ícones fáceis de reconhecer.',
            '1' => '14.02.2022: Renovação da página de ajuda',
        ],
        '5' => [
            '1' => '2006-2015: Introdução de novos modelos',
            '2' => 'A página inicial original do MetaGer passa por muitas pequenas alterações nos anos seguintes.',
        ],
        '13' => [
            '2' => 'Maps.MetaGer.de é lançado como um serviço de mapas que respeita a privacidade. Os mapas baseiam-se nos dados do projeto OpenStreetMap, mas têm a sua própria visualização e um planeador de rotas.',
            '1' => 'dezembro de 2016: Lançamento do MetaGer Maps',
        ],
        '6' => [
            '1' => '29.08.2013 Lançada a versão inglesa',
            '2' => 'Pela primeira vez, a interface do MetaGer também está disponível em inglês.',
        ],
        '7' => [
            '1' => 'dezembro de 2013: Serviço Tor',
            '2' => 'O MetaGer oferece acesso à rede de anonimização Tor como uma caraterística relacionada com a segurança.',
        ],
        '14' => [
            '1' => '2017: Relançamento da versão inglesa do MetaGer',
            '2' => 'O extenso ajuste fino e as fontes de resultados personalizadas melhoram muito a versão inglesa do MetaGer.',
        ],
        '22' => [
            '1' => 'dezembro de 2017: Nova pesquisa sem anúncios para os membros da SUMA-EV',
            '2' => 'A MetaGer apresentou a sua nova pesquisa sem anúncios. Os membros da SUMA-EV podem agora efetuar pesquisas sem anúncios, utilizando um sistema baseado em chaves.',
        ],
        '1' => [
            '2' => 'A ideia do MetaGer surgiu ao engenheiro alemão Dr. Wolfgang Sander-Beuermann durante a feira de computadores CeBIT, enquanto almoçava. Começou imediatamente a desenhá-la num guardanapo de papel. O trabalho no primeiro protótipo começou nessa mesma noite.',
            '1' => 'março de 1996: Cebit Hanover',
        ],
        '2' => [
            '2' => 'O MetaGer está a ser desenvolvido como um projeto de investigação da Universidade de Hannover e do centro regional de computação da Baixa Saxónia.',
            '1' => 'Final de 1996: Lançamento do MetaGer',
        ],
        '21' => [
            '2' => 'Ainda hoje, o MetaGer está em constante evolução.',
            '1' => 'Hoje',
        ],
        '12' => [
            '2' => 'O MetaGer é publicado como código aberto sob a licença livre GNU AGPL.',
            '1' => '16.08.2016: Lançamento do código fonte',
        ],
        '11' => [
            '1' => '2016-2019: Página inicial do MetaGer',
            '2' => 'Outros pequenos ajustes solidificam gradualmente o laranja como a cor do MetaGer',
        ],
        '9' => [
            '1' => '2015-2016: Página inicial do MetaGer',
            '2' => 'Como parte de modernizações mais extensas, é oferecida uma nova página inicial simplificada.',
        ],
        '10' => [
            '2' => 'A modernização extensiva no núcleo do MetaGer reduz o tempo médio de resposta do motor de busca para menos de 1,3 segundos. Nos próximos anos, este tempo diminuirá ainda mais, para quase metade.',
            '1' => 'março de 2016: Resultados da pesquisa em menos de 1,3 segundos',
        ],
        '19' => [
            '2' => 'O MetaGer lança a pesquisa sem anúncios para todos, utilizando um sistema baseado em chaves. Inclui uma maior seleção de motores de pesquisa, uma pesquisa de imagens melhorada e a opção de pesquisar sem rastreio comprovado utilizando o novo sistema de token anónimo.',
            '1' => '26.04.2023: A nova pesquisa sem anúncios',
        ],
        '16' => [
            '1' => '2016-2019: Página inicial do MetaGer',
            '2' => 'Na nova página inicial, quatro caraterísticas do MetaGer são destacadas e promovidas pela primeira vez.',
        ],
        '17' => [
            '2' => 'Como parte de uma extensa remodelação da página inicial, as quatro propostas de venda exclusivas da MetaGer são revisitadas e apresentadas num formato alargado. Para não sobrecarregar a função central de pesquisa na Web, as descrições podem ser acedidas através da deslocação. <br> Outra caraterística da nova conceção é o modo escuro em toda a página. Isto permite que o MetaGer seja utilizado em todas as páginas com um design escuro.</br> ',
            '1' => '26.10.2020: Página inicial atual e modo escuro',
        ],
    ],
    'text' => [
        '8' => 'E muito mais...',
        '4' => 'Função da pesquisa na pesquisa',
        '3' => 'Possibilidade de criar uma lista negra pessoal',
        '7' => 'O único motor de pesquisa alemão que combina resultados de vários grandes índices Web',
        '2' => 'O MetaGer é diferente dos outros motores de busca. Isto reflecte-se não só na nossa orientação para o bem público e na nossa preocupação com a privacidade, mas também através de algumas caraterísticas únicas:',
        '5' => 'Possibilidade de pesquisa sem publicidade',
        '6' => 'Integração de projectos de motores de busca como o YaCy',
    ],
    'head' => [
        '1' => 'Sobre nós',
        '4' => 'O que torna o MetaGer especial?',
        '2' => 'Linha do tempo',
        '3' => 'O que defendemos',
    ],
    'points' => [
        'renewable' => [
            'heading' => '100% de energia renovável',
            'text' => 'A sustentabilidade e o consumo de recursos são também uma questão importante para nós. Por isso, prestamos atenção ao consumo de energia dos nossos serviços e utilizamos apenas eletricidade proveniente de fontes de energia renováveis.',
        ],
        'diverse' => [
            'text' => 'O MetaGer produz resultados diversos porque é um motor de meta-pesquisa. Explicámos exatamente o que isto significa na nossa declaração de transparência <a href=":transparenz"></a> . Ao publicar o nosso código fonte, mostramos que o livre acesso ao conhecimento é importante para nós. O nosso código fonte é gratuito e de código aberto.',
            'heading' => 'Diversos e livres',
        ],
        'association' => [
            'heading' => 'Associação sem fins lucrativos',
            'text' => 'O MetaGer é um projeto da associação sem fins lucrativos SUMA-EV, Associação para o Livre Acesso ao Conhecimento. A SUMA-EV tem como principal objetivo a promoção da literacia mediática.',
        ],
        'privacy' => [
            'text' => 'A proteção de dados e a privacidade são importantes para nós. É por isso que não rastreamos nem armazenamos dados pessoais e oferecemos vários serviços para proteger a sua privacidade, por exemplo, o nosso proxy anónimo (funcionalidade "Abrir anonimamente").',
            'heading' => 'Privacidade garantida',
        ],
    ],
];
