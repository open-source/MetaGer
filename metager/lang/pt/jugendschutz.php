<?php
return [
    'title' => 'Proteção dos jovens',
    'text' => 'Fornecemos resultados de pesquisa para âmbitos muito diferentes da Internet. Alguns deles não são adequados para pessoas da sua idade. Se quiser encontrar algo, tente utilizar outros motores de busca - <a href="https://www.blinde-kuh.de/">Blinde Kuh</a>, por exemplo.',
];
