<?php
return [
    '1' => 'Desculpe o incómodo',
    '2' => 'Introduza os cinco caracteres da imagem ou do leitor áudio no campo de introdução e confirme com "OK" para aceder à página de resultados.',
    '3' => 'Introduzir captcha',
    '4' => 'Não voltar a perguntar neste dispositivo (opcional; define um cookie)',
    '5' => 'Gerar um novo captcha',
];
