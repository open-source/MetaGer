<?php
return [
    'plugin' => [
        'btn-app' => 'MetaGer App',
        'btn-add' => 'Adicionar o plugin MetaGer',
        'title' => 'Instalar o MetaGer agora',
        'p' => 'Utilize o nosso plugin para instalar o MetaGer como o seu motor de busca. Pode utilizar confortavelmente a aplicação MetaGer no seu smartphone.',
        'image' => [
            'alt' => 'MetaGer Apps',
        ],
    ],
    'eco' => [
        'title' => '100% de energia renovável',
        'p' => 'A sustentabilidade e a utilização eficiente dos recursos são importantes para nós. Todos os nossos serviços são geridos com recurso a energias renováveis. Desde o servidor até à máquina de café.',
        'image' => [
            'alt' => 'folha verde',
        ],
    ],
    'privacy' => [
        'title' => 'Privacidade garantida',
        'p' => 'Ao utilizar a nossa pesquisa sem anúncios, mantém o controlo total sobre os seus dados. O nosso proxy anónimo mantém-no protegido mesmo quando continua a navegar. Nós não rastreamos.',
        'image' => [
            'alt' => 'fechadura',
        ],
    ],
    'btn-more' => 'Mais',
    'diversity' => [
        'title' => 'Diversos e livres',
        'p' => 'O MetaGer protege contra a censura, combinando os resultados de vários motores de busca. Os nossos algoritmos são transparentes e estão disponíveis para serem lidos por todos. O nosso código fonte é gratuito.',
        'image' => [
            'alt' => 'Arco-íris',
        ],
    ],
    'ngo' => [
        'image' => [
            'alt' => 'Coração',
        ],
        'p' => 'O MetaGer é desenvolvido e gerido pela nossa organização sem fins lucrativos, a SUMA-EV - Associação para o Acesso Livre ao Conhecimento. Fortaleça-nos tornando-se membro ou fazendo um donativo à SUMA-EV. Os membros pesquisam sem anúncios!',
        'title' => 'Gerido por uma organização sem fins lucrativos',
    ],
    'btn-mg-code' => 'Código fonte do Metager',
    'btn-member' => 'Formulário de Membro',
    'btn-data-protection' => 'Política de privacidade',
    'btn-SUMA-EV' => 'SUMA-EV',
    'btn-mg-algorithm' => 'O nosso algoritmo',
    'btn-member-advantage' => 'Filiação',
    'btn-donate' => 'Formulário de donativo',
    'four-reasons' => 'Quatro razões para utilizar o MetaGer',
];
