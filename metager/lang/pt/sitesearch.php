<?php
return [
    'generated' => [
        '6' => 'en',
        '3' => 'Pesquisar com MetaGer...',
        '5' => 'Código',
        '2' => 'Pesquisar e encontrar em segurança com o MetaGer',
        '1' => 'Pré-visualização',
        '4' => 'Pesquisar',
    ],
    'head' => [
        '5' => 'Gerar',
        '1' => 'MetaGer Sitesearch-Widget',
        '2' => 'Aqui encontra um Metager-Widget para o seu sítio Web.',
        '3' => 'Sítio Web para pesquisar',
        '4' => 'Entrar no sítio Web...',
    ],
];
