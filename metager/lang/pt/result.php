<?php
return [
    'adblocker' => 'Um resultado de pesquisa <a href=":resultlink"></a> de <code>:host</code> foi removido aqui pelo seu bloqueador de anúncios. <a href=":infolink">Saiba mais</a>',
    'more-news' => 'mais',
    'options' => [
        '3' => 'ocultar *.:domínio',
        'savetab' => 'Guardar o resultado em TAB',
        'headline' => 'Opções',
        '1' => 'Iniciar uma nova pesquisa neste domínio',
        '7' => 'ABERTO',
        'direct' => 'Abertura direta',
        '4' => 'Parceiro',
        'togglelabel' => 'alternar opções de resultados',
        '8' => 'ELIMINAR A PUBLICIDADE',
        '2' => 'Ocultar :host',
        '6' => 'ABRIR NUM NOVO SEPARADOR',
        '5' => 'ABRIR ANONIMAMENTE',
    ],
    'providers' => 'fornecedores',
    'news' => 'Notícias relevantes',
    'image' => [
        'copyright' => 'As imagens podem estar protegidas por direitos de autor.',
        'download' => 'Descarregar imagem',
    ],
    'videos' => 'Vídeos',
    'advertisement' => 'Anúncio',
    'metagerkeytext' => 'Utilizar o MetaGer sem anúncios',
    'proxytext' => 'A ligação de resultados é aberta de forma anónima. Os seus dados não serão transferidos para os servidores de destino. Algumas páginas web podem não funcionar como habitualmente.',
    'gefVon' => 'por',
    'alt' => [
        'more' => 'Mais',
    ],
];
