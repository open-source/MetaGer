<?php
return [
    'password' => 'As palavras-passe devem ter pelo menos seis caracteres e corresponder à confirmação.',
    'reset' => 'A sua palavra-passe foi reposta!',
    'sent' => 'Enviámos por e-mail a sua ligação de redefinição da palavra-passe!',
    'token' => 'Este token de redefinição de palavra-passe é inválido.',
    'user' => 'Não conseguimos encontrar um utilizador com esse endereço de correio eletrónico.',
];
