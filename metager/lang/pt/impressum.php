<?php
return [
    'title' => 'Aviso do sítio',
    'headline' => [
        '1' => '<a href="http://suma-ev.de/" target="_blank" rel="noopener">SUMA-EV - Verein für freien Wissenszugang (e.V.)</a>',
    ],
    'info' => [
        '1' => 'Wikipedia-entrada para o <a href="http://de.wikipedia.org/wiki/Suma_e.V." target="_blank" rel="noopener">SUMA-EV</a>',
        '2' => "SUMA-EV\r
Postfach 51 01 43\r
D-30631 Hannover\r
Alemanha",
        '3' => "Contacto:\r
Tel: +4951134000070\r
Correio eletrónico: <a href=\"/en-GB/kontakt\">Formulário de contacto encriptado</a>",
        '8' => 'A "SUMA-EV - Verein für freien Wissenszugang" é uma associação de solidariedade social, inscrita no registo de associações do Amtsgericht Hannover sob o número VR200033. Número de identificação fiscal: DE 300 464 091. A "Gottfried Wilhelm Leibniz Universität Hannover" é um organismo estatutário.',
        '4' => 'Direção: Dominik Hebeler, Phil Höfer, Carsten Riel, Manuela Branz',
        '6' => 'Comissária para a Proteção da Juventude: Manuela Branz <a href="mailto:jugendschutz@metager.de">jugendschutz@metager.de</a>',
        '9' => 'Nota de responsabilidade:',
        '10' => 'Apesar do controlo rigoroso dos conteúdos, não assumimos qualquer responsabilidade pelo conteúdo das ligações externas. Apenas os seus operadores são fiáveis pelo conteúdo das ligações externas.',
    ],
];
