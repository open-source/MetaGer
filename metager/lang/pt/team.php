<?php
return [
    'contact' => [
        '1' => 'Por favor, comunique as suas questões/problemas sobre o MetaGer, etc., utilizando o formulário de contacto <a href=":link_contact"></a> .',
        '2' => 'Se receber mensagens de correio eletrónico com conteúdos estranhos da nossa parte, leia mais sobre este assunto: <a href="https://metager.de/wsb/fakemail/">https://metager.de/wsb/fakemail/</a>',
        '3' => 'Apenas em excepções fundamentadas, se quiser contactar alguém diretamente, deve enviar-lhe um e-mail. Porque os membros da equipa podem estar de férias, doentes, etc.',
    ],
    'role' => [
        '7' => 'secretário',
        '1' => [
            '0' => 'executivo',
            '1' => 'executivo',
        ],
        '2' => 'Comissário para a proteção dos jovens',
        '9' => 'estagiário',
        '3' => 'secretário de imprensa',
        '6' => 'programador',
        '8' => 'fundador',
        '5' => 'programador',
        '0' => 'DIRECTOR EXECUTIVO',
    ],
];
