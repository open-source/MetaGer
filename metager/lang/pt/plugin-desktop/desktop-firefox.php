<?php
return [
    'default-search-v61' => [
        '1' => 'Em alternativa, clique em "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">" no lado direito da barra de endereço e selecione "Adicionar motor de pesquisa".',
        '2' => 'Introduza "about:preferences#search" na barra de endereço. Selecione MetaGer no menu pendente em "Motor de pesquisa predefinido".',
    ],
    'default-search-v52' => [
        '3' => 'Introduza "about:preferences#search" na barra de endereço para aceder às definições de pesquisa.',
        '4' => 'Selecione MetaGer no menu pendente em "Motor de pesquisa predefinido".',
        '2' => 'No novo menu, clique em "Adicionar MetaGer ..."',
        '1' => 'Navegue até ao canto superior direito e clique no ícone da lupa junto à barra de endereço.',
    ],
    'default-page-v61' => [
        '2' => 'Selecione "Página inicial" no lado esquerdo.',
        '3' => 'Em "Novas janelas e separadores", clique no menu pendente junto a "Página inicial e novas janelas" e selecione "URLs personalizados...".',
        '4' => 'No novo campo de texto, introduza ":link" .',
        '1' => 'Navegue até ao canto superior direito, clique em "<img class= "mg-icon" src="/img/menu.svg">" e selecione "Opções".',
    ],
    'default-search-v89' => [
        '3' => 'Em "Search" (Pesquisa), clique no menu pendente em "Default Search Engine" (Motor de pesquisa predefinido) e selecione "MetaGer (en)".',
        '1' => 'Em alternativa, clique com o botão direito do rato na barra de endereço na parte superior do seu browser e clique em Adicionar "MetaGer: pesquisa e localização protegidas pela privacidade". O MetaGer está agora instalado como um motor de busca disponível.',
        '2' => 'Clique no canto superior direito do navegador em <img class= "mg-icon" src="/img/menu.svg"> e abra as "Definições".',
    ],
    'default-page-v52' => [
        '1' => 'Navegue até ao canto superior direito, clique em "<img class= "mg-icon" src="/img/menu.svg">" e selecione "Opções".',
        '2' => 'Em "Página inicial", introduza ":link" .',
    ],
    'plugin' => 'Com a instalação da nossa extensão do browser <a href="https://addons.mozilla.org/firefox/addon/metager-suche/" target="_blank" rel="noopener"></a> , o MetaGer é automaticamente instalado como o motor de busca predefinido após a aprovação.',
    'default-search-v57' => [
        '1' => 'Abra um novo separador e introduza "about:preferences#search" para aceder às definições de "Pesquisa".',
        '2' => 'Em "Barra de pesquisa", selecione "Adicionar barra de pesquisa na barra de ferramentas".',
        '4' => 'No novo menu, clique em "Adicionar MetaGer ..."',
        '5' => 'Volte ao separador Definições. Escolha "MetaGer" no menu pendente em "Motor de pesquisa predefinido".',
        '3' => 'Volte ao separador MetaGer e clique na lupa à direita, junto à barra de endereço.',
    ],
];
