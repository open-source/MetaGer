<?php
return [
    'default-search-v3-3' => [
        1 => 'Clique com o botão direito do rato na barra de pesquisa no final desta instrução',
        2 => 'Selecione "Adicionar como motor de pesquisa..." no menu de contexto.',
        3 => 'Selecione a caixa "Definir como pesquisa predefinida" e clique em "Adicionar" na janela de contexto.',
    ],
    'default-page-v3-3' => [
        1 => 'Navegue até ao canto superior esquerdo, clique no logótipo "Vivaldi" e escolha "<img class= "mg-icon" src="/img/icon-settings.svg">" "Definições" em "Ferramentas"',
        2 => 'Em "Página inicial", selecione "Página específica" e introduza :link .',
    ],
];
