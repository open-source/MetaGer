<?php
return [
    'default-page-v10' => [
        '1' => 'Clique em <a href="/" target="_blank" rel="noopener">aqui</a> para abrir o MetaGer num novo separador.',
        '2' => 'No canto superior esquerdo, clique em "Safari" e selecione "Preferências".',
        '3' => 'Em "Geral", clique no botão "Definir para a página atual".',
    ],
];
