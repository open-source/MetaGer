<?php
return [
    'default-search-v49' => [
        '3' => 'Mova o rato sobre a entrada "MetaGer" e clique em "Tornar predefinido".',
        '1' => 'Navegue até ao canto superior direito do seu browser, clique em "<img class= "mg-icon" src="/img/menu.svg">" e selecione "Definições".',
        '2' => 'No botão "Pesquisar", clique no botão "Gerir motores de pesquisa...".',
    ],
    'default-page-v49' => [
        '2' => 'Em "No arranque", escolha "Abrir uma página específica ou um conjunto de páginas" e clique em "Definir páginas".',
        '3' => 'Introduza ":link" como URL e clique em OK.',
        '4' => 'Dica: Todas as páginas listadas nesta janela serão abertas no arranque. Pode remover entradas passando o rato por cima delas e clicando no "x".',
        '1' => 'Navegue até ao canto superior direito do seu browser, clique em "<img class= "mg-icon" src="/img/ellipsis.svg">" e selecione "Definições".',
    ],
    'default-search-v59' => [
        '1' => 'Navegue até ao canto superior direito do seu browser, clique em "<img class= "mg-icon" src="/img/ellipsis.svg">" e selecione "Definições".',
        '2' => 'Em "Motor de busca", clique no botão "Gerir motores de busca".',
        '3' => 'Clique em "<img class= "mg-icon" src="/img/ellipsis.svg">" junto à entrada MetaGer e selecione "Tornar predefinido".',
    ],
    'default-search-v53' => [
        '3' => 'Mova o rato sobre a entrada "MetaGer" e clique em "Tornar predefinido".',
        '1' => 'Navegue até ao canto superior direito do seu browser, clique em "<img class= "mg-icon" src="/img/ellipsis.svg">" e selecione "Definições".',
        '2' => 'Em "Pesquisa", clique no botão "Gerir motores de pesquisa...".',
    ],
    'plugin' => 'Quando instala a nossa extensão do browser <a href="https://chromewebstore.google.com/detail/metager-suche/gjfllojpkdnjaiaokblkmjlebiagbphd" target="_blank" rel="noopener"></a> , o MetaGer é automaticamente instalado como o motor de busca predefinido.',
];
