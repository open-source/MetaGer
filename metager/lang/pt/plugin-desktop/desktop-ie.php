<?php
return [
    'default-search-v11' => [
        '1' => 'Clique em <a href="javascript:window.external.addSearchProvider($(\'link[rel=search]\').attr(\'href\'));">aqui</a>, para adicionar o MetaGer como motor de pesquisa.',
        '2' => 'Navegue até ao canto superior direito e clique em "<img class= "mg-icon" src="/img/icon-settings.svg">".',
        '3' => 'Selecione "Gerir suplementos".',
        '4' => 'À esquerda, selecione "Fornecedores de pesquisa" e clique em "MetaGer" à direita.',
        '5' => 'Clique em "Definir como predefinição" no canto inferior direito da janela',
    ],
    'default-page-v9' => [
        '2' => 'No novo separador, clique na seta junto a <img class= "mg-icon" src="/img/home.svg"> no canto superior esquerdo e escolha "Adicionar ou alterar a página inicial".',
        '3' => 'Na janela pop-up, selecione "Utilizar esta página Web como única página inicial" e clique em "Sim".',
        '1' => 'Clique em <a href="/" target="_blank" rel="noopener">aqui</a> para abrir o MetaGer num novo separador.',
    ],
    'default-search-v9' => [
        '1' => 'Clique em <a href="javascript:window.external.addSearchProvider($(\'link[rel=search]\').attr(\'href\'));">aqui</a> para adicionar o MetaGer como motor de pesquisa.',
        '2' => 'Marque a caixa "Tornar este o meu fornecedor de pesquisa predefinido" e clique em "Adicionar".',
    ],
];
