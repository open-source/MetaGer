<?php
return [
    'default-search-v15' => [
        '3' => 'Desloque-se novamente para baixo e, em "Pesquisar na barra de endereço com", clique em "Alterar motor de pesquisa".',
        '4' => 'Escolha "MetaGer: pesquisa e localização protegidas pela privacidade" e clique em "Definir como predefinição".',
        '1' => 'Navegue até ao canto superior direito do seu browser, clique em "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">" e selecione "Definições".',
        '2' => 'Desloque-se para baixo e clique em "Definições avançadas".',
    ],
    'default-search-v18' => [
        '3' => 'Desloque-se para baixo e, em "Pesquisa na barra de endereço", clique em "Alterar fornecedor de pesquisa".',
        '4' => 'Escolha "MetaGer: pesquisa e localização protegidas pela privacidade" e clique em "Definir como predefinição".',
        '1' => 'Navegue até ao canto superior direito do seu browser, clique em "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">" e selecione "Definições".',
        '2' => 'Clique em "avançado" no lado esquerdo do novo menu',
    ],
    'default-page-v15' => [
        '3' => 'Ao clicar em "<img class= "mg-icon" src="/img/icon-lupe.svg">", adiciona o MetaGer à lista de arranque.',
        '2' => 'Selecione em "Abrir o Microsoft Edge com" "Uma página ou páginas específicas" e introduza ":link".',
        '1' => 'Navegue até ao canto superior direito do seu browser, clique em "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">" e selecione "Definições".',
    ],
    'default-page-v80' => [
        '3' => 'Dica: Todas as páginas listadas nesta janela serão abertas no arranque. Pode remover entradas movendo o rato sobre elas, clicar em "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">" e escolher "Eliminar".',
        '1' => 'Abra um novo separador e introduza "edge://settings/onStartup" na barra de endereço para aceder às definições de arranque',
        '2' => 'Escolha "Abrir uma página ou páginas específicas" e introduza ":link" como URL em "Adicionar uma nova página". Clique em "Adicionar".',
    ],
    'default-search-v80' => [
        '2' => 'Clique em "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">" junto à entrada MetaGer e selecione "Tornar predefinido".',
        '1' => 'Abra um novo separador e introduza "edge://settings/searchEngines" na barra de endereço para aceder às definições do motor de busca.',
    ],
    'plugin' => 'Quando instala a nossa extensão do browser <a href="https://microsoftedge.microsoft.com/addons/detail/fdckbcmhkcoohciclcedgjmchbdeijog" target="_blank" rel="noopener"></a> , o MetaGer é automaticamente instalado como o motor de busca predefinido. É provável que tenha de ativar manualmente a extensão nas definições do browser após a instalação.',
];
