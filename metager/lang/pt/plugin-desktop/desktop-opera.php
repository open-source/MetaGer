<?php
return [
    'default-search-v36' => [
        '1' => 'Clique com o botão direito do rato na barra de pesquisa no final desta instrução',
        '2' => 'Selecione "Criar motor de busca..." no menu de contexto.',
        '3' => 'Clique em "Criar" na janela pop-up.',
    ],
    'default-page-v36' => [
        '1' => 'Navegue até ao canto superior esquerdo e clique no logótipo "Opera" ou "Menu" e clique em "Definições".',
        '2' => 'Em "No arranque", escolha "Abrir uma página específica ou um conjunto de páginas" e clique em "Adicionar uma nova página".',
        '3' => 'Introduza ":link" como URL e clique em "Adicionar".',
        '4' => 'Sugestão: Todas as páginas listadas nesta janela serão abertas no arranque. Pode remover entradas movendo o rato sobre elas e clicando em "<img class= "mg-icon" src="/img/ellipsis-horizontal.svg">".',
    ],
];
