<?php
return [
    'sumaev' => [
        '1' => 'O MetaGer é desenvolvido e gerido por ',
        '2' => 'SUMA-EV - Associação para o Acesso Livre ao Conhecimento.',
        'link' => 'https://suma-ev.de/',
    ],
];
