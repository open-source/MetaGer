<?php
return [
    'meta' => [
        'language' => 'en',
        'Description' => 'Pesquise com segurança e respeite a sua privacidade. O conhecimento digital do mundo deve ser de livre acesso, sem paternalismo e sem a tutela de Estados ou corporações.',
        'Keywords' => 'Pesquisa na Internet, privacidade, motor de busca, proteção de dados, Anonproxy, proxy anónimo, pesquisa anónima, pesquisa de imagens, anónimo, MetaGer, metager, metager.de, metager.net',
    ],
    'opensearch' => 'MetaGer: pesquisa e localização protegidas pela privacidade',
];
