<?php
return [
    'skiplinks' => [
        'settings' => 'Saltar para as definições de pesquisa',
        'navigation' => 'Saltar para a navegação',
        'query' => 'Saltar para o campo de introdução da consulta de pesquisa',
        'return' => 'Pode voltar a este menu em qualquer altura, premindo a tecla de escape',
        'heading' => 'Saltar rapidamente para o conteúdo',
        'results' => 'Saltar para os resultados da pesquisa',
    ],
    'engines' => [
        'queried' => 'Serviços de pesquisa consultados',
        'disabled' => 'Adicionar serviços de pesquisa à consulta',
        'payment_required' => 'Serviços de pesquisa disponíveis com <a href=\':link\'>MetaGer key</a>',
    ],
    'metager3' => 'Está atualmente a utilizar uma versão de teste do MetaGer.',
    'impressum' => 'aviso de sítio',
    'opensearch' => 'MetaGer: pesquisa e localização seguras, proteção da privacidade',
    'startseite' => 'Página inicial do MetaGer',
    'search-placeholder' => 'A sua consulta de pesquisa...',
];
