<?php
return [
    'body' => [
        '1' => 'MetaGer para instalar no seu sítio Web. Para tal, selecione o local onde pretende efetuar a pesquisa:',
        '3' => 'Pesquisar um domínio',
        '4' => 'Atenção: Não deve utilizar este widget se a sua implementação sugerir que o MetaGer é o seu serviço ou que o seu sítio é a verdadeira página inicial do MetaGer (tudo isto já aconteceu). Por conseguinte, é especialmente proibido remover o nosso logótipo.',
        '2' => 'Pesquisar na Web',
    ],
    'head' => 'MetaGer Widget',
];
