<?php
return [
    'searchbar-replacement' => [
        'payment_id_error' => "Introduziu um ID de pagamento que não é uma chave correta. A sua chave tem 36 caracteres.",
        'start' => 'Criar uma nova chave',
        'message' => 'O motor de busca MetaGer está agora disponível apenas sem anúncios!',
        'why' => 'Porquê?',
        'key_error' => "A chave introduzida não é válida. Por favor, verifique a entrada.",
        'login_code_error' => "O código de início de sessão introduzido não era válido. Dica: os códigos de início de sessão só são válidos enquanto estiverem visíveis noutro dispositivo!",
        'login' => "Iniciar sessão com chave",
    ],
    'plugin-title' => 'Adicionar MetaGer ao seu browser',
    'key' => [
        'tooltip' => [
            'low' => 'A ficha esgotou-se rapidamente. Recarregar agora.',
            'nokey' => 'Configurar uma pesquisa sem anúncios',
            'empty' => 'Token usado. Recarregar agora.',
            'full' => 'Pesquisa sem anúncios activada.',
        ],
        'placeholder' => 'Introduzir a chave de membro',
    ],
    'foki' => [
        'produkte' => 'Produtos',
        'bilder' => 'Imagens',
        'web' => 'Web',
        'nachrichten' => 'Notícias',
        'science' => 'Ciência',
        'maps' => 'Mapas',
    ],
    'skip' => [
        'fokus' => 'Saltar para a seleção do foco de pesquisa',
        'navigation' => 'Saltar para a navegação',
        'search' => 'Saltar para a introdução da consulta de pesquisa',
    ],
    'searchreset' => 'eliminar a entrada da consulta de pesquisa',
    'placeholder' => 'MetaGer: Pesquisa e localização protegidas pela privacidade',
    'adfree' => 'MetaGer sem anúncios',
    'searchbutton' => 'Iniciar MetaGer-Search',
    'plugin' => 'Instalar o MetaGer',
    'lang' => 'linguagem wwitch',
];
