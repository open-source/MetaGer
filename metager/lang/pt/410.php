<?php
return [
    'title' => 'Recarregar a página de resultados',
    'text' => 'Utilizou uma ligação desactualizada. Por favor, recarregue a página',
];
