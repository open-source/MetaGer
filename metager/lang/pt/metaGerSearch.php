<?php
return [
    'quicktips' => [
        'wikipedia' => [
            'adress' => 'de <a href="https://en.wikipedia.org" target="_blank" rel="noopener"> Wikipedia, A Enciclopédia Livre</a>',
        ],
        'dictcc' => [
            'adress' => 'de <a href="https://www.dict.cc/" target="_blank" rel="noopener">dict.cc</a>',
        ],
        'tips' => [
            'title' => 'Sabia que?',
        ],
        'bang' => [
            'title' => '!bang redireccionamento',
            'buttonlabel' => 'Continuar para',
            'from' => 'de',
        ],
    ],
];
