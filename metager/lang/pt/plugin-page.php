<?php
return [
    'desktop-unable' => 'O browser que utiliza não oferece a possibilidade de adicionar o MetaGer como motor de pesquisa (predefinido), mas pode descarregar o Firefox <a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank" rel="noopener">aqui</a>, um browser de código aberto que oferece esta funcionalidade.',
    'browser-download' => 'Descarregar o Firefox',
    'head' => [
        'Instalar o Firefox e adicionar o MetaGer',
    ],
    'search-engine' => [
        '2' => 'Com o MetaGer como motor de pesquisa predefinido, pode dar instruções ao seu browser para utilizar automaticamente o MetaGer quando são introduzidas consultas de pesquisa, ou seja, na barra de endereços.',
        '1' => 'O que é um motor de pesquisa predefinido?',
    ],
    'plugin' => 'Definir o MetaGer como motor de pesquisa predefinido com o add-on',
    'default-page' => 'Definir o MetaGer como página inicial',
    'firefox-default-search' => 'Definir o MetaGer como motor de pesquisa predefinido sem add-on',
    'default-search' => 'Definir o MetaGer como motor de pesquisa predefinido',
    'mobile-notlisted' => 'Não sabemos se o browser que utiliza permite adicionar o MetaGer como motor de pesquisa (predefinido), mas pode descarregar o Firefox <a href="https://www.mozilla.org/en-US/firefox/mobile/" target="_blank" rel="noopener">aqui</a>, um browser de código aberto que oferece esta funcionalidade.',
    'desktop-notlisted' => 'Não sabemos se o browser que utiliza permite adicionar o MetaGer como motor de pesquisa (predefinido), mas pode descarregar o Firefox <a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank" rel="noopener">aqui</a>, um browser de código aberto que oferece esta funcionalidade.',
    'mobile-unable' => 'O browser que utiliza não oferece a possibilidade de adicionar o MetaGer como motor de pesquisa (predefinido), mas pode descarregar o Firefox <a href="https://www.mozilla.org/en-US/firefox/mobile/" target="_blank" rel="noopener">aqui</a>, um browser de código aberto que oferece esta funcionalidade.',
    'open-modal' => [
        'title' => 'Adicionar MetaGer ao seu browser',
    ],
];
