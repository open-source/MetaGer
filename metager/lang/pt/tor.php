<?php
return [
    'description' => 'Abaixo encontra a ligação para o serviço oculto do MetaGer. Atenção: Esta ligação só pode ser acedida através da rede Tor. O teu browser produz uma mensagem de erro se não estiver ligado a ela.',
    'torbutton' => 'Abrir o serviço oculto do TOR',
    'torurl' => 'http://metagerv65pwclop2rsfzg4jwowpavpwd6grhhlvdgsswvo6ii4akgyd.onion/en/',
];
