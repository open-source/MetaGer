<?php
return [
    'heading' => 'MetaGer-Partnershop',
    'paragraph' => [
        '1' => 'Sempre que vir resultados com uma etiqueta que diga "Partnershop", trata-se apenas de um resultado normal. Não é classificado de forma diferente ou preferido.',
        '2' => 'Quando o link é clicado, recebemos uma pequena quantia para financiar o MetaGer, e o logótipo da empresa associada é apresentado com o resultado. Também pode sempre reconhecer as nossas lojas parceiras a partir disto. Pode remover estes links, bem como qualquer publicidade, com uma chave <a href=":link">MetaGer</a>.',
        '3' => 'A sua inscrição no SUMA-EV inclui uma chave MetaGer no valor da sua inscrição. <a href="/membership">Tornar-se membro!</a>',
    ],
];
