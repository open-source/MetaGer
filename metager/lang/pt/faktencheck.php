<?php
return [
    'heading' => [
        '1' => 'Verificação de factos contra notícias falsas:',
    ],
    'list' => [
        '4' => [
            '0' => 'Emails',
            '3' => 'Utilizar correio eletrónico assinado e, na melhor das hipóteses, também encriptado. Mais uma vez, isto não é assim tão simples. Pode encontrar mais informações sobre este assunto aqui: <a href="https://www.heise.de/ct/artikel/Ausgebootet-289538.html" target="_blank" rel="noopener">https://www.heise.de/ct/artikel/Ausgebootet-289538.html</a>',
            '2' => 'O correio eletrónico provém realmente do remetente especificado? Para o fazer, verifique o cabeçalho da mensagem de correio eletrónico. Além disso, pode examinar os endereços IP e as rotas de entrega aí mencionadas. No entanto, isto não é fácil.',
            '1' => 'É muito fácil falsificar mensagens de correio eletrónico. Assim, há muitos e-mails falsos; como verificar isso?',
        ],
        '5' => [
            '2' => 'É possível encontrar imagens semelhantes com a pesquisa inversa de imagens dos motores de busca de imagens?',
            '0' => 'Bilder, Vídeos',
            '1' => 'Observe atentamente o fundo. Paisagem, edifícios, carros e matrículas, vestuário, pessoas. É coerente, enquadra-se no texto correspondente?',
            '3' => 'É possível ler os metadados das imagens com programas gráficos? Estes metadados correspondem ao conteúdo da imagem?',
        ],
        '3' => [
            '1' => 'Nem tudo o que está escrito na Wikipédia está correto. Para verificar a exatidão das informações, podes fazer o seguinte:',
            '4' => 'Existe uma página de discussão para esta entrada da Wikipédia a partir da qual se possam obter mais informações?',
            '5' => 'Verificar quais as fontes indicadas na entrada da Wikipédia. Estas podem também servir de fonte de informação adicional.',
            '0' => 'Wikipédia',
            '3' => 'É possível descobrir alguma coisa sobre estes autores?',
            '2' => 'Verificar o histórico de versões: Quem escreveu o quê e quando?',
        ],
        '1' => [
            '2' => '<strong>Autor</strong>: É nomeado um autor? As boas fontes nomeiam frequentemente um autor ou outra fonte de informação. Se um autor for nomeado, é muitas vezes útil encontrar mais informações sobre ele e ver se essa pessoa publica frequentemente sobre este tópico.',
            '0' => 'Sítios Web',
            '1' => '<strong>Datas de publicação</strong>: As boas fontes especificam frequentemente a data de publicação de um artigo. Esta informação pode indicar a qualidade e/ou a atualidade da informação.',
            '4' => '<strong>Impressão</strong>: Se existir uma impressão: O que é que se pode saber sobre as pessoas/empresas aí mencionadas?',
            '3' => '<strong>Outras fontes</strong>: Existem outras fontes citadas? Talvez valha a pena verificá-las também e ver se diminuem a plausibilidade da fonte primária.',
        ],
        '2' => [
            '4' => 'Desde quando é que este perfil existe?',
            '3' => 'No Facebook, existe uma opção adicional para ver se essa pessoa foi verificada.(<a href="https://www.facebook.com/help/196050490547892" target="_blank" rel="noopener">https://www.facebook.com/help/196050490547892</a>)',
            '1' => 'O nome do autor tem um nome que parece real? Se não for esse o caso, a mensagem é provavelmente questionável. Há também a questão de saber se existem informações adicionais sobre o nome.',
            '0' => 'Redes sociais',
            '2' => 'Existem outros canais de comunicação onde esta pessoa possa ser contactada?',
            '5' => 'Quantos subscritores ou amigos tem este perfil? O que é que se pode descobrir sobre os nomes dessas pessoas?',
        ],
        '7' => 'No entanto, não existe uma segurança absoluta contra falsificações ou notícias falsas.',
    ],
    'paragraph' => [
        '1' => 'As notícias falsas são obras de desinformação que são frequentemente apresentadas como meios jornalísticos. As Fake News são frequentemente utilizadas para fins políticos ou comerciais. Então, o que é que se pode fazer para descobrir se algo é Fake News?',
        '2' => 'Existem várias formas de o fazer. Para verificar a qualidade da informação, existe o chamado <a href="https://library.csuchico.edu/sites/default/files/craap-test.pdf">"CRAAP-Test"</a>. Foi desenvolvido na Universidade Estatal da Califórnia em Chico. O teste oferece muitas perguntas úteis para avaliar se a informação é fiável. Para além deste teste, pode encontrar mais informações úteis aqui:',
    ],
];
