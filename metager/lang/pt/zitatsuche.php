<?php
return [
    'title' => 'MetaGer - Pesquisa de cotações',
    'subtitle' => 'Introduza as consultas (autores ou citações) no campo de texto abaixo.',
    'search-label' => 'Introduzir consulta',
    'results-label' => 'Resultados da pesquisa',
];
