<?php
return [
    'query' => [
        'alteration' => [
            'new' => 'Contém resultados para <a href=":link">:alteração <img class= "mg-icon" src="/img/icon-lupe.svg"></a>',
            'original' => 'Em vez disso, procure por <a href=":link">:original <img class= "mg-icon" src="/img/icon-lupe.svg"></a>',
        ],
    ],
    'images' => [
        'external' => [
            'bing' => "Pesquisar no Bing",
            'buy' => "Comprar uma chave MetaGer",
            'google' => "Pesquisar no Google",
            'description' => "Só pesquisou na base de dados de Pixabay. É melhor procurar com uma chave MetaGer.",
            'heading' => "Faltam aqui alguns resultados",
            'or' => "ou",
            'save' => "Recordar decisão (com cookie)",
        ],
    ],
    'results' => [
        'summary' => 'Mostrar :resultcount de cerca de :totalresults resultados da pesquisa',
    ],
    'backtotop' => 'voltar ao topo',
    'zurueck' => 'voltar',
    'weiter' => 'mais',
];
