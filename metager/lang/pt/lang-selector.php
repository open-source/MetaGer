<?php
return [
    'lang' => [
        'da' => 'Dinamarquesa',
        'nl' => 'holandês',
        'sv' => 'Sueco',
        'es' => 'espanhol',
        'de' => 'alemão',
        'no' => 'Norueguês',
        'fi' => 'finlandês',
        'pl' => 'Polaco',
        'fr' => 'francês',
        'it' => 'italiano',
        'en' => 'Inglês',
        'pt' => 'Português',
    ],
    'translate' => [
        'title' => 'Encontrou algum erro nas nossas traduções?',
        'description' => 'Se encontrar um erro nas nossas traduções, teremos todo o gosto em receber as suas sugestões diretamente através da nossa instância de weblate <a href="https://translate.metager.de"></a> (não é necessária uma conta). Em alternativa, pode enviar-nos as suas conclusões através do nosso formulário de contacto <a href=":contactlink"></a> .',
    ],
    'detection' => [
        'title' => 'Qual é a definição de língua predefinida?',
        'description' => 'Utilizamos URLs específicos para cada língua que podem ser utilizados diretamente. Se não for definida uma língua no URL, utilizamos a definição de língua armazenada nas definições do seu browser para pré-selecionar a região mais adequada para si. Na maioria dos casos, os resultados da pesquisa serão automaticamente apresentados para a região correta. Em alternativa, pode alterar a definição acima referida.',
    ],
    'h1' => [
        '1' => 'Seleção da língua',
    ],
    'description' => 'Abaixo encontra-se uma lista de todas as línguas/regiões atualmente suportadas pela pesquisa MetaGer. Os resultados da sua pesquisa serão optimizados para a região selecionada. Utilizamos um cookie para guardar uma definição alterada.',
];
