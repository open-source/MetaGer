<?php
return [
    'heading' => "Bloqueador de anúncios no MetaGer",
    'adblocker' => "Em alguns casos, estas ligações de afiliados são falsamente ocultadas pelos bloqueadores de anúncios. Se for afetado e pretender que os resultados da pesquisa sejam apresentados na íntegra, tem as seguintes opções:",
    'options' => [
        'disable' => "Desativar o bloqueador de anúncios para o MetaGer. Os resultados de pesquisa com hiperligações de afiliados deixam de estar ocultos.",
        'key' => "Utilize a nossa pesquisa <a href=\":keylink\">sem anúncios</a>. Os resultados da sua pesquisa deixarão de conter quaisquer ligações de afiliados.",
    ],
    'free' => "A utilização do MetaGer é gratuita, mas a sua manutenção não é. Sempre financiámos o nosso serviço através de publicidade não personalizada. Uma nova adição é a pesquisa opcional sem anúncios usando uma chave <a href=\":keylink\">MetaGer</a>. A nossa publicidade consiste tanto em publicidade tradicional de pesquisa como em ligações de afiliados nos resultados da pesquisa. Com este rendimento podemos pagar à nossa equipa, os custos do servidor e, claro, os serviços de pesquisa que utilizamos como fontes.",
];
