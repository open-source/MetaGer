<?php
return [
    'metager' => [
        'manuell' => 'Instalação manual',
        '1' => 'Esta aplicação traz toda a potência do Metager para o seu smartphone. Pesquise na Web com um toque, preservando a sua privacidade.',
        '2' => 'Há duas formas de obter a nossa aplicação: instalar através da Google Playstore ou (melhor para a sua privacidade) obtê-la diretamente do nosso servidor.',
        'fdroid' => 'Loja F-Droid',
        'playstore' => 'Google Playstore',
    ],
    'maps' => [
        '1' => 'Esta aplicação proporciona uma integração nativa do <a href="https://maps.metager.de" target="_blank">MetaGer Maps</a> (desenvolvido por <a href="https://www.openstreetmap.de/" target="_blank">Openstreetmap</a>) no seu dispositivo móvel Android.',
        '4' => 'Após o primeiro arranque, ser-lhe-ão pedidas as seguintes permissões:',
        '3' => 'O APK para instalação manual tem cerca de 4x o tamanho da instalação da playstore (~250MB) porque contém bibliotecas para todas as arquitecturas de CPU comuns. O atualizador integrado conhecerá a arquitetura da CPU do seu dispositivo e instalará a versão correta (pequena) da aplicação na primeira atualização. Se conhecer a arquitetura do seu dispositivo, também pode <a href="https://gitlab.metager.de/metagermaps/android/-/releases/permalink/latest" target="_blank">instalar diretamente o pacote pequeno</a>.',
        '2' => 'Por conseguinte, o planeador de itinerários e o serviço de navegação funcionam muito rapidamente no seu smartphone. A aplicação é mais rápida em comparação com a utilização num navegador Web móvel. E há mais algumas vantagens - veja!',
        'list' => [
            '2' => 'O APK para instalação manual tem um atualizador integrado. Para que o atualizador funcione, a aplicação pedirá permissão para publicar notificações, a fim de o avisar de uma atualização disponível, e utiliza a permissão Android REQUEST_INSTALL_PACKAGES para lhe poder pedir que instale a atualização da aplicação',
            '1' => 'Acesso aos dados de posicionamento => Com o GPS ativado, podemos fornecer melhores resultados de pesquisa. Com isto, tem acesso à navegação passo a passo. <b> Naturalmente, não armazenamos nenhum dos seus dados e não os cedemos a terceiros.</b>',
        ],
    ],
    'head' => [
        '1' => 'MetaGer Apps',
        '4' => 'Instalação',
        '2' => 'MetaGer App',
        '3' => 'Aplicação de mapas MetaGer',
    ],
    'disclaimer' => [
        '1' => 'Atualmente, só dispomos de uma versão Android da nossa aplicação.',
    ],
];
