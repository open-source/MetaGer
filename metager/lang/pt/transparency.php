<?php
return [
    'text' => [
        '4' => 'Pegamos nas classificações dos nossos motores de busca de origem e pesamo-las. Estas classificações são depois convertidas em pontuações. São atribuídos ou deduzidos pontos adicionais pela ocorrência dos termos de pesquisa no URL e no snippet, bem como pela ocorrência excessiva de caracteres especiais (por exemplo, outros conjuntos de caracteres, como o cirílico). Também utilizamos uma lista de bloqueio para remover páginas individuais da lista de resultados. Bloqueamos páginas Web no ecrã se formos legalmente obrigados a fazê-lo. Também nos reservamos o direito de bloquear páginas Web com informações comprovadamente incorrectas, páginas Web de qualidade extremamente fraca e outras páginas Web particularmente duvidosas.',
        '3' => 'Uma vantagem clara dos motores de metapesquisa é que o utilizador só precisa de uma única consulta de pesquisa para aceder aos resultados de vários motores de pesquisa. O motor de metapesquisa apresenta os resultados relevantes numa lista de resultados novamente ordenada. O MetaGer não é um motor de meta-pesquisa puro, uma vez que também utilizamos pequenos índices próprios.',
        '1' => 'O MetaGer é transparente. O nosso código-fonte <a href=":sourcecode"></a> é licenciado gratuitamente e está disponível publicamente para todos verem. Não armazenamos os dados do utilizador e valorizamos a proteção e a privacidade dos dados. Por isso, concedemos acesso anónimo aos resultados da pesquisa. Isto é possível através de um proxy anónimo e de um acesso oculto por TOR. Além disso, o MetaGer tem uma estrutura organizacional transparente, uma vez que é apoiado pela associação sem fins lucrativos <a href=":sumalink">SUMA-EV</a>, da qual qualquer pessoa pode tornar-se membro.',
        '5' => 'Se tiver mais questões ou dúvidas, não hesite em utilizar o nosso formulário de contacto <a href=":contact"></a> e coloque-nos as suas questões!',
        '2' => [
            '1' => 'Para explicar o que são os motores de meta-pesquisa, faz sentido começar por explicar brevemente como funciona a indexação dos motores de pesquisa normais. Os motores de pesquisa normais obtêm os seus resultados de pesquisa a partir de uma base de dados de páginas Web, também designada por índice. Os motores de pesquisa utilizam os chamados "crawlers", que recolhem páginas Web e as adicionam ao índice (base de dados). O "crawler" começa com um conjunto de páginas Web e abre todas as páginas Web para as quais tem ligações. Estas são indexadas, ou seja, adicionadas ao índice. De seguida, o rastreador abre as páginas Web ligadas a essas páginas Web e continua desta forma.',
            '2' => 'Um metamotor de pesquisa combina os resultados de vários motores de pesquisa e avalia-os novamente de acordo com os seus próprios critérios. Isto significa que o motor de meta-pesquisa não tem o seu próprio índice. Por conseguinte, os motores de meta-pesquisa não utilizam rastreadores. Utilizam o índice de outros motores de pesquisa.',
        ],
        'compliance' => 'Cumprimos os pedidos das autoridades se formos legalmente obrigados a fazê-lo e chegarmos à conclusão de que o nosso cumprimento não viola as liberdades fundamentais. Levamos esta análise muito a sério. Além disso, armazenamos o mínimo possível de dados pessoais para reduzir o risco de ter de os divulgar. No quadro que se segue, encontrará dados sobre os pedidos das autoridades que processámos nos últimos 5 anos. Em breve, serão fornecidas mais informações.',
    ],
    'head' => [
        '5' => 'Como é composta a nossa classificação?',
        'compliance' => 'Como é que a MetaGer responde aos pedidos das autoridades?',
        '4' => 'Qual é a vantagem de um motor de meta-pesquisa?',
        '2' => 'MetaGer é transparente',
        '3' => 'O que é um motor de meta-pesquisa?',
        '1' => 'Declaração de transparência',
    ],
    'table' => [
        'compliance' => [
            'th' => [
                'authinfocomp' => 'Pedidos de informação satisfeitos',
                'authblockcomp' => 'Pedidos de bloqueio satisfeitos',
            ],
        ],
    ],
    'alt' => [
        'text' => [
            '1' => 'Representação visual de dois índices que se complementam para formar um meta-índice',
        ],
    ],
];
