<?php
return [
    'opensearch' => 'MetaGer: turvallinen haku ja löytäminen, yksityisyyden suojaaminen',
    'startseite' => 'MetaGerin aloitussivu',
    'impressum' => 'sivuston ilmoitus',
    'search-placeholder' => 'Hakukyselysi...',
    'metager3' => 'Olet tällä hetkellä MetaGerin testiversiossa.',
    'engines' => [
        'queried' => 'Haetut hakupalvelut',
        'disabled' => 'Hakupalvelujen lisääminen kyselyyn',
        'payment_required' => 'Hakupalvelut saatavilla <a href=\':link\'>MetaGer-avaimella.</a>',
    ],
    'skiplinks' => [
        'heading' => 'Nopea siirtyminen sisältöön',
        'results' => 'Hyppää hakutuloksiin',
        'query' => 'Siirry hakukyselyn syöttökenttään',
        'settings' => 'Siirry hakuasetuksiin',
        'navigation' => 'Hyppää navigointiin',
        'return' => 'Voit palata tähän valikkoon milloin tahansa painamalla escape-näppäintä.',
    ],
];
