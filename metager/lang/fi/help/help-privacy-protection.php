<?php
return [
    'title' => 'MetaGer - Apua',
    'backarrow' => 'Takaisin',
    'tor' => [
        'title' => 'TOR Piilotettu palvelu <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-torhidden" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'MetaGer on piilottanut IP-osoitteet ja jättänyt ne tallentamatta jo vuosien ajan. Osoitteet ovat kuitenkin tilapäisesti näkyvissä MetaGer-palvelimella haun ollessa käynnissä: jos MetaGer vaarantuu, hyökkääjä voi lukea ja tallentaa osoitteesi. Täyttääkseen korkeimmat turvallisuusvaatimukset käytämme MetaGer-instanssia Tor-verkossa: MetaGer TOR Hidden Service -palvelu, johon pääsee osoitteesta: <a href="/tor/" target="_blank" rel="noopener">https://metager.de/tor/</a>. Sen käyttämiseen tarvitset erityisen selaimen, jonka voit ladata osoitteesta <a href="https://www.torproject.org/" target="_blank" rel="noopener">https://www.torproject.org/</a>.',
        '2' => 'Voit käyttää MetaGeriä Tor-selaimessa osoitteessa: http://metagerv65pwclop2rsfzg4jwowpavpwd6grhhlvdgsswvo6ii4akgyd.onion .',
    ],
    'proxy' => [
        'title' => 'MetaGer-välityspalvelimen anonymisointi <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-proxy" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Käyttääksesi sitä sinun tarvitsee vain klikata "OPEN ANONYMOUSLY" MetaGer-tulossivun tuloksen alareunassa. Tämän jälkeen pyyntösi ohjataan kohdesivustolle anonymisoivan välityspalvelimemme kautta, ja henkilötietosi pysyvät täysin suojattuina. Tärkeää: jos seuraat sivujen linkkejä tästä eteenpäin, pysyt välityspalvelimen suojaamana. Et kuitenkaan voi syöttää uutta osoitetta yläreunan osoitekenttään. Tällöin menetät suojan. Voit nähdä, oletko edelleen suojattu, osoitekentästä, joka näyttää: https://proxy.suma-ev.de/?url=here on todellinen osoite.',
    ],
    'content' => [
        'title' => 'Kyseenalainen sisältö / Nuorten suojelu <a title="For easy help, click here" href="/help/easy-language/privacy-protection#eh-content" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'explanation' => [
            '1' => 'Olen saanut "osumia", jotka eivät ole mielestäni vain ärsyttäviä vaan sisältävät mielestäni myös laitonta sisältöä!',
            '2' => 'Jos löydät internetistä jotain, joka on mielestäsi laitonta tai alaikäisille haitallista, voit ottaa yhteyttä osoitteeseen <a href="mailto:hotline@jugendschutz.net" target="_blank" rel="noopener">hotline@jugendschutz.net</a> sähköpostitse tai käydä osoitteessa <a href="http://www.jugendschutz.net/" target="_blank" rel="noopener">www.jugendschutz.net</a> ja täyttää siellä olevan valituslomakkeen. On hyödyllistä kirjoittaa lyhyesti, mitä pidät sopimattomana ja miten olet törmännyt kyseiseen sisältöön. Voit myös ilmoittaa kyseenalaisesta sisällöstä suoraan meille. Lähetä tätä varten sähköpostia nuorisosuojavastaavalle (<a href="mailto:jugendschutz@metager.de" target="_blank" rel="noopener">jugendschutz@metager.de</a>).',
        ],
    ],
    'maps' => [
        'title' => 'MetaGer-kartat',
        '1' => 'Yksityisyyden suojaaminen maailmanlaajuisten datajättiläisten aikakaudella on myös saanut meidät kehittämään <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a>: (tietojemme mukaan) ainoa reittisuunnittelija, joka tarjoaa täydet toiminnot selaimen ja sovelluksen kautta tallentamatta käyttäjän sijaintia. Kaikki tämä on todennettavissa, koska ohjelmistomme on avointa lähdekoodia. Suosittelemme maps.metager.de:n käyttöön nopeaa sovellusversiotamme. Voit ladata sovelluksemme osoitteesta <a href="/app" target="_blank">täältä</a> (tai tietysti myös Play Storesta).',
        '2' => 'Tätä karttatoimintoa voi käyttää myös MetaGer-hakupalvelusta (ja päinvastoin). Kun olet hakenut termiä MetaGerissä, näet oikeassa yläkulmassa uuden hakukohdan \'Kartat\'. Klikkaamalla sitä pääset vastaavaan karttaan.',
        '3' => 'Kun kartta on ladattu, siinä näkyvät MetaGerin löytämät pisteet (POI = Points of Interest), jotka on lueteltu myös oikeassa sarakkeessa. Zoomattaessa tämä luettelo mukautuu karttaosaan. Kun viet hiiren osoittimen päälle kartalla tai luettelossa, vastaava kohde korostuu. Napsauttamalla \'Tiedot\' saat lisätietoja kyseisestä pisteestä alla olevasta tietokannasta.',
    ],
    'easy-help' => 'Klikkaamalla symbolia <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> pääset yksinkertaistettuun versioon ohjeesta.',
    'privacy' => [
        'title' => "Anonymiteetti ja tietoturva",
        '1' => 'Seurantaevästeet, istuntotunnisteet ja IP-osoitteet <a title="For easy help, click here" href="/hilfe/easy-language/privacy-protection#eh-tracking" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '3' => "Lisätietoja on jäljempänä. Toiminnot löytyvät navigointipalkin kohdasta 'Palvelut'.",
        '2' => 'Mitään näistä ei käytetä, tallenneta, säilytetä tai muutoin käsitellä täällä MetaGerissä (poikkeus: lyhytaikainen tallennus hakkerointi- ja bottihyökkäyksiltä suojaamiseksi). Koska pidämme tätä aihetta erittäin tärkeänä, olemme myös luoneet keinoja, jotka auttavat sinua saavuttamaan korkeimman turvallisuustason: MetaGer TOR Hidden Service ja anonymisoiva välityspalvelimemme.',
    ],
];
