<?php
return [
    'title' => 'MetaGer - Apua',
    'backarrow' => 'Takaisin',
    'app' => [
        'title' => 'Android-sovellus <a title="For easy help, click here" href="/hilfe/easy-language/services#help-app" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Voit käyttää MetaGeriä myös sovelluksena. Lataa yksinkertaisesti <a href="https://metager.de/app" target="_blank" rel="noopener">MetaGer App</a> Android-älypuhelimeesi.',
    ],
    'widget' => [
        'title' => 'MetaGer Widget <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-widget" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Tämä on koodigeneraattori, jonka avulla voit upottaa MetaGerin verkkosivuillesi. Voit käyttää sitä hakujen tekemiseen omalla sivustollasi tai internetissä halutessasi. Jos sinulla on kysyttävää, käytä <a href="/kontakt/" target="_blank" rel="noopener">yhteydenottolomakettamme</a>.',
    ],
    'maps' => [
        'title' => 'MetaGer-kartat <a title="For easy help, click here" href="/hilfe/easy-language/services#eh-maps" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '1' => 'Yksityisyyden suojaaminen maailmanlaajuisten datajättiläisten aikakaudella on myös saanut meidät kehittämään <a href="https://maps.metager.de" target="_blank">https://maps.metager.de</a>: (tietojemme mukaan) ainoa reittisuunnittelija, joka tarjoaa täydet toiminnot selaimen ja sovelluksen kautta tallentamatta käyttäjän sijaintia. Kaikki tämä on todennettavissa, koska ohjelmistomme on avointa lähdekoodia. Suosittelemme maps.metager.de:n käyttöön nopeaa sovellusversiotamme. Voit ladata sovelluksemme osoitteesta <a href="/app" target="_blank">täältä</a> (tai tietysti myös Play Storesta).',
        '2' => 'Tätä karttatoimintoa voi käyttää myös MetaGer-hakupalvelusta (ja päinvastoin). Kun olet hakenut termiä MetaGerissä, näet oikeassa yläkulmassa uuden hakukohdan \'Kartat\'. Klikkaamalla sitä pääset vastaavaan karttaan.',
        '3' => 'Kun kartta on ladattu, siinä näkyvät MetaGerin löytämät pisteet (POI = Points of Interest), jotka on lueteltu myös oikeassa sarakkeessa. Zoomattaessa tämä luettelo mukautuu karttaosaan. Kun viet hiiren osoittimen päälle kartalla tai luettelossa, vastaava kohde korostuu. Napsauttamalla \'Tiedot\' saat lisätietoja kyseisestä pisteestä alla olevasta tietokannasta.',
    ],
    'easy-help' => 'Klikkaamalla symbolia <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> pääset yksinkertaistettuun versioon ohjeesta.',
    'services' => [
        'text' => "Lisäpalvelut haun ympärillä",
    ],
];
