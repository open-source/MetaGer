<?php
return [
    'backarrow' => 'Takaisin',
    'title' => [
        '2' => 'Pääsivujen käyttäminen',
        '1' => 'MetaGer - Apua',
    ],
    'startpage' => [
        'title' => 'Aloitussivu <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-startpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'Aloitussivu sisältää hakukentän, oikeassa yläkulmassa olevan painikkeen, jolla pääset valikkoon, ja kaksi linkkiä hakukentän alapuolella, joiden avulla voit lisätä MetaGerin selaimeesi ja tehdä hakuja ilman mainoksia. Alemmassa osiossa on tietoa MetaGeristä ja SUMA-EV-yhdistyksestä. Lisäksi alareunassa näkyvät painopistealueemme <i>Takuunalainen yksityisyys, Voittoa tavoittelematon yhdistys, Monipuolinen ja vapaa</i> sekä <i>100 % vihreää energiaa</i>. Klikkaamalla kyseisiä osioita tai selaamalla löydät lisätietoja. ',
    ],
    'searchfield' => [
        'title' => 'Hakukenttä <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#eh-searchfield" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => 'Hakukenttä koostuu useista osista:',
        'memberkey' => 'avainsymboli: Tässä voit syöttää avaimesi mainoksetonta hakua varten. Voit myös tarkastella tunnussaldoasi ja hallita avaintasi.',
        'slot' => 'hakukenttä: Kirjoita hakusanasi tähän. Isoja ja pieniä kirjaimia ei eroteta toisistaan.',
        'search' => 'suurennuslasi: Aloita haku klikkaamalla tästä tai painamalla "Enter".',
        'morefunctions' => 'Lisätoiminnot löytyvät valikkokohdasta "<a href = "/hilfe/funktionen">Hakutoiminnot</a>".',
    ],
    'resultpage' => [
        'title' => 'Tulossivu <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-resultpage" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'foci' => 'Hakukentän alla on kolme eri hakukohdetta (Web, Kuvat, Uutiset), joista kukin liittyy tiettyihin hakukoneisiin.',
        'choice' => 'Alla näet kaksi kohtaa: "Suodatin" ja "Asetukset", jos sovellettavissa.',
        'filter' => 'Suodatin: Tässä voit näyttää ja piilottaa suodatinvaihtoehtoja ja soveltaa suodattimia. Kussakin hakukohdassa on erilaisia valintamahdollisuuksia. Jotkin toiminnot ovat käytettävissä vain MetaGer-näppäintä käytettäessä.',
        'settings' => 'Asetukset: MetaGer-hakuasetukset: Täällä voit tehdä pysyviä hakuasetuksia MetaGer-hakua varten nykyisessä fokuksessa. Voit myös valita tarkennukseen liittyviä hakukoneita ja poistaa niiden valinnan. Asetuksesi tallennetaan käyttämällä ei-henkilökohtaisesti tunnistettavaa selkokielistä evästettä. Pääset asetussivulle myös oikeassa yläkulmassa olevan valikon kautta.',
    ],
    'result' => [
        'title' => 'Tulokset <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-results" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        'info' => [
            '1' => 'Kaikki tulokset esitetään seuraavassa muodossa:',
            'open' => '"OPEN": Napsauta otsikkoa tai alla olevaa linkkiä (URL-osoite) avataksesi tuloksen samaan välilehteen.',
            'newtab' => '"OPEN IN NEW TAB" avaa tulokset uuteen välilehteen. Vaihtoehtoisesti voit myös avata uuden välilehden käyttämällä CTRL-näppäintä ja vasenta painiketta tai hiiren keskimmäistä painiketta.',
            'anonym' => '"OPEN ANONYMOUSLY" tarkoittaa, että tulos avataan valtakirjamme suojassa. Tietoa tästä on kohdassa <a href = "/hilfe/datensicherheit#h-proxy">Anonymizing MetaGer Proxy Server</a>.',
            'more' => '<img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>: Kun napsautat <img class="help-ellipsis-image lm-only" src="/img/ellipsis.svg" alt="More"/> <img class="help-ellipsis-image dm-only" src="/img/ellipsis-dm.svg" alt="More"/>, saat uusia vaihtoehtoja; tuloksen ulkoasu muuttuu:',
            '2' => 'Uudet vaihtoehdot ovat:',
            'domainnewsearch' => '"Aloita uusi haku tällä verkkotunnuksella": Tuloksen verkkotunnuksesta suoritetaan yksityiskohtaisempi haku.',
            'hideresult' => '"hide": Tämän avulla voit piilottaa tämän verkkotunnuksen tulokset. Voit myös kirjoittaa tämän kytkimen suoraan hakusanasi jälkeen ja ketjuttaa sen; myös jokerimerkki "*" on sallittu. Katso myös Asetukset pysyvää ratkaisua varten.',
        ],
    ],
    'settings' => [
        'title' => 'Asetukset <a title="For easy help, click here" href="/hilfe/easy-language/mainpages#help-settings" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a>',
        '2' => 'Käytetyt hakukoneet <br> Tässä voit tarkastella ja säätää käyttämiäsi hakukoneita. Klikkaamalla vastaavaa nimeä voit ottaa sen käyttöön tai poistaa sen käytöstä vastaavasti.',
        '3' => 'Hakusuodattimet <br> Hakusuodattimien avulla voit suodattaa hakua pysyvästi.',
        '4' => 'Musta lista <br> Tässä voit luoda henkilökohtaisen mustan listan. Voit käyttää sitä tiettyjen verkkotunnusten suodattamiseen ja omien hakuasetusten luomiseen. Kun napsautat "Lisää", nämä asetukset liitetään linkkiin "Huomautus"-osiossa.',
        '5' => 'Tummennustilan vaihtaminen <br> Siirry tummennustilaan helposti tästä.',
        '6' => 'Avaa tulokset uudessa välilehdessä <br> Tässä voit ottaa pysyvästi käyttöön toiminnon, joka avaa tulokset uuteen välilehteen.',
        '7' => 'Lainaukset <br> Voit kytkeä lainausten näyttämisen päälle ja pois tästä.',
        '1' => 'Mainokseton haku <br> Täällä voit tarkastella avaimesi ja avaimesi saldoa. Sinulla on myös mahdollisuus ladata tai poistaa avaimesi.',
        '9' => 'Hienovaraista mainontaa omille palveluillemme <br> Näytämme hienovaraista mainontaa omille palveluillemme. Voit kytkeä itsemainontamme pois päältä tästä.',
        '8' => 'Palauta kaikki nykyiset asetukset <br> Näyttöön tulee linkki, jonka voit asettaa etusivuksi tai kirjanmerkiksi, jotta voit säilyttää nykyiset asetukset.',
    ],
    'easy-help' => 'Klikkaamalla symbolia <a title="For easy help, click here" href="/hilfe/easy-language/services" ><img class="easy-help-icon lm-only" src="/img/help-questionmark-icon-lm.svg"/><img class="easy-help-icon dm-only" src="/img/help-questionmark-icon-dm.svg"/></a> pääset yksinkertaistettuun versioon ohjeesta.',
];
