<?php
return [
    'plugin' => 'Asenna MetaGer',
    'plugin-title' => 'Lisää MetaGer selaimeesi',
    'key' => [
        'placeholder' => 'Syötä jäsenen avain',
        'tooltip' => [
            'nokey' => 'Aseta mainokseton haku',
            'empty' => 'Merkki käytetty loppuun. Lataa nyt.',
            'low' => 'Merkki pian käytetty. Lataa nyt.',
            'full' => 'Mainokseton haku käytössä.',
        ],
    ],
    'placeholder' => 'MetaGer: Etsi ja löydä yksityisyyden suojaa',
    'searchbutton' => 'Aloita MetaGer-haku',
    'foki' => [
        'web' => 'Web',
        'bilder' => 'Kuvat',
        'nachrichten' => 'Uutiset',
        'science' => 'Tiede',
        'produkte' => 'Tuotteet',
        'maps' => 'Kartat',
    ],
    'adfree' => 'Käytä MetaGeria mainosvapaasti',
    'skip' => [
        'search' => 'Siirry hakukyselyn syöttöön',
        'navigation' => 'Siirry navigointiin',
        'fokus' => 'Siirry hakutarkennuksen valintaan',
    ],
    'lang' => 'wwitch kieli',
    'searchreset' => 'poista hakukyselyn syöttö',
    'searchbar-replacement' => [
        'login' => 'Kirjaudu sisään avaimella',
        'start' => 'Luo uusi avain',
        'message' => 'MetaGer-hakukone on nyt saatavilla vain ilman mainoksia!',
        'why' => 'Miksi?',
        'key_error' => "Syötetty avain ei ollut kelvollinen. Tarkista syöttö.",
        'login_code_error' => "Syötetty kirjautumiskoodi ei ollut voimassa. Vihje: Kirjautumiskoodit ovat voimassa vain silloin, kun ne näkyvät toisessa laitteessa!",
        'payment_id_error' => "Olet syöttänyt maksutunnuksen, joka ei ole oikea avain. Avaimesi on 36 merkkiä pitkä.",
    ],
];
