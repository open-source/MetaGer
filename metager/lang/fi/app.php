<?php
return [
    'head' => [
        '1' => 'Metager sovellukset',
        '2' => 'MetaGer-sovellus',
        '3' => 'MetaGer-karttasovellus',
        '4' => 'Asennus',
    ],
    'disclaimer' => [
        '1' => 'Tällä hetkellä meillä on sovelluksestamme vain Android-versio.',
    ],
    'metager' => [
        '1' => 'Tämä sovellus tuo täyden Metagerin tehon älypuhelimeesi. Etsi verkosta yhdellä kosketuksella ja säilytä samalla yksityisyytesi.',
        '2' => 'Voit hankkia sovelluksemme kahdella tavalla: asenna se Google Playstoren kautta tai (yksityisyytesi kannalta parempi) hanki se suoraan palvelimeltamme.',
        'playstore' => 'Google Playstore',
        'fdroid' => 'F-Droid Store',
        'manuell' => 'Manuaalinen asennus',
    ],
    'maps' => [
        '1' => 'Tämä sovellus tarjoaa <a href="https://maps.metager.de" target="_blank">MetaGer Maps</a> (powered by <a href="https://www.openstreetmap.de/" target="_blank">Openstreetmap</a>) natiivin integroinnin Android-mobiililaitteeseesi.',
        '2' => 'Siksi reittisuunnittelija ja navigointipalvelu toimivat älypuhelimessasi erittäin nopeasti. Sovellus on nopeampi verrattuna mobiililaitteen verkkoselaimessa tapahtuvaan käyttöön. Ja on vielä muitakin etuja - tutustu siihen!',
        '3' => 'Manuaalisen asennuksen APK on noin nelinkertainen Playstore-asennukseen verrattuna (~250MB), koska se sisältää kirjastot kaikille yleisimmille suorittimen arkkitehtuureille. Integroitu päivitysohjelma tuntee laitteesi suorittimen arkkitehtuurin ja asentaa sovelluksen oikean (pienen) version ensimmäisen päivityksen yhteydessä. Jos tiedät itse laitteesi arkkitehtuurin, voit myös <a href="https://gitlab.metager.de/metagermaps/android/-/releases/permalink/latest" target="_blank">asentaa suoraan pienen paketin</a>.',
        'list' => [
            '1' => 'Pääsy paikannustietoihin => Kun GPS on aktivoitu, voimme tarjota parempia hakutuloksia. Tämän avulla saat pääsyn vaiheittaiseen navigointiin. <b> Emme tietenkään säilytä mitään tietojasi emmekä anna mitään tietojasi kolmansille henkilöille.</b>',
            '2' => 'Manuaalisen asennuksen APK:ssa on integroitu päivitin. Jotta päivitin toimisi, sovellus pyytää lupaa lähettää ilmoituksia ilmoittaakseen sinulle saatavilla olevasta päivityksestä ja käyttää Androidin lupaa REQUEST_INSTALL_PACKAGES, jotta se voi pyytää sinua asentamaan sovelluksen päivityksen.',
        ],
        '4' => 'Ensimmäisen käynnistyksen jälkeen sinulta kysytään seuraavat oikeudet:',
    ],
];
