<?php
return [
    'detection' => [
        'description' => 'Käytämme kielikohtaisia URL-osoitteita, joita voidaan käyttää suoraan. Jos URL-osoitteessa ei ole määritetty kieltä, käytämme selaimesi asetuksiin tallennettua kieliasetusta esivalitaksemme sinulle sopivimman alueen. Useimmissa tapauksissa tämä antaa automaattisesti oikean alueen hakutulokset. Vaihtoehtoisesti voit muuttaa edellä mainittua asetusta.',
        'title' => 'Mikä on oletuskieliasetus?',
    ],
    'lang' => [
        'de' => 'saksa',
        'en' => 'englanti',
        'es' => 'espanja',
        'nl' => 'hollanti',
        'fi' => 'suomi',
        'no' => 'norja',
        'it' => 'italia',
        'da' => 'tanska',
        'sv' => 'ruotsi',
        'pl' => 'puola',
        'fr' => 'ranska',
        'pt' => 'Portugues',
    ],
    'h1' => [
        '1' => 'Kielen valinta',
    ],
    'description' => 'Alla on luettelo kaikista kielistä/alueista, joita MetaGer-haku tällä hetkellä tukee. Hakutulokset optimoidaan valitulle alueelle. Käytämme evästettä muuttuneen asetuksen tallentamiseen.',
    'translate' => [
        'title' => 'Löysitkö virheitä käännöksissämme?',
        'description' => 'Jos olet löytänyt virheen käännöksissämme, otamme mielellämme vastaan ehdotuksesi suoraan <a href="https://translate.metager.de">weblate-instanssin</a> kautta (tiliä ei tarvita). Vaihtoehtoisesti voit lähettää meille havaintosi sähköpostitse <a href=":contactlink">yhteydenottolomakkeemme</a> kautta.',
    ],
];
