<?php

return [
    "url" => env("INVOICENINJA_URL", "https://invoicing.co"),
    "access_token" => env("INVOICENINJA_TOKEN", ""),
    "logs_group_id" => env("INVOICENINJA_LOGS_GROUP_ID", ""),
];