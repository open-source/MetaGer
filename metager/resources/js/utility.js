import { statistics } from "./statistics";

document.addEventListener("DOMContentLoaded", (event) => {
  document
    .querySelectorAll(".js-only")
    .forEach((el) => el.classList.remove("js-only"));
  document.querySelectorAll(".no-js").forEach((el) => el.classList.add("hide"));
  document.querySelectorAll(".print-button").forEach((el) =>
    el.addEventListener("pointerdown", () => {
      window.print();
    })
  );
  document.querySelectorAll(".copyLink").forEach((el) => {
    let input_field = el.querySelector("input[type=text]");
    let copy_button = el.querySelector("button");
    if (copy_button) {
      copy_button.addEventListener("click", (e) => {
        // Select all the text
        let key = input_field.value;
        input_field.select();
        navigator.clipboard
          .writeText(key)
          .then(() => {
            copy_button.classList.add("success");
            setTimeout(() => {
              copy_button.classList.remove("success");
            }, 3000);
          })
          .catch((reason) => {
            console.error(reason);
            copy_button.classList.add("failure");
            setTimeout(() => {
              copy_button.classList.remove("failure");
            }, 3000);
          });
      });
    }
  });

  let key_keyup = function (e) {
    let el = e.target;
    let has_focus = document.activeElement == el;
    if (el.value.match(/^\d{6}$/) && el.dataset.type != "otp") {
      let clone = el.cloneNode(true);
      clone.setAttribute("type", "text");
      clone.setAttribute("autocomplete", "one-time-code");
      clone.dataset.type = "otp";
      el.replaceWith(clone);
      clone.addEventListener("keyup", key_keyup);
      clone.addEventListener("focus", key_focus);
      clone.addEventListener("blur", key_blur);
      if (has_focus) {
        clone.focus();
        clone.setSelectionRange(clone.value.length, clone.value.length);
      }
    } else if (el.dataset.type != "password") {
      let clone = el.cloneNode(true);
      clone.setAttribute("type", "password");
      clone.removeAttribute("autocomplete");
      clone.dataset.type = "password";
      el.replaceWith(clone);
      clone.addEventListener("keyup", key_keyup);
      clone.addEventListener("focus", key_focus);
      clone.addEventListener("blur", key_blur);
      if (has_focus) {
        clone.focus();
        clone.setSelectionRange(clone.value.length, clone.value.length);
      }
    }
  };

  let key_focus = function (e) {
    e.target.type = "text";
  };
  let key_blur = function (e) {
    if (!e.target.value.match(/^\d{6}$/)) {
      e.target.type = "password";
    }
  };
  document.querySelectorAll("input[type=password][name=key]").forEach((el) => {
    el.addEventListener("keyup", key_keyup);
    el.addEventListener("focus", key_focus);
    el.addEventListener("blur", key_blur);

    el.dispatchEvent(new Event("keyup"));

    let error_key = new URLSearchParams(document.location.search).get(
      "invalid_key"
    );
    if (error_key != null && error_key.length > 0) {
      el.dispatchEvent(new Event("keyup"));
    }
  });

  let sidebarToggle = document.getElementById("sidebarToggle");
  if (sidebarToggle) {
    document.querySelectorAll("label[for=sidebarToggle]").forEach((label) => {
      label.addEventListener("click", (e) => {
        e.preventDefault();
        sidebarToggle.checked = !sidebarToggle.checked;
      });
    });
  }

  // Add a element with the ID "plugin-btn" if it does not exist on this page
  // Used to determine if a web extension is installed
  if (document.getElementById("plugin-btn") == null) {
    let new_container = document.createElement("div");
    new_container.classList.add("hidden");
    new_container.id = "plugin-btn";
    document.querySelector("body").appendChild(new_container);
  }

  backButtons();
});

reportJSAvailabilityForAuthenticatedSearch();
function reportJSAvailabilityForAuthenticatedSearch() {
  let Cookies = require("js-cookie");
  let key_cookie = Cookies.get("key");

  if (key_cookie !== undefined) {
    Cookies.set("js_available", "true", { sameSite: "Lax" });
  }
}

// Implement Back button functionality
function backButtons() {
  document.querySelectorAll(".back-button").forEach((button) => {
    button.style.display = "block";
    button.addEventListener("click", (e) => {
      let href = button.href;
      // Use the defined URL on the button if there is one
      if (href && href.trim().length !== 0 && href.trim() != "#") {
        return;
      }
      e.preventDefault();
      history.back();
    });
  });
}

(async () => {
  statistics.registerPageLoadEvents();
})();

(async () => {
  // Check login status on the startpage (temporary fix for chrome extension not initializing fast enough on startup)
  let starttime = (new Date()).getTime();

  setTimeout(checkLoginStatus, 500);  // Start after 500 ms

  async function checkLoginStatus() {
    let key_container = document.querySelector("input[name=key]#key");
    let login_button = document.querySelector("button#login");
    if (key_container == null || login_button == null) return;  // Stop if there is no key input field
    return fetch("/authorized", {
      method: "POST"
    }).then(response => {
      if (response.status == 401) {
        if ((new Date()).getTime() - starttime > 5000) return;
        return (new Promise(resolve => { setTimeout(resolve, 1000) })).then(() => checkLoginStatus());
      } else {
        response.json().then(json_response => {
          if (json_response.hasOwnProperty("is_bugged_extension") && json_response.is_bugged_extension) {
            document.location.pathname += "meta/meta.ger3";
          } else {
            document.location.replace(document.location.href);
          }
        });
        return;
      }
    });
  }
})();

document.addEventListener("readystatechange", (e) => {
  if (document.readyState == "complete") {
    setTimeout(() => {
      // Check if a web extension is active
      let extension_installed = document.getElementById("plugin-btn") == null;
      if (extension_installed) {
        updateWebExtensionStatus(new Date().getTime());
      } else {
        updateWebExtensionStatus("no");
      }
    }, 250);
  }
});

function updateWebExtensionStatus(time) {
  let Cookies = require("js-cookie");
  let extension_cookie = Cookies.get("webextension");
  if (time != "no" && extension_cookie == undefined) {
    Cookies.set("webextension", time, { sameSite: "Lax" });
  } else if (time == "no" && extension_cookie != undefined) {
    Cookies.remove("webextension");
  }
  if (localStorage) {
    localStorage.setItem("webextension", time);
  }
  window.webextension = time;
  window.dispatchEvent(
    new CustomEvent("webextension_status_update", { detail: time })
  );
}

(() => {
  let url = new URL(document.location)
  if (url.searchParams.has("key")) {
    // Remove the key from the visible URL in the browser
    url.searchParams.delete("key");
    window.history.replaceState({}, null, url);
  }
})();