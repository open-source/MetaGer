<?php

namespace App\Models\Configuration;

use App\Models\Authorization\Authorization;
use App\Models\DisabledReason;
use App\Models\Searchengine;
use App\Models\SearchengineConfiguration;
use App\SearchSettings;
use Cookie;
use Log;
use Request;

/**
 * Stores all available Searchengines for usage
 * Reads in sumas.json configuration file to do so
 */
class Searchengines
{
    /** @var SearchEngine[] */
    public $sumas = [];

    /** @var DisabledReason[] */
    public $disabledReasons = [];
    public $user_settings = [];

    public function __construct()
    {
        $settings = app(SearchSettings::class);
        $this->user_settings = [];
        foreach ($settings->sumasJson->sumas as $name => $info) {
            $path = "App\\Models\\parserSkripte\\" . $info->{"parser-class"};
            // Check if parser exists
            try {
                $configuration = new SearchengineConfiguration($info);
                $this->sumas[$name] = new $path($name, $configuration);
            } catch (\ErrorException $e) {
                Log::error("Konnte " . $info->infos->display_name . " nicht abfragen. " . $e);
                continue;
            }
        }

        $engines_in_fokus = $settings->sumasJson->foki->{$settings->fokus}->sumas;

        foreach ($this->sumas as $name => $suma) {
            // Parse user configuration
            // Default mode for this searchengine. Can be overriden by the user configuration
            if ($suma->configuration->disabledByDefault) {
                $suma->configuration->disabled = true;
                $suma->configuration->disabledReasons[] = DisabledReason::SUMAS_DEFAULT_CONFIGURATION;
                $this->disabledReasons[] = DisabledReason::SUMAS_DEFAULT_CONFIGURATION;
            }
            // User setting defined via permanent cookie in browser
            $engine_user_setting = $this->parseUserEngineSetting($name);

            if ($engine_user_setting !== null) {
                if ($engine_user_setting === "off" && $suma->configuration->disabled === false) {
                    $suma->configuration->disabled = true;
                    $suma->configuration->disabledReasons[] = DisabledReason::USER_CONFIGURATION;
                    $this->disabledReasons[] = DisabledReason::USER_CONFIGURATION;
                }
                if ($engine_user_setting === "on" && $suma->configuration->disabled === true) {
                    $suma->configuration->disabled = false;
                }
            }
        }

        foreach ($this->sumas as $suma) {
            if (!app(Authorization::class)->canDoAuthenticatedSearch(false) && $suma->configuration->cost > 0) {
                $suma->configuration->disabled = true;
                $suma->configuration->disabledReasons[] = DisabledReason::PAYMENT_REQUIRED;
                $this->disabledReasons[] = DisabledReason::PAYMENT_REQUIRED;
            }
            // Disable searchengine if it serves ads and this request is authorized
            if ($suma->configuration->ads && app(Authorization::class)->canDoAuthenticatedSearch()) {
                $suma->configuration->disabled = true;
                $suma->configuration->disabledReasons[] = DisabledReason::SERVES_ADVERTISEMENTS;
                $this->disabledReasons[] = DisabledReason::SERVES_ADVERTISEMENTS;
            }
            // Disable all searchengines not supported by this fokus
            if (!in_array($suma->name, $engines_in_fokus)) {
                $suma->configuration->disabled = true;
                $suma->configuration->disabledReasons[] = DisabledReason::INCOMPATIBLE_FOKUS;
                $this->disabledReasons[] = DisabledReason::INCOMPATIBLE_FOKUS;
            }
            // Disable all searchengines not supporting the current locale
            $suma->configuration->applyLocale();
        }

        $settings->loadQueryFilter();
        $settings->loadParameterFilter($this);

        $authorization = app(Authorization::class);

        $authorization->setCost(0); // Update cost with actual cost that are correct for current engine configuration

        foreach ($this->sumas as $suma) {
            if ($suma->configuration->disabled) {
                continue;
            }
            // Disable searchengine if it does not support a possibly defined query filter
            foreach ($settings->queryFilter as $filterName => $filter) {
                if (empty($settings->sumasJson->filter->{"query-filter"}->$filterName->sumas->{$suma->name})) {
                    $suma->configuration->disabled = true;
                    $suma->configuration->disabledReasons[] = DisabledReason::INCOMPATIBLE_FILTER;
                    $this->disabledReasons[] = DisabledReason::INCOMPATIBLE_FILTER;
                    continue 2;
                }
            }
            // Disable searchengine if it does not support a possibly defined parameter filter
            foreach ($settings->parameterFilter as $filterName => $filter) {
                // If a searchengine does not support safesearch and safesearch is set to off
                // it will not get disabled as it is probably the same
                if ($filterName === "safesearch" && $filter->value === "o") {
                    continue;
                }
                // We need to check if the searchengine supports the parameter value, too
                if ($filter->value !== null && (empty($filter->sumas->{$suma->name}) || empty($filter->sumas->{$suma->name}->values->{$filter->value}))) {
                    $suma->configuration->disabled = true;
                    $suma->configuration->disabledReasons[] = DisabledReason::INCOMPATIBLE_FILTER;
                    $this->disabledReasons[] = DisabledReason::INCOMPATIBLE_FILTER;
                    continue 2;
                }
            }
            // Apply final settings to Sumas
            $suma->applySettings();
            if (!$suma->configuration->disabled && $suma->configuration->cost > 0) {
                $authorization->setCost($authorization->getCost() + $suma->configuration->cost);
            }
        }

        $this->handleFilterOptIn();

        uasort($this->sumas, function ($a, $b) {
            if ($a->configuration->engineBoost === $b->configuration->engineBoost) {
                return 0;
            }
            return ($a->configuration->engineBoost > $b->configuration->engineBoost) ? -1 : 1;
        });
    }

    /**
     * There are disabled searchengines which are disabled by default but can be automatically
     * enabled if it has the filterOptIn option set to true and the user has selected a focus
     * which would otherwise not be supported by another searchengine
     * 
     * @return void
     */
    private function handleFilterOptIn()
    {
        $filter_disabled_engine_present = false;    // Is there a searchengine disabled only because of the selected filter
        $filter_opt_in_engines = [];      // Is there a searchengine which could serve the filter and has filterOptIn enabled

        $authorization = app(Authorization::class);
        foreach ($this->sumas as $suma) {
            if (!$suma->configuration->disabled)
                return;    // If there is an enabled searchengine there is no need for filterOptIn mechanic
            if (sizeof($suma->configuration->disabledReasons) === 1) {
                if (in_array(DisabledReason::INCOMPATIBLE_FILTER, $suma->configuration->disabledReasons)) {
                    $filter_disabled_engine_present = true;
                }
                if (in_array(DisabledReason::SUMAS_DEFAULT_CONFIGURATION, $suma->configuration->disabledReasons) && $suma->configuration->filterOptIn === true) {
                    $filter_opt_in_engines[] = $suma;
                }
            }
        }
        if ($filter_disabled_engine_present && sizeof($filter_opt_in_engines) > 0) {
            foreach ($filter_opt_in_engines as $suma) {
                $suma->configuration->disabled = false;
                $suma->configuration->disabledReasons = [];
                if ($suma->configuration->cost > 0) {
                    $authorization->setCost($authorization->getCost() + $suma->configuration->cost);
                }
                $suma->applySettings();
            }
        }
    }

    public function getSearchEnginesForFokus()
    {
        $settings = app(SearchSettings::class);
        $engines_in_fokus = $settings->sumasJson->foki->{$settings->fokus}->sumas;
        $sumas = [];
        foreach ($this->sumas as $name => $suma) {
            if (in_array($name, $engines_in_fokus)) {
                $sumas[$name] = $suma;
            }
        }
        return $sumas;
    }

    public function getEnabledSearchengines()
    {
        $sumas = [];
        foreach ($this->sumas as $suma) {
            if ($suma->configuration->disabled === false) {
                $sumas[] = $suma;
            }
        }
        return $sumas;
    }

    public function getEnabledSearchengine(string $name): Searchengine|null
    {
        if (array_key_exists($name, $this->sumas) && $this->sumas[$name]->configuration->disabled === false) {
            return $this->sumas[$name];
        } else {
            return null;
        }
    }

    /**
     * Is there a disabled searchengine with given reason
     *
     * @return bool
     */
    public function hasDisabledSearchenginesWithReason(DisabledReason $reason)
    {
        foreach ($this->sumas as $suma) {
            if ($suma->configuration->disabled && in_array($reason, $suma->configuration->disabledReasons)) {
                return true;
            }
        }
        return false;
    }

    public function getSearchCost()
    {
        $cost = 0;
        foreach ($this->sumas as $suma) {
            if (!$suma->configuration->disabled && $suma->configuration->cost > 0) {
                $cost += $suma->configuration->cost;
            }
        }
        return $cost;
    }

    public function checkPagination()
    {
        if (!\Request::has("next") || !\Cache::has(\Request::input("next"))) {
            return;
        }
        $next = unserialize(\Cache::get(\Request::input("next")));
        // Pagination call detected. Disable all Searchengines and replace the searchengines with the cached ones
        foreach ($this->sumas as $suma) {
            $suma->configuration->disabled = true;
            $suma->configuration->disabledReasons[] = DisabledReason::SUMAS_CONFIGURATION;
        }
        foreach ($next["engines"] as $engine) {
            foreach ($this->sumas as $name => $suma) {
                if ($engine instanceof $suma) {
                    $this->sumas[$name] = $engine;
                }
            }
        }
        $settings = app(SearchSettings::class);
        $settings->page = $next["page"];
    }

    /**
     * Parses the current request and checks if the specified engine setting is defined in the following order:
     * 1. GET-Parameter
     * 2. HTTP Header with that name
     * 3. Cookie 
     * 
     * @param string $engine_name The name of the searchengine
     * @param bool $global (Optional) Is this setting global or specific to a focus
     * @param bool|string|null $default (Optional) Default value to return if setting is not defined anywhere
     * @return string|null
     */
    private function parseUserEngineSetting(string $engine_name, $default = null): string|null
    {
        $settings = app(SearchSettings::class);
        $valid_values = ["on", "off"];
        /**
         * Check GET-Parameter in all variations
         */
        // Setting defined directly in GET Parameters
        if (Request::filled($engine_name)) {
            $value = Request::input($engine_name, $default);
            if (in_array($value, $valid_values)) {
                $this->user_settings[$engine_name] = $value;
                return $value;
            }
        }
        // Setting defined without fokus prefix which will be handled as matching all foki
        if (stripos($engine_name, $settings->fokus . "_engine_") === 0 && Request::filled(str_replace($settings->fokus . "_engine_", "", $engine_name))) {
            $value = Request::input(str_replace($settings->fokus . "_engine_", "", $engine_name), $default);
            if (in_array($value, $valid_values)) {
                $this->user_settings[$engine_name] = $value;
                return $value;
            }
        }
        // Setting defined with fokus prefix in request parameters and fokus matches currently used one
        if (stripos($engine_name, $settings->fokus . "_engine_") === false && Request::filled($settings->fokus . "_engine_" . $engine_name)) {
            $value = Request::input($settings->fokus . "_engine_" . $engine_name, $default);
            if (in_array($value, $valid_values)) {
                $this->user_settings[$engine_name] = $value;
                return $value;
            }
        }

        /**
         * Check Request HTTP Header in all variations
         */
        // Setting defined directly in GET Parameters
        if (Request::hasHeader($engine_name)) {
            $value = Request::header($engine_name, $default);
            if (in_array($value, $valid_values)) {
                $this->user_settings[$engine_name] = $value;
                return $value;
            }
        }
        // Setting defined without fokus prefix which will be handled as matching all foki
        if (stripos($engine_name, $settings->fokus . "_engine_") === 0 && Request::hasHeader(str_replace($settings->fokus . "_engine_", "", $engine_name))) {
            $value = Request::header(str_replace($settings->fokus . "_engine_", "", $engine_name), $default);
            if (in_array($value, $valid_values)) {
                $this->user_settings[$engine_name] = $value;
                return $value;
            }
        }
        // Setting defined with fokus prefix in request parameters and fokus matches currently used one
        if (stripos($engine_name, $settings->fokus . "_engine_") === false && Request::hasHeader($settings->fokus . "_engine_" . $engine_name)) {
            $value = Request::header($settings->fokus . "_engine_" . $engine_name, $default);
            if (in_array($value, $valid_values)) {
                $this->user_settings[$engine_name] = $value;
                return $value;
            }
        }

        /**
         * Check Cookies in all variations
         */
        // Setting defined directly in GET Parameters
        if (Cookie::has($engine_name)) {
            $value = Cookie::get($engine_name, $default);
            if (in_array($value, $valid_values)) {
                $this->user_settings[$engine_name] = $value;
                return $value;
            }
        }
        // Setting defined without fokus prefix which will be handled as matching all foki
        if (stripos($engine_name, $settings->fokus . "_engine_") === 0 && Cookie::has(str_replace($settings->fokus . "_engine_", "", $engine_name))) {
            $value = Cookie::get(str_replace($settings->fokus . "_engine_", "", $engine_name), $default);
            if (in_array($value, $valid_values)) {
                $this->user_settings[$engine_name] = $value;
                return $value;
            }
        }
        // Setting defined with fokus prefix in request parameters and fokus matches currently used one
        if (stripos($engine_name, $settings->fokus . "_engine_") === false && Cookie::has($settings->fokus . "_engine_" . $engine_name)) {
            $value = Cookie::get($settings->fokus . "_engine_" . $engine_name, $default);
            if (in_array($value, $valid_values)) {
                $this->user_settings[$engine_name] = $value;
                return $value;
            }
        }

        return $default;
    }
}