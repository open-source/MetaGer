<?php

namespace App\Http\Controllers;

use App\Localization;
use \App\MetaGer;
use App\Models\Authorization\Authorization;
use App\Models\Authorization\KeyAuthorization;
use App\Models\Configuration\Searchengines;
use App\Models\DisabledReason;
use App\SearchSettings;
use Cookie;
use \Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use LaravelLocalization;

class SettingsController extends Controller
{
    public function index(Request $request)
    {
        $settings = app(SearchSettings::class);
        $searchengines = app(Searchengines::class);
        $sumas = $searchengines->getSearchEnginesForFokus();
        $fokus = $settings->fokus;
        $fokusName = trans('index.foki.' . $fokus);

        $langFile = MetaGer::getLanguageFile();
        $langFile = json_decode(file_get_contents($langFile));


        # Parse the Parameter Filter
        $filters = $settings->parameterFilter;

        $filteredSumas = false;
        foreach ($langFile->filter->{"parameter-filter"} as $name => $filter) {
            foreach ($sumas as $name => $suma) {
                if ($suma->configuration->disabled && in_array(DisabledReason::INCOMPATIBLE_FILTER, $suma->configuration->disabledReasons)) {
                    $filteredSumas = true;
                }
            }
        }

        $authorization = app(Authorization::class);
        $url = $request->input('url', '');

        // Check if any setting is active
        $settingActive = false;
        if (sizeof($settings->user_settings) > 0 || sizeof($searchengines->user_settings) > 0) {
            $settingActive = true;
        }

        # Reading cookies for black list entries
        $blacklist_tld = array_map(function ($value) {
            return "*." . $value;
        }, app(SearchSettings::class)->blacklist_tld);
        $blacklist = array_merge($blacklist_tld, app(SearchSettings::class)->blacklist);

        # Generating link with set cookies
        $settings_params = [];

        # Add Settings for searchengines supplied in cookies and headers
        foreach (array_merge($request->header(), $request->cookie()) as $key => $value) {
            if (is_array($value)) {
                $value = $value[0];
            }
            if ($settings->isValidSetting($key, $value)) {
                $settings_params[$key] = $value;
            }
        }

        unset($settings_params["js_available"]);
        if ($authorization instanceof KeyAuthorization) {
            $settings_params["key"] = $authorization->getToken();
        }
        $cookieLink = null;
        if (sizeof($settings_params) > 0) {
            $cookieLink = route('loadSettings', $settings_params);
        }

        $agent = new Agent();

        return response(view('settings.index')
            ->with('title', trans('titles.settings', ['fokus' => $fokusName]))
            ->with('fokus', $settings->fokus)
            ->with('fokusName', $fokusName)
            ->with('authorization', $authorization)
            ->with('filteredSumas', $filteredSumas)
            ->with('disabledReasons', app(Searchengines::class)->disabledReasons)
            ->with('sumas', $sumas)
            ->with('searchCost', app(Searchengines::class)->getSearchCost())
            ->with('filter', $filters)
            ->with('settingActive', $settingActive)
            ->with('url', $url)
            ->with('blacklist', $blacklist)
            ->with('cookieLink', $cookieLink)
            ->with('agent', $agent)
            ->with('browser', $agent->browser())
            ->with('js', [mix('js/scriptSettings.js')]), 200, ["Cache-Control" => "no-store"]);
    }

    private function getSumas($fokus)
    {
        $langFile = MetaGer::getLanguageFile();
        $langFile = json_decode(file_get_contents($langFile));

        if (empty($langFile->foki->{$fokus})) {
            // Fokus does not exist in this suma file
            return [];
        }

        $sumasFoki = $langFile->foki->{$fokus}->sumas;

        $sumas = [];
        $locale = LaravelLocalization::getCurrentLocaleRegional();
        $lang = Localization::getLanguage();
        foreach ($sumasFoki as $suma) {
            if (
                (!empty($langFile->sumas->{$suma}->disabled) && $langFile->sumas->{$suma}->disabled) ||
                (!empty($langFile->sumas->{$suma}->{"auto-disabled"}) && $langFile->sumas->{$suma}->{"auto-disabled"}) ||
                    ## Lang support is not defined
                (!\property_exists($langFile->sumas->{$suma}, "lang") || !\property_exists($langFile->sumas->{$suma}->lang, "languages") || !\property_exists($langFile->sumas->{$suma}->lang, "regions")) ||
                    ## Current Locale/Lang is not supported by this engine
                (!\property_exists($langFile->sumas->{$suma}->lang->languages, $lang) && !\property_exists($langFile->sumas->{$suma}->lang->regions, $locale))
            ) {
                continue;
            }
            $sumas[$suma]["display-name"] = $langFile->sumas->{$suma}->infos->display_name;
            $sumas[$suma]["filtered"] = false;
            if (Cookie::get($fokus . "_engine_" . $suma) === "off") {
                $sumas[$suma]["enabled"] = false;
            } else {
                $sumas[$suma]["enabled"] = true;
            }
        }

        foreach ($langFile->filter->{"parameter-filter"} as $name => $filter) {
            $values = $filter->values;
            $cookie = Cookie::get($fokus . "_setting_" . $filter->{"get-parameter"});
            foreach ($sumas as $suma => $sumaInfo) {
                if ($cookie !== null && (empty($filter->sumas->{$suma}) || (!empty($filter->sumas->{$suma}) && empty($filter->sumas->{$suma}->values->$cookie)))) {
                    $sumas[$suma]["filtered"] = true;
                }
            }
        }
        return $sumas;
    }

    public function disableSearchEngine(Request $request)
    {
        $sumaName = $request->input('suma', '');
        $url = $request->input('url', '');

        if (empty($sumaName)) {
            abort(404);
        }

        $settings = app(SearchSettings::class);
        $engines = app(Searchengines::class)->getSearchEnginesForFokus();
        $secure = app()->environment("local") ? false : true;
        if (!$engines[$sumaName]->configuration->disabled) {
            if ($engines[$sumaName]->configuration->disabledByDefault) {
                Cookie::queue(Cookie::forget($settings->fokus . "_engine_" . $sumaName, "/"));
            } else {
                Cookie::queue(Cookie::forever($settings->fokus . "_engine_" . $sumaName, "off", "/", null, $secure, false));
            }
        }

        $redirect_url = route('settings', ["focus" => $settings->fokus, "url" => $url, "anchor" => "engines"]);

        if ($request->wantsJson()) {
            $response = $this->cookiesToJsonResponse($redirect_url);
            return response()->json($response);
        } else {
            return redirect($redirect_url);
        }
    }

    public function enableSearchEngine(Request $request)
    {
        $sumaName = $request->input('suma', '');
        $url = $request->input('url', '');

        if (empty($sumaName)) {
            abort(404);
        }

        $settings = app(SearchSettings::class);
        $engines = app(Searchengines::class)->getSearchEnginesForFokus();
        $secure = app()->environment("local") ? false : true;
        if ($engines[$sumaName]->configuration->disabled) {
            if ($engines[$sumaName]->configuration->disabledByDefault) {
                Cookie::queue(Cookie::forever($settings->fokus . "_engine_" . $sumaName, "on", "/", null, $secure, false));
            } else {
                Cookie::queue(Cookie::forget($settings->fokus . "_engine_" . $sumaName, "/"));
            }
        }

        $redirect_url = route('settings', ["focus" => $settings->fokus, "url" => $url, "anchor" => "engines"]);
        if ($request->wantsJson()) {
            $response = $this->cookiesToJsonResponse($redirect_url);
            return response()->json($response);
        } else {
            return redirect($redirect_url);
        }
    }

    public function enableFilter(Request $request)
    {
        $fokus = $request->input('focus', '');
        $url = $request->input('url', '');
        if (empty($fokus)) {
            abort(404);
        }

        $newFilters = $request->except(["focus", "url"]);

        $langFile = MetaGer::getLanguageFile();
        $langFile = json_decode(file_get_contents($langFile));

        $settings = app(SearchSettings::class);
        app(Searchengines::class); // Needs to be loaded for parameterfilters to be populated

        foreach ($newFilters as $key => $value) {
            if (!empty($value)) {
                // Check if the new value is the default value for this filter
                foreach ($settings->parameterFilter as $name => $filter) {
                    if ($filter->{"get-parameter"} === $key && $filter->{"default-value"} === $value) {
                        $value = null;
                    }
                }
            }
            if (empty($value)) {
                $path = \Request::path();
                $cookiePath = "/";
                Cookie::queue(Cookie::forget($fokus . "_setting_" . $key, "/"));
            } else {
                # Check if this filter and its value exists:
                foreach ($langFile->filter->{"parameter-filter"} as $name => $filter) {
                    if ($key === $filter->{"get-parameter"} && !empty($filter->values->$value)) {
                        $path = \Request::path();
                        $cookiePath = "/";
                        $secure = app()->environment("local") ? false : true;
                        Cookie::queue(Cookie::forever($fokus . "_setting_" . $key, $value, "/", null, $secure, false));
                        break;
                    }
                }
            }
        }

        $redirect_url = route('settings', ["focus" => $fokus, "url" => $url, "anchor" => "filter"]);
        if ($request->wantsJson()) {
            $response = $this->cookiesToJsonResponse($redirect_url);
            return response()->json($response);
        } else {
            return redirect($redirect_url);
        }
    }

    /**
     * Will enable the use of an external search provider. Currently
     * supported for the imagesearch as we do not currently have
     * access to a viable and free alternative
     */
    public function enableExternalSearchProvider(Request $request)
    {
        $fokus = $request->input('focus', '');
        $url = $request->input('url', '');
        $secure = app()->environment("local") ? false : true;

        $external_setting = $request->input('bilder_setting_external', '');
        if (!empty($external_setting) && in_array($external_setting, ["google", "bing", "metager"])) {
            if ($external_setting === "metager") {
                Cookie::queue(Cookie::forget("bilder_setting_external", "/"));
            } else {
                Cookie::queue(Cookie::forever("bilder_setting_external", $external_setting, "/", null, $secure, false));
            }
        }

        $redirect_url = route('settings', ["focus" => $fokus, "url" => $url, "anchor" => "external-search-service"]);
        if ($request->wantsJson()) {
            $response = $this->cookiesToJsonResponse($redirect_url);
            return response()->json($response);
        } else {
            return redirect($redirect_url);
        }
    }

    public function enableSetting(Request $request)
    {
        $fokus = $request->input('focus', '');
        $url = $request->input('url', '');
        $secure = app()->environment("local") ? false : true;
        // Currently only the setting for quotes is supported

        $suggestions = $request->input('sg', '');
        if (!empty($suggestions)) {
            if ($suggestions === "off") {
                Cookie::queue(Cookie::forever('suggestions', 'off', '/', null, $secure, false));
            } elseif ($suggestions === "on") {
                Cookie::queue(Cookie::forget("suggestions", "/"));
            }
        }

        $self_advertisements = $request->input('self_advertisements', '');
        if (!empty($self_advertisements)) {
            if ($self_advertisements === "off") {
                Cookie::queue(Cookie::forever('self_advertisements', 'off', '/', null, $secure, false));
            } elseif ($self_advertisements === "on") {
                Cookie::queue(Cookie::forget("self_advertisements", "/"));
            }
        }

        $tiles_startpage = $request->input('tiles_startpage', '');
        if (!empty($tiles_startpage)) {
            if ($tiles_startpage === "off") {
                Cookie::queue(Cookie::forever('tiles_startpage', 'off', '/', null, $secure, false));
            } elseif ($tiles_startpage === "on") {
                Cookie::queue(Cookie::forget("tiles_startpage", "/"));
            }
        }

        $quotes = $request->input('zitate', '');
        if (!empty($quotes)) {
            if ($quotes === "off") {
                Cookie::queue(Cookie::forever('zitate', 'off', '/', null, $secure, false));
            } elseif ($quotes === "on") {
                Cookie::queue('zitate', '', 5256000, '/', null, $secure, true);
            }
        }

        $darkmode = $request->input('dm');
        if (!empty($darkmode)) {
            if ($darkmode === "off") {
                Cookie::queue(Cookie::forever('dark_mode', '1', '/', null, $secure, false));
            } elseif ($darkmode === "on") {
                Cookie::queue(Cookie::forever('dark_mode', '2', '/', null, $secure, false));
            } elseif ($darkmode === "system") {
                Cookie::queue(Cookie::forget('dark_mode', '/'));
            }
        }

        $newTab = $request->input('nt');
        if (!empty($newTab)) {
            if ($newTab === "off") {
                Cookie::queue(Cookie::forget('new_tab', '/'));
            } elseif ($newTab === "on") {
                Cookie::queue(Cookie::forever('new_tab', 'on', '/', null, $secure, false));
            }
        }

        $redirect_url = route('settings', ["focus" => $fokus, "url" => $url, "anchor" => "more-settings"]);
        $headers = ["Cache-Control" => "no-store"];
        if ($request->wantsJson()) {
            $response = $this->cookiesToJsonResponse($redirect_url);
            return response()->json($response, 200, $headers);
        } else {
            return redirect($redirect_url, 302, $headers);
        }
    }

    public function deleteSettings(Request $request)
    {
        $fokus = $request->input('focus', '');
        $url = $request->input('url', '');
        if (empty($fokus)) {
            abort(404);
        }

        $global_settings = [
            "dark_mode",
            "new_tab",
            "zitate",
            "self_advertisements",
            "tiles_startpage",
            "suggestions",
        ];

        $settings = Cookie::get();
        if ($request->wantsJson()) {
            foreach ($request->header() as $key => $value) {
                $settings[str_replace("-", "_", $key)] = $value;
            }
        }
        foreach ($settings as $key => $value) {
            if (stripos($key, $fokus . "_engine_") === 0 || stripos($key, $fokus . "_setting_") === 0) {
                Cookie::queue(Cookie::forget($key, "/"));
            }

            if (in_array($key, $global_settings)) {
                Cookie::queue(Cookie::forget($key, "/"));
            }
        }
        $this->clearBlacklist($request);

        $redirect_url = route('settings', ["focus" => $fokus, "url" => $url]);
        if ($request->wantsJson()) {
            $response = $this->cookiesToJsonResponse($redirect_url);
            return response()->json($response);
        } else {
            return redirect($redirect_url);
        }
    }

    public function allSettingsIndex(Request $request)
    {
        $sumaFile = MetaGer::getLanguageFile();
        $sumaFile = json_decode(file_get_contents($sumaFile));

        return view('settings.allSettings')
            ->with('title', trans('titles.allSettings'))
            ->with('url', $request->input('url', ''))
            ->with('sumaFile', $sumaFile);
    }

    public function removeOneSetting(Request $request)
    {
        $key = $request->input('key', '');
        $path = \Request::path();
        $cookiePath = "/";
        if ($key === 'dark_mode') {
            Cookie::queue(Cookie::forget($key, "/"));
        } elseif ($key === 'new_tab') {
            Cookie::queue(Cookie::forget($key, "/"));
        } elseif ($key === 'key') {
            Cookie::queue(Cookie::forget($key, "/"));
        } elseif ($key === 'zitate') {
            Cookie::queue(Cookie::forget($key, "/"));
        } else {
            Cookie::queue(Cookie::forget($key, "/"));
        }

        $redirect_url = $request->input('url', 'https://metager.de');
        if ($request->wantsJson()) {
            $response = $this->cookiesToJsonResponse($redirect_url);
            return response()->json($response);
        } else {
            return redirect($redirect_url);
        }
    }

    public function removeAllSettings(Request $request)
    {
        foreach (app(SearchSettings::class)->user_settings as $key => $value) {
            Cookie::queue(Cookie::forget($key, "/"));
        }

        $redirect_url = $request->input('url', 'https://metager.de');
        if ($request->wantsJson()) {
            $response = $this->cookiesToJsonResponse($redirect_url);
            return response()->json($response);
        } else {
            return redirect($request->input('url', 'https://metager.de'));
        }
    }

    public function newBlacklist(Request $request)
    {
        $fokus = $request->input('focus', '');
        $url = $request->input('url', '');

        $blacklist = $request->input('blacklist');
        $blacklist = substr($blacklist, 0, 2048);

        // Split the blacklist by all sorts of newlines
        $blacklist = preg_split('/\r\n|[\r\n]/', $blacklist);

        $valid_blacklist_entries = [];

        foreach ($blacklist as $blacklist_entry) {
            if (!preg_match('/^https?:\/\//', $blacklist_entry)) {
                $blacklist_entry = "https://" . $blacklist_entry;
            }
            // Only use hostname from url
            $blacklist_entry = parse_url($blacklist_entry, PHP_URL_HOST);
            if ($blacklist_entry === null || $blacklist_entry === false)
                continue;
            $blacklist_entry = substr($blacklist_entry, 0, 255);

            $valid_blacklist_entries[] = $blacklist_entry;
        }
        $valid_blacklist_entries = array_unique($valid_blacklist_entries);
        sort($valid_blacklist_entries);

        # Check if any setting is active
        $cookies = Cookie::get();

        # Remove all cookies from the old method where they got stored
        # in multiple Cookies.
        # The old cookies are in the request currently send so just delete the old cookie
        foreach ($cookies as $key => $value) {
            if (preg_match('/_blpage[0-9]+$/', $key) === 1 && stripos($key, $fokus) !== false) {
                Cookie::queue(Cookie::forget($key, "/"));
            }
        }

        $valid_blacklist_entries = array_unique($valid_blacklist_entries);
        sort($valid_blacklist_entries);

        $cookieName = $fokus . '_blpage';
        $secure = app()->environment("local") ? false : true;
        Cookie::queue(Cookie::forever($cookieName, implode(",", $valid_blacklist_entries), "/", null, $secure, false));


        $redirect_url = route('settings', ["focus" => $fokus, "url" => $url, "anchor" => "bl"]);
        if ($request->wantsJson()) {
            $response = $this->cookiesToJsonResponse($redirect_url);
            return response()->json($response);
        } else {
            return redirect($redirect_url);
        }
    }

    public function deleteBlacklist(Request $request)
    {
        $fokus = $request->input('focus', '');
        $url = $request->input('url', '');
        $cookieKey = $request->input('cookieKey');

        Cookie::queue(Cookie::forget($cookieKey, "/"));

        $redirect_url = route('settings', ["focus" => $fokus, "url" => $url, "anchor" => "bl"]);
        if ($request->wantsJson()) {
            $response = $this->cookiesToJsonResponse($redirect_url);
            return response()->json($response);
        } else {
            return redirect($redirect_url);
        }
    }

    public function clearBlacklist(Request $request)
    {
        //function to clear the whole black list
        $fokus = $request->input('focus', '');
        $url = $request->input('url', '');
        $cookies = Cookie::get();

        foreach ($cookies as $key => $value) {
            if (stripos($key, $fokus . '_blpage') === 0) {
                Cookie::queue(Cookie::forget($key, "/"));
            }
        }

        $redirect_url = route('settings', ["focus" => $fokus, "url" => $url]);
        if ($request->wantsJson()) {
            $response = $this->cookiesToJsonResponse($redirect_url);
            return response()->json($response);
        } else {
            return redirect($redirect_url);
        }
    }

    public function loadSettings(Request $request)
    {
        $langFile = MetaGer::getLanguageFile();
        $langFile = json_decode(file_get_contents($langFile));

        $settings = $request->query();
        $secure = app()->environment("local") ? false : true;

        $params_for_startpage = [];
        if ($request->filled("eingabe")) {
            $params_for_startpage["eingabe"] = $request->input("eingabe");
        }

        $searchsettings = app(SearchSettings::class);

        foreach ($settings as $key => $value) {
            # Add Settings for searchengines supplied in cookies and headers
            if ($searchsettings->isValidSetting($key, $value)) {
                if ($key === 'key') {
                    Cookie::queue(Cookie::forever("key", $value, '/', null, $secure, false));
                    $params_for_startpage["key"] = $value;
                } elseif ($key === 'dark_mode' && ($value === '1' || $value === '2')) {
                    Cookie::queue(Cookie::forever($key, $value, '/', null, $secure, false));
                } elseif ($key === 'new_tab' && $value === 'on') {
                    Cookie::queue(Cookie::forever($key, 'on', '/', null, $secure, false));
                } elseif ($key === 'zitate' && $value === 'off') {
                    Cookie::queue(Cookie::forever($key, 'off', '/', null, $secure, false));
                } else {
                    // Setting page
                    Cookie::queue(Cookie::forever($key, $value, '/', null, $secure, false));
                }
            }
        }

        // Check if a redirect url is defined
        if ($request->filled("redirect_url") && $request->filled("signature") && $request->filled("expires")) {
            $signature = $request->input("signature");
            $expires = filter_var($request->input("expires"), FILTER_VALIDATE_INT);
            $redirect_url = $request->input("redirect_url");
            if (now()->unix() <= $expires && hash_equals(hash_hmac("sha256", $redirect_url . $request->input("expires"), config("app.key")), $signature)) {
                $url = $redirect_url;
            } else {
                $url = route("startpage", $params_for_startpage);
            }
        } else {
            $url = route("startpage", $params_for_startpage);
        }

        return redirect($url);
    }

    /**
     * The webextension calls settings manually
     * and expects a json response instead of
     * set cookies.
     * We will loop through all queued cookies and 
     * create a JSON response object from that
     */
    private function cookiesToJsonResponse($redirect_url)
    {
        $cookies = Cookie::getQueuedCookies();
        $response = [
            "remove" => [],
            "set" => [],
            "redirect" => $redirect_url
        ];
        foreach ($cookies as $cookie) {
            if ($cookie->isCleared()) {
                $response["remove"][] = $cookie->getName();
            } else {
                $response["set"][$cookie->getName()] = $cookie->getValue();
            }
        }
        return $response;
    }
    private function loadBlacklist(Request $request)
    {
    }
}